<?php
class My_Form_validation extends CI_Form_validation {
		
	

	function valid_domain($domain) {
		// Strip out any http or www
		$search = array('http://', 'www.');
		$replace = array('', '');
		$domain = str_replace($search, $replace, $domain);

		//$regex = "/^([a-zа-я0-9][a-zа-я0-9\-]{1,63})\.[a-zа-я\.]{2,6}$/iu";
		$regex = "/([a-zA-Zа-яА-Я0-9\-_]+\.)?[a-zа-яA-ZА-Я0-9\-_]+\.[a-zA-Zа-яА-Я]{2,5}/iu";
		
		$result = preg_match($regex, $domain, $matches);
		if (!$result) {
			$this -> set_message(__FUNCTION__, "Домен &quot;{$domain}&quot; не валиден.");
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	public function phone_validate($phone) {
		// Костыль на модерацию телефона.
		return TRUE;
		
		$regex = "/^((8|\+7|3|\+3)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/iu";
		
		$result = preg_match($regex, $phone, $matches);
		if (!$result) {
			$this -> set_message(__FUNCTION__, "Номер телефона &quot;{$phone}&quot; не валиден.");
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	/**
	 * Check captcha in form
	 *
	 * @return bool
	 * @author  
	 */
	public function captcha($captcha) {
		$captcha = strtoupper($captcha);
		$expiration = time()-7200; // Two hour limit
		$this->CI->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);	
		
		// Then see if a captcha exists:
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($captcha, $this->CI->input->ip_address(), $expiration);
		$query = $this->CI->db->query($sql, $binds);
		$row = $query->row();
		
		if ($row->count == 0)
		{
		    return false;
		} else {
			return true;
		}
	}

}
?>