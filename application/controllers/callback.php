<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class callback extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
		$this->load->model('pagesModel','pages');
		$this->load->model('callbackmodel','callback');
		
		$this->template->set_theme('frontend_second');
		$this->template->set_layout('default');
		$this->load->model('menu');
		
		$this->template->set(array('tPath'=>'/include/frontend/'));
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$data=array();
		if ($this->input->post()) {
				$this->form_validation->set_rules('name', 'Имя', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
				$this->form_validation->set_rules('message', 'Сообщение', 'required');
				$this->form_validation->set_rules('phone', 'Телефон', 'phone_validate');
				$this->form_validation->set_rules('captcha', 'Код безопасности', 'required|captcha');
				
				$this->form_validation->set_message('required', '"%s" - обязательное поле');
				$this -> form_validation -> set_message('valid_email', '"%s" - электронный ящик не корректен');
				$this->form_validation->set_message('alpha_dash', '"%s" - поле должно быть числом');
				$this->form_validation->set_message('captcha', '"%s" - значение введено не верно');
				
				if ($this->form_validation->run() == FALSE)
				{
					$data['result']='error';
					$data['html']=$this->load->view('callback',array(),true);
				}
				else
				{
					$data['result']='success';
					$this->callback->sendMessage($this->input->post());
					$data['html']=$this->callback->messageSuccess();
				}
			
		} else {
			
		}
				
		echo json_encode($data);
		
		
		//$data['message'] = 'Ваше сообщение успешно отправлено!<br/>В ближайшее время наш менеджер свяжется с Вами!';
		//$this->template->title('Сообщение успешно отправлено')->build('pages/callback', $data);
	}
	
}