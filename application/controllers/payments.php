<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('menu');
		$this -> template -> set_theme('frontend_second');
		$this->template->set_layout('default');		
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
		
		$subMenuItems = new menuItem('additional-menu','/money');
		$subMenuItems->addChild('История платежей', '', 'icon-eye-open');
		$subMenuItems->addChild('Пополнить', 'add', 'icon-plus');
		$subMenu = $subMenuItems->generate();
		
		$userInfo = $this->ion_auth->user()->row();
		
		$this->template->set(array(
			'tPath'=>'/include/frontend/',
			'subMenu' => $subMenu,
			'user' => $userInfo
		));
	}
	
	
	
	public function result()
	{
		
		
	    $post = $this->input->post(); 
        if (empty($post))
        {
            $data['alertClass'] = 'alert-danger';
            $data['message'] = 'К сожалению, ваша страница не может быть отображена';
			$this->template->title('Ошибка 404 - Страница не найдена')->build('modules/money/dummy', $data);
        }
        else
        {
            $this->load->library('robokassa');
            $this->load->model('moneyModel','money');
            $inv_id = (int)$post['InvId'];
            $result = $this->robokassa->result($post);
            if ($result == "OK$inv_id")
            {
                $this->money->updatePaymentStatus($inv_id);
				// Начисление бонусных процентов
				//$this->money->setBonus($inv_id,50);
                echo "OK$inv_id";
            }
            else
            {
                $data['alertClass'] = 'alert-danger';
	            $data['message'] = 'Ошибка пополнения баланса. Обратитесь в службу поддержки';
				$this->template->title('Пополнение баланса')->build('modules/money/dummy', $data);
            }
        }	
	}
    
    public function success()
    {
		$this->load->model('menu');
		if (!$this->ion_auth->logged_in()) {
			redirect('/');
		}	
		$this->menu->setActive('money');
		$mainMenu = $this->menu->menuItems->generate();
	    $post = $this->input->post(); 

        if (empty($post))
        {
            $data['alertClass'] = 'alert-danger';
            $data['message'] = 'К сожалению, ваша страница не может быть отображена';
			$this->template->title('Ошибка 404 - Страница не найдена')->build('modules/money/dummy', $data);
        }
        else
        {
            $this->load->library('robokassa');
            $result = $this->robokassa->success($post);
            // Сделать шаблон в рамках бэкенда, например как в /money/add
            if ($result == true)
            {
				$data['alertClass'] = 'alert-success';
	            $data['message'] = 'Баланс успешно пополнен';
				$this->template->title('Пополнение баланса')->build('modules/money/dummy', $data);
            }
            else
            {
				$data['alertClass'] = 'alert-danger';
	            $data['message'] = 'Ошибка пополнения баланса. Обратитесь в службу поддержки';
				$this->template->title('Пополнение баланса')->build('modules/money/dummy', $data);
				
            } 
        }        

    }
    
    public function fail()
    {
        $this->load->model('menu');
		if (!$this->ion_auth->logged_in()) {
			redirect('/');
		}
		$userInfo = $this->ion_auth->user()->row();
		$mainMenu = $this->menu->menuItems->generate();
        
	    $post = $this->input->post(); 
        if (empty($post))
        {
        	$data['alertClass'] = 'alert-danger';
            $data['message'] = 'К сожалению, ваша страница не может быть отображена';
			$this->template->title('Ошибка 404 - Страница не найдена')->build('modules/money/dummy', $data);
        }
        else
        {
            $inv_id = (int)$post['InvId'];
            $data['alertClass'] = 'alert-danger';
            $data['message'] = 'Платеж '.$inv_id.' отменен';
			$this->template->title('Ошибка платежа')->build('modules/money/dummy', $data);
        }
    }
	
	
}