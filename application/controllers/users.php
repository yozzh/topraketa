<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if (!$this -> ion_auth -> logged_in()) {
			redirect('/');
		} else {
			if (!$this -> ion_auth -> is_admin()) {
				redirect('/projects');
			}
		}
		$this -> load -> model('menu');
		$this -> load -> model('usersmodel', 'users');
		$this -> load -> helper('html');
		$this->load->helper('projects');

		$subMenuItems = new menuItem('additional-menu', '/users');
		$subMenuItems -> addChild('Все пользователи', '', 'icon-eye-open');
		$subMenuItems -> addChild('Добавить пользователя', 'add', 'icon-plus');
		$subMenuItems -> addChild('Заблокированные', 'locked', 'icon-lock');
		$subMenu = $subMenuItems -> generate();
		$userInfo = $this -> ion_auth -> user() -> row();
		$this -> template -> set(array('tPath' => '/include/frontend/', 'subMenu' => $subMenu, 'user' => $userInfo));

		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('default');
		$this -> template -> set_partial('header', 'partials/header');
		$this -> template -> set_partial('footer', 'partials/footer');
	}

	public function index($page=0) {
		$scripts[] = 'users.js';
		$this -> template -> set('scripts', $scripts);
		$this -> template -> set('subtitle', 'Список пользователей');
		if ($this->input->get('email')) {
			$data['users'] = $this -> users -> search('email',$this->input->get('email'));
			$config['per_page'] = 200;
			$config['total_rows'] = count($data['users']);
		} else {
			$data['users'] = $this -> users -> get(1,$page);
			$config['per_page'] = 20;
			$config['total_rows'] = $this->users->count();	
		}
		
		$this->load->library('pagination');

		$config['base_url'] = '/users/index/';
		
		
		
		 
		
		$this->pagination->initialize($config); 
		
		$data['pagination'] = $this->pagination->create_links();
		
		$this -> template -> title('Пользователи') -> build('modules/users/index', $data);
	}
	
	public function locked() {
		$scripts[] = 'users.js';
		$this -> template -> set('scripts', $scripts);
		$this -> template -> set('subtitle', 'Заблокированные');
		$data['users'] = $this -> users -> get(0);
		$data['locked'] = true;
		$this -> template -> title('Пользователи') -> build('modules/users/index', $data);
	}

	public function edit() {
		$data['user']=$this->ion_auth->user($this->input->post('id'))->row();
		
		$groups = $this->ion_auth->get_users_groups($this->input->post('id'))->result();
		$data['user']->groups = array();
		
		foreach ($groups as $group) {
			$data['user']->groups[$group->id]=$group;
		}
		
		$data['groups']=$groups = $this->ion_auth->groups()->result();
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
				
		if ($this -> input -> post('status')=='save') {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('first_name', 'Имя', 'required');
			$this -> form_validation -> set_rules('email', 'E-mail', 'required|email');
			if ($this->input->post('password')!='') {
				$this->form_validation->set_rules('password', 'Пароль', 'matches[passconf]');
				$this->form_validation->set_rules('passconf', 'Подтверждение пароля', '');
				$this->form_validation->set_message('matches', 'Пароль и его подтверждение не совпадают');	
			}
			$this->form_validation->set_message('required', '"%s" - обязательное поле');
			if ($this -> form_validation -> run() == FALSE) {
				$result = array(
					'status'=>'edit',
					'data'=> $this -> template -> build('modules/users/edit', $data, true)
				);
			} else {
				$post = $this->input->post();
				$data = array(
					'first_name' => $post['first_name'],
					'email' => $post['email'],
					'phone' => $post['phone'],
					'username' => $post['email']
				 );
				 
				 if ($post['password']!='') {
				 	$data['password'] = $post['password'];
				 }
				$result = $this->ion_auth->update($post['id'], $data);
				$this->users->updateGroups($post['id'],$post['group']);
				$this -> session -> set_flashdata('success', 'Профиль успешно изменен.');
				$result = array(
					'status'=>'ok',
					'data'=> $this -> template -> build('modules/users/edit_success', $data, true)
				);
			}
			echo json_encode($result);
		} else {
			$result = array(
				'status'=>'edit',
				'data'=> $this -> template -> build('modules/users/edit', $data, true)
			);
			echo json_encode($result);
		}
	}

	public function addMoney() {
		$targetUser = $this->ion_auth->user($this->input->post('id'))->row();
		$data['user']=$targetUser;
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
				
		if ($this -> input -> post('status')=='save') {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('cost', 'Баланс', 'required|numeric');
			$this->form_validation->set_message('required', '"%s" - обязательное поле');
			if ($this -> form_validation -> run() == FALSE) {
				$result = array(
					'status'=>'edit',
					'data'=> $this -> template -> build('modules/users/add_money', $data, true)
				);
			} else {
				$post = $this->input->post();
				$this->load->model('moneymodel','money');
				$comment = "Пополнение баланса вручную.";
				
				$data = array(
					'user_id'=>$post['id'],
					'value'=>$post['cost'],
					'date' => date("Y-m-d H:i:s",time()),
					'status' => 2,
					'target_id' => '',
					'comment' => $comment,
					'type' => 'manual'
				);
				
				$result = $this->money->addExpense($data);
				$data['userBalance'] = $this -> money -> calcUserMoney($targetUser->id);
				$this -> template -> set_theme('frontend_second');
				$this -> template -> set_layout('ajax');
				$body = $this->template->build('modules/money/email/add_expense',$data, true);
				$this->subscribemodel->send($targetUser->email,'Пополнение баланса', $body);
				$this -> session -> set_flashdata('success', 'Баланс успешно пополнен.');
				$result = array(
					'status'=>'ok',
					'data'=> $this -> template -> build('modules/users/edit_success', $data, true)
				);
			}
			echo json_encode($result);
		} else {
			$result = array(
				'status'=>'edit',
				'data'=> $this -> template -> build('modules/users/add_money', $data, true)
			);
			echo json_encode($result);
		}
	}
	
	public function lock() {
		$post = $this->input->post();
		$this->users->lock($post['id']);
		$result = array(
			'status'=>'ok',
			'data'=> '<div class="alert alert-success">Пользователь успешно заблокирован</div><META HTTP-EQUIV="REFRESH" CONTENT="1">'
		);
		echo json_encode($result);
	}
	
	
	public function unlock() {
		$post = $this->input->post();
		$this->users->lock($post['id'],1);
		$result = array(
			'status'=>'ok',
			'data'=> '<div class="alert alert-success">Пользователь успешно разблокирован</div><META HTTP-EQUIV="REFRESH" CONTENT="1">'
		);
		echo json_encode($result);
	}
	
	/**
	 * Switch to target user imitation
	 *
	 * @return bool
	 * @author  
	 */
	function magicEye($userId) {
		$this->ion_auth->magicEye($userId);
		redirect('/projects');
	}
	
}
