<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Help extends AdminController {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('pagesmodel', 'pages');
		$this -> menu -> setActive('/admin/help');
		$mainMenu = $this -> menu -> menuItems -> generate();
		$subMenuItems = new menuItem('additional-menu', '/admin/help');
		$subMenuItems -> addChild('Страницы', '');
    $subMenuItems -> addChild('Добавить страницу', 'add');
		$subMenu = $subMenuItems -> generate();
		$userInfo = $this -> ion_auth -> user() -> row();
		$this -> template -> set(array('tPath' => '/include/backend/', 'mainMenu' => $mainMenu, 'user' => $userInfo, 'subMenu' => $subMenu));

		$this -> template -> set_layout('default');
		$this -> template -> set_partial('header', 'partials/header');
		$this -> template -> set_partial('footer', 'partials/footer');
	}

	public function index() {
	  $data['pages']=$this->pages->pages();
		$this->template->build('modules/help/index',$data);
	}
  
  public function add() {
    $post = $this->input->post();
    
    if ($post) {
      $post['status']=1;
      $post['pid']=0;
      $this->pages->save($post);
      redirect('/admin/help');      
    } else {
      $scripts[] = 'admin/help.js';
      $this -> template -> set('scripts', $scripts);
      $this->template->build('modules/help/add',array('newOrder'=>$this->pages->getMaxOrder()));  
    }
    
  }
  
  function edit($id=false) {
    $post = $this->input->post();
    if ($post) {
      $this->pages->save($post,$post['id']);
      redirect('/admin/help');      
    } else {
      $data['page'] = $this->pages->page($id);
      $scripts[] = 'admin/help.js';
      $this -> template -> set('scripts', $scripts);
      $this->template->build('modules/help/add',$data);  
    }
  }
  
  function delete($id) {
    $this->pages->archive($id);
    redirect('/admin/help');
  }

	protected function informer() {
		$this -> setAjax();
		$data['informer'] = $this -> finance -> stat();
		return $this -> template -> build('modules/finance/informer', $data, true);
	}

}
