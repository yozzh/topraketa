<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finance_Users extends Finance {
	
	public function __construct()
   	{
   		parent::__construct();
		$this->load->model('usersmodel','users');
		
	}
	
	public function index($page=0) {
		$scripts[] = 'admin/finance.js';
		$this -> template -> set('scripts', $scripts);
		
		if ($this->input->get('email')) {
			$data['users'] = $this -> finance -> search('email',$this->input->get('email'));
			$config['per_page'] = 200;
			$config['total_rows'] = count($data['users']);
		} else {
			$data['users'] = $this -> finance -> get(1,$page);
			$config['per_page'] = 20;
			$config['total_rows'] = $this->finance->count();	
		}
		
		$this->load->library('pagination');

		$config['base_url'] = '/admin/finance/users/index';
	
		$this->pagination->initialize($config); 
		
		$data['pagination'] = $this->pagination->create_links();
		
		$this -> template -> title('Финансы - Клиенты') -> build('modules/finance/usersIndex', $data);
	}

	public function statistic($userId) {
		$this->setAjax();
		$data['statistic'] = $this->finance->statistic($userId);
		
		$user = $this->ion_auth->user($userId)->row();
		
		$json['title'] = $user->first_name.' - '.$user->email;
		$json['body'] = $this -> template -> build('modules/finance/statistic', $data, true);
		header('Content-type: application/json');
		echo json_encode($json);
	}
}