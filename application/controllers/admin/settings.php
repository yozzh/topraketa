<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
	
	public function __construct()
   	{
        parent::__construct();
		$this->load->model('adminmenu','menu');
		$this->load->spark('ion_auth/2.4.0');
		
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
			redirect('/');
		}
		
		$this->menu->setActive('/main');
		$mainMenu = $this->menu->menuItems->generate();
		$subMenuItems = new menuItem('additional-menu','/admin/settings');
		$subMenuItems->addChild('Счетчик на главной', 'counter');
		$subMenuItems->addChild('Ключи на главной', 'keywords');
		$subMenu = $subMenuItems->generate();
		
        $userInfo = $this->ion_auth->user()->row();
		$this->template->set(array(
			'tPath'=>'/include/backend/',
			'mainMenu' => $mainMenu,
			'user' => $userInfo,
			'subMenu' => $subMenu
		));
		

		$this->template->set_layout('default');
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$this->template->build('modules/admin/index');
	}
	
	public function counter() {
		if ($this->input->post()) {
			$this->menu->saveForm($this->input->post());
			$this -> session -> set_flashdata('success', 'Данные успешно изменены');
		}
		$data['form']=$this->menu->getForm('counter','/admin/settings/counter');
		$this->template->build('modules/settings/counter', $data);
	}
	
	public function keywords() {
		if ($this->input->post()) {
			$keywords = explode("\n",$this->input->post('keywords_keywordsItems'));
			$json = json_encode($keywords);
			$this->db->where('alias','keywordsItems');
			$this->db->update('settings',array('value'=>$json));
			$this -> session -> set_flashdata('success', 'Данные успешно изменены');
			redirect('/admin/settings/keywords');
		}
		$jsonKeywords = (array)json_decode($this->settingsmodel->get('keywordsItems'),true);
		//var_dump($jsonKeywords);
		$data['keywords'] = implode("\n",$jsonKeywords);
		$this->template->build('modules/settings/keywords', $data);
	}
}