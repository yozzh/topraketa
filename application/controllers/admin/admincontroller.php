<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminController extends CI_Controller {
	
	public function __construct()
   	{
        parent::__construct();
		
		$this->load->model('adminmenu','menu');
		$this->load->spark('ion_auth/2.4.0');
		$this->load->helper('projects');
		
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
			redirect('/');
		}
		
		$this->menu->setActive('/main');
		$mainMenu = $this->menu->menuItems->generate();
		
		$subMenuItems = new menuItem('additional-menu','/admin/finance');
		$subMenuItems->addChild('Клиенты', 'users');
		$subMenuItems->addChild('Проекты', 'projects');
		$subMenuItems->addChild('Статистика', 'statistic');
		$subMenu = $subMenuItems->generate();
		
        $userInfo = $this->ion_auth->user()->row();
		$this->template->set(array(
			'tPath'=>'/include/backend/',
			'user' => $userInfo,
			//'mainMenu' => $mainMenu,
			//'subMenu' => $subMenu
		));
		

		$this->template->set_layout('default');
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
		
   	}	
	
	protected function setAjax() {
		$this->template->set_layout('ajax');
	}
}