<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Finance extends AdminController {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('financemodel', 'finance');
		$this -> menu -> setActive('/admin/finance');
		$mainMenu = $this -> menu -> menuItems -> generate();
		$subMenuItems = new menuItem('additional-menu', '/admin/finance');
		$subMenuItems -> addChild('Клиенты', 'users');
		$subMenuItems -> addChild('Проекты', '#');
		$subMenuItems -> addChild('Статистика', '#');
		$subMenu = $subMenuItems -> generate();
		$userInfo = $this -> ion_auth -> user() -> row();
		$this -> template -> set(array('tPath' => '/include/backend/', 'mainMenu' => $mainMenu, 'user' => $userInfo, 'subMenu' => $subMenu, 'preContent' => $this -> informer()));

		$this -> template -> set_layout('default');
		$this -> template -> set_partial('header', 'partials/header');
		$this -> template -> set_partial('footer', 'partials/footer');
	}

	public function index() {
		$this->template->build('modules/admin/index');
	}

	protected function informer() {
		$this -> setAjax();
		$data['informer'] = $this -> finance -> stat();
		return $this -> template -> build('modules/finance/informer', $data, true);
	}

}
