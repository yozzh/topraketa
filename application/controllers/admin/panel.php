<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Panel extends CI_Controller {
	
	public function __construct()
   	{
        parent::__construct();
		$this->load->model('adminmenu','menu');
		$this->load->spark('ion_auth/2.4.0');
		
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
			redirect('/');
		}
		
		$this->menu->setActive('/main');
		$mainMenu = $this->menu->menuItems->generate();
		$subMenuItems = new menuItem('additional-menu','/admin');
		$subMenuItems->addChild('Администрирование', '');
		$subMenuItems->addChild('Продукты', 'products');
		$subMenuItems->addChild('Пользователи', 'users');
		$subMenuItems->addChild('Платежи', 'payments');
		$subMenu = $subMenuItems->generate();
		
        $userInfo = $this->ion_auth->user()->row();
		$this->template->set(array(
			'tPath'=>'/include/backend/',
			'mainMenu' => $mainMenu,
			'user' => $userInfo,
			'subMenu' => $subMenu
		));
		

		$this->template->set_layout('default');
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$this->template->build('modules/admin/index');
	}
}