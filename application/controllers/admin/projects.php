<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends CI_Controller {
	
	public function __construct()
   	{
        parent::__construct();
		$this->load->model('adminmenu','menu');
		$this->load->spark('ion_auth/2.4.0');
		$this->load->helper('projects');
		
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
			redirect('/');
		}
		
		$this->menu->setActive('/main');
		$mainMenu = $this->menu->menuItems->generate();
		$subMenuItems = new menuItem('additional-menu','/admin/projects');
		$subMenuItems->addChild('Замороженные проекты', 'frozen');
		$subMenuItems->addChild('Перенос проектов', 'transfer');
		$subMenu = $subMenuItems->generate();
		
        $userInfo = $this->ion_auth->user()->row();
		$this->template->set(array(
			'tPath'=>'/include/backend/',
			'mainMenu' => $mainMenu,
			'user' => $userInfo,
			'subMenu' => $subMenu
		));
		

		$this->template->set_layout('default');
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$this->template->build('modules/admin/index');
	}
	
	public function frozen() {
		$data['projects'] = $this->projects->frozen(true,true);
		if (is_array($data['projects'])) {
			foreach ($data['projects'] as &$project) {
				$project->budget = $this->benefit->calcKeywordBudget($project->budget,true);
			}
		}
		$this->template->build('modules/projects/frozen',$data);
	}
	
	public function transfer($a = false) {
		$post = $this->input->post();
		if ($post) {
			$projectId = $post['projectId'];
			$project = $this->hammermodel->getProjects(array($projectId));
			$data['hm_project_id'] = $projectId;
			$data['user_id'] = $post['userId'];
			$data['domain'] = $project[0]['domain'];
			$result = $this -> projects -> save($data);
			$this -> session -> set_flashdata('success', 'Проект успешно добавлен.');
			redirect('/admin/projects/transfer');
		}
		
		$scripts[] = 'admin/transfer.js';
		$this -> template -> set('scripts', $scripts);
		$this->template->build('modules/projects/transfer');
	}
	
	public function delete($id) {
		if ($id) {
			$this -> projects -> delete($id, true, 0);
			$this -> session -> set_flashdata('success', 'Проект успешно удален.');
			redirect('/admin/projects/frozen');
		}
	}
	
	public function saveKeywords($id) {
		$result = $this->projects->saveKeywords($id);
	}
	
	public function projectsList() {
		$projects = $this->hammermodel->getProjects();
		$data['projects'] = array();
		foreach ($projects as $project) {
			$tmpProject = array(
				'id'=>$project['id'],
				'domain'=>$project['domain']
			);
			$data['projects'][] = $tmpProject;
		}		
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	public function usersList() {
		$users = $this->ion_auth->users()->result();
		$data['users'] = array();
		foreach ($users as $user) {
			$tmpUser = array(
				'id'=>$user->id,
				'email'=>$user->email
			);
			$data['users'][] = $tmpUser;
		}		
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
}