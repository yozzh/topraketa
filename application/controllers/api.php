<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('partnershipmodel', 'partnership');
		$this -> load -> model('apimodel', 'api');
		$this -> load -> helper('string');
		$this->ion_auth->set_error_delimiters('',". ");
		$postJson = $this->input->post('json');
		if ($postJson) {
			//log_message('debug', 'api-json'.print_r(json_decode($postJson,true)));
		}
	}
	
	
	public function index() {
		
	}
	/**
	 * API для управления созданием аккаунтов через партнерскую программу
	 *
	 * @return void
	 * @author
	 */
	function partnership($function) {
		$args = func_get_args();
		if (is_array($args)) {
			array_shift($args);
		}
		call_user_func(array($this, 'partnership_' . $function), $args);
	}

	/**
	 *  Добавление пользователя в систему с автоматической генерацией пароля
	 *
	 * @return void
	 * @author
	 */
	function partnership_add() {
		
		$post = $this->input->post();
		$json = $this->input->post('json');
		$request = $this->api->getJson();
		$_POST['email'] = $request->email;
		$_POST['partnerCode'] = $request->partnerCode;
		$post = $this -> input -> post();

		// Email нового клиента
		$email = $post['email'];

		// Партнерский код отправителя
		$partnerCode = $post['partnerCode'];

		$this -> form_validation -> set_rules('partnerCode', 'Код партнерской программы', 'required');
		$this -> form_validation -> set_rules('email', 'Email', 'valid_email|required');
	
		$this -> form_validation -> set_message('required', '"%s" - обязательное поле');
		$this -> form_validation -> set_message('valid_email', '"%s" - электронный ящик не корректен');
		$this -> form_validation -> set_error_delimiters('', "");

		if ($this -> form_validation -> run() == FALSE) {
			$return['code'] = 'ERROR_VALIDATION';
			$return['message'] = validation_errors();

		} else {
			$addins = array();
			$addins['first_name'] = $this -> input -> post('email');
			$partnerId = $this -> partnership -> decode($partnerCode);
			if ($partnerId) {
				$addins['partner_id'] = $partnerId;
				$username = $this -> input -> post('email');
				$email = $this -> input -> post('email');
				$password = random_string('alnum', 8);
				$sum = 150;
			
				if ($this -> ion_auth -> register($username, $password, $email, $addins)) {
					$return['code'] = 'SUCCESS';
					$return['message'] = 'Клиент успешно зарегистрирован в системе. Уведомление отправлено на электронный ящик '.$email;
					
					$this->partnership->sendInvite($email,$password,$sum);
					$this->partnership->addSocialBonus($email, $sum);
				} else {
					
					$return['code'] = 'ERROR_REGISTRATION';
					$return['message'] = $this -> ion_auth -> errors();
				}
			} else {
				$return['code'] = 'ERROR_PARTNER';
				$return['message'] = 'Партнер с заданным кодом не найден.';
			}
		}
		header('Content-type: application/json');
		echo json_encode($return);
	}

	function regions($function) {
		$args = func_get_args();
		if (is_array($args)) {
			array_shift($args);
		}
		call_user_func(array($this, 'regions_' . $function), $args);
	}
	
	function regions_yandex($id = -1)
	{
		$json = read_file(APPPATH.'/data/ya_regions.json');
		if ($json) {
			$data['code']='OK';
			$data['data']=json_decode($json);
		} else {
			$data['code']='ERROR';
			$data['message']='Не удалось открыть файл. Обратитесь к администратору';
		}
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function regions_google($id = -1)
	{
		$json = read_file(APPPATH.'/data/google_regions.json');
		if ($json) {
			$data['code']='OK';
			$data['data']=json_decode($json);
		} else {
			$data['code']='ERROR';
			$data['message']='Не удалось открыть файл. Обратитесь к администратору';
		}
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function users($function) {
		$args = func_get_args();
		if (is_array($args)) {
			array_shift($args);
		}
		call_user_func(array($this, 'users_' . $function), $args);
	}
	
	function users_all($args) {
		$post = $this->input->post();
		$request = $this->api->getJson();
		$partnerId = $this -> partnership -> decode($request->partnerCode);
		$this->db->select('id, first_name, email');
		$this->db->where('partner_id',$partnerId);
		$query = $this->db->get('users');
		$users = $query->result();
		if (count($users)) {
			$data['code']='OK';
			$data['data']=$users;
		} else {
			$data['code']='ERROR';
			$data['message']='Список пользователей пуст.';
		}
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function users_info($args) {
		$request = $this->api->getJson();
		
		$partnerId = $this -> partnership -> decode($request->partnerCode);
		$this->db->select('id, first_name, email');
		$this->db->where('partner_id',$partnerId);
		$this->db->where('id',$request->userId);
		$query = $this->db->get('users');
		$user = $query->row();
		if ($user) {
			$data['code']='OK';
			$data['data']=$user;
		} else {
			$data['code']='ERROR';
			$data['message']='Список пользователей пуст.';
		}
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function users_add($args) {
		$request = $this->api->getJson();
		$_POST['email'] = (isset($request->email))?$request->email:'';
		$_POST['partnerCode'] = (isset($request->partnerCode))?$request->partnerCode:'';
		
		$this -> form_validation -> set_rules('partnerCode', 'Код партнерской программы', 'required');
		$this -> form_validation -> set_rules('email', 'Email', 'valid_email|required');
	
		$this -> form_validation -> set_message('required', '"%s" - обязательный параметр');
		$this -> form_validation -> set_message('valid_email', '"%s" - электронный ящик не корректен');
		$this -> form_validation -> set_error_delimiters('', ", ");

		if ($this -> form_validation -> run() == FALSE) {
			$data['code'] = 'ERROR_VALIDATION';
			$data['message'] = 'Ошибка валидации: '.validation_errors();

		} else {
			$password = random_string('alnum', 8);
			$partnerId = $this -> partnership -> decode($request->partnerCode);
			
			$addins['partner_id'] = $partnerId;
			$userId = $this -> ion_auth -> register($request->email, $password, $request->email, $addins);
			if ($userId) {
				$data['code'] = 'OK';
				$data['message'] = 'Пользователь успешно зарегистрирован в системе.';
				$data['return']['id'] = $userId;
			} else {
				$data['code'] = 'ERROR_REGISTRATION';
				$data['message'] = $this -> ion_auth -> errors();
			}
		}
		
		header('Content-type: application/json');
		echo json_encode($data);
	}

	function projects($function) {
		$args = func_get_args();
		$this->load->model('projectsmodel','projects');
		if (is_array($args)) {
			array_shift($args);
		}
		call_user_func(array($this, 'projects_' . $function), $args);
	}	
	
	function projects_all() {
		$request = $this->api->getJson();
		
		$_POST['partnerCode'] = (isset($request->partnerCode))?$request->partnerCode:'';
		$partnerId = $this -> partnership -> decode($_POST['partnerCode']);
		$_POST['userId'] = (isset($request->userId))?$request->userId:'';

		$this->api->checkUserPartner($_POST['userId'],$partnerId);
		$this->db->select('id, url, cdate, status');
		$this->db->where('user_id',$request->userId);
		$query = $this->db->get('projects');
		$projects = $query->result();
		if (count($projects)) {
			$data['code']='OK';
			$data['data']=$projects;
		} else {
			$data['code']='ERROR';
			$data['message']='Список проектов пуст.';
		}
		header('Content-type: application/json');
		echo json_encode($data);
	}	
	
	function projects_add() {
		$request = $this->api->getJson();
		$_POST['url'] = $request->url;
		$_POST['partnerCode'] = (isset($request->partnerCode))?$request->partnerCode:'';
		$partnerId = $this -> partnership -> decode($_POST['partnerCode']);
		$_POST['userId'] = (isset($request->userId))?$request->userId:'';
		$this->api->checkUserPartner($_POST['userId'],$partnerId);
		
		
		$this -> form_validation -> set_rules('url', 'URL сайта', 'required');
	
		$this -> form_validation -> set_message('required', '"%s" - обязательный параметр');
		$this -> form_validation -> set_error_delimiters('', ", ");

		if ($this -> form_validation -> run() == FALSE) {
			$data['code'] = 'ERROR_VALIDATION';
			$data['message'] = 'Ошибка валидации: '.validation_errors();

		} else {
				$save['hm_project_id'] = -1;
				$save['status'] = 0;
				$save['domain'] = $request->url;
				$save['user_id'] = $request->userId;
				$projectId = $this -> projects -> save($save);	
				$data['code']='OK';
				$data['message']='Проект успешно добавлен.';
				$data['return']['id'] = $projectId;
		}
		header('Content-type: application/json');
		echo json_encode($data);
	}

	function keywords($function) {
		$args = func_get_args();
		$this->load->model('projectsmodel','projects');
		if (is_array($args)) {
			array_shift($args);
		}
		call_user_func(array($this, 'keywords_' . $function), $args);
	}	

	function keywords_add() {
		$request = $this->api->getJson();
		$_POST['partnerCode'] = (isset($request->partnerCode))?$request->partnerCode:'';
		$partnerId = $this -> partnership -> decode($_POST['partnerCode']);
		$_POST['userId'] = (isset($request->userId))?$request->userId:'';
		$_POST['projectId'] = (isset($request->projectId))?$request->projectId:'';
		$this->api->checkUserPartner($_POST['userId'],$partnerId);
		$this->api->checkUserProject($_POST['userId'],$_POST['projectId']);
		
		$_POST['keywords'] = (isset($request->keywords))?$request->keywords:array(0);
		$this->api->checkKeywordsArray($_POST['keywords']);
		
		foreach ($_POST['keywords'] as $keyword) {
			$newBudget = $this->benefit->calcKeywordBudget($keyword->budget);
			$this -> projects -> addTempKeyword($_POST['projectId'], $keyword -> text, $keyword -> url, $newBudget, $keyword -> ya_region, $keyword -> g_region);
		}
		
		$data['code']='OK';
		$data['message']='Ключевые слова успешно добавлены.';
		
		header('Content-type: application/json');
		echo json_encode($data);
	}

	function keywords_budget() {
		$request = $this->api->getJson();
		$_POST['keywords'] = (isset($request->keywords))?$request->keywords:array(0);
		$this->api->checkKeywordsArray($_POST['keywords'], false);
		
		foreach ($_POST['keywords'] as &$keyword) {
			$keyword->budget = $this -> hammermodel -> getRegionBudget($keyword->text, $keyword->ya_region);
		}
		
		$data['code']='OK';
		$data['message']='Бюджет по ключевым словам успешно рассчитан.';
		$data['keywords'] = $_POST['keywords'];
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function orders($function) {
		$args = func_get_args();
		$this->load->model('moneymodel','money');
		//$this->load->model('paymentsmodel','payments');
		if (is_array($args)) {
			array_shift($args);
		}
		call_user_func(array($this, 'orders_' . $function), $args);
	}
	
	function orders_add() {
		$request = $this->api->getJson();
		
		$_POST['partnerCode'] = (isset($request->partnerCode))?$request->partnerCode:'';
		$partnerId = $this -> partnership -> decode($_POST['partnerCode']);
		$_POST['user_id'] = (isset($request->userId))?$request->userId:'';
		$this->api->checkUserPartner($_POST['user_id'],$partnerId);
		$_POST['value'] = (isset($request->value))?$request->value:0;
		$_POST['comment'] = 'Пополнение через коммерческое предложение';
		$_POST['type'] = 'api';
		$data['return']['orderId'] = $this->money->updateBalans($_POST);
		$data['return']['link'] = $this->money->generateLink($data['return']['orderId']);
		header('Content-type: application/json');
		echo json_encode($data);
	}
}
?>

