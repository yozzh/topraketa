<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('/main/login');
		}
		
		$this->load->model('menu');
		$this->load->model('ordersModel','orders');
		$this->load->helper('orders');
		$this->load->model('products');
				
		$this->menu->setActive('orders');
		$mainMenu = $this->menu->menuItems->generate();
		
		$subMenuItems = new menuItem('additional-menu','/orders');
		$subMenuItems->addChild('Все заказы', '', 'icon-eye-open');
		$subMenuItems->addChild('Добавить заказ', 'add', 'icon-plus');
		$subMenu = $subMenuItems->generate();
		$userInfo = $this->ion_auth->user()->row();
		$this->template->set(array(
			'tPath'=>'/include/',
			'mainMenu' => $mainMenu,
			'subMenu' => $subMenu,
			'user' => $userInfo
		));
		
        
		$this->template->set_layout('default');		
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$data['items']=$this->orders->items();
		$this->template->title('Заказы')->build('modules/orders/index',$data);
	}
	
	public function add($project_id=0)
	{
		$this->load->model('projectsModel','projects');
		
		
		if ($this->input->post()) {
			$post = $this->input->post();
			$result = $this->orders->save($post);
			redirect('/orders/');
		} else {
			$data['project_id']=$project_id;
			$data['projects']=$this->projects->items();
			$data['products']=$this->products->items();
			$this->template->title('Заказы')->build('modules/orders/add',$data);	
		}
	}
	
	public function edit($id)
	{
		if ($this->input->post()) {
			$post = $this->input->post();
						
			$result = $this->orders->update($post,$id);
			redirect('/orders/');
		} else {
			$data['project'] = $this->projects->item($id);
			$this->template->title('Заказы')->build('modules/orders/add',$data);	
		}
	}
	
	public function delete($id)
	{
		$this->orders->delete($id);
		redirect('/orders/');
	}
	
	public function cancel($id)
	{
		$this->orders->cancel($id);
		redirect('/orders/');
	}
	
	public function getOrderSum() 
	{
		$post = $this->input->post();
		$data['data'] = $this->products->calcOrder($post['productId'],$post['count']);
		echo json_encode($data);
	}
}