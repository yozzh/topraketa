<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Partnership extends CI_Controller {

	private $userInfo;
	public function __construct()
   	{
        parent::__construct();		
		$this->load->model('partnershipmodel','partnership');
		$this -> userInfo = $this -> ion_auth -> user() -> row();
		
		if ($this->ion_auth->logged_in()) {
			redirect('/');
		}
   	}
	
	public function index() {
		
	}
	
	public function code($code)
	{
		$partnerId = $this->partnership->decode($code);
		if ($partnerId != $this->userInfo->id) {
			$this->session->set_userdata('partnerId', $partnerId);
		}
		//redirect('/main/registration');
		redirect('/');
	}
}