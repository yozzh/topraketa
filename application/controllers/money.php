<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Money extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('/');
		}
		
		$this->load->model('menu');
		$this->load->model('moneyModel','money');
		$this->load->helper('money');
		
		$this->menu->setActive('projects');
		$mainMenu = $this->menu->menuItems->generate();
		
		$subMenuItems = new menuItem('additional-menu','/money');
		$subMenuItems->addChild('История платежей', '', 'icon-eye-open');
		$subMenuItems->addChild('Пополнить', 'add', 'icon-plus');
		
		$subMenu = $subMenuItems->generate();
		$userInfo = $this->ion_auth->user()->row();
		$this->template->set(array(
			'tPath'=>'/include/frontend/',
			'mainMenu' => $mainMenu,
			'subMenu' => $subMenu,
			'user' => $userInfo
		));
        $this -> template -> set_theme('frontend_second');
		$this->template->set_layout('default');		
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$data['items']=$this->money->payments();
		$this->template->title('Платежи')->build('modules/money/index',$data);
	}
	
	public function add()
	{
	    $this->load->library('robokassa');
		if ($this->input->post()) {
			//$result = $this->money->save($post);
			//redirect('/money/');
            $post = $this->input->post();
            $count = (int)$post['count'];
            if ($count < 10)
            {
                $data = array(
                    'error' => 'Минимальная сумма для пополнения 100 рублей',
                    'default_value' => 100
                );
                $this->template->title('Пополнить баланс')->build('modules/money/add',$data);	
                return;
            }
            $post['value'] = (int)$post['count'];
			$post['comment'] = 'Пополнение через платежную систему';
			$post['type'] = 'online';
			// Акция +50 процентов при пополнении
			//$percent50 = round($post['value'] / 100 * 50);
			//$post['value'] += $percent50;
			
            $post['user_id'] = $this->ion_auth->user()->row()->id;
            $insert_id = $this->money->updateBalans($post);
            $this->robokassa->inv_id = $insert_id;
            $this->robokassa->out_summ = $post['value'];
            $data = array(
                'payform' => $this->robokassa->layout(),
                'default_value' => (int)$post['value']
            );
            $this->template->title('Пополнить баланс')->build('modules/money/add',$data);
		} 
        else 
        {
            $data = array(
                'default_value' => 500
            );
			$this->template->title('Пополнить баланс')->build('modules/money/add',$data);	
		}
	}
	
	public function edit($id)
	{
		if ($this->input->post()) {
			$post = $this->input->post();
						
			$result = $this->projects->update($post,$id);
			redirect('/projects/');
		} else {
			$data['project'] = $this->projects->item($id);
			$this->template->title('Проекты')->build('modules/projects/add',$data);	
		}
	}
	
	public function delete($id)
	{
		$this->projects->delete($id);
		redirect('/projects/');
	}
	
	public function setPayment($id) {
		$this->money->updatePaymentStatus($id);
	}
}