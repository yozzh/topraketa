<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Support extends CI_Controller {

  private $userInfo;

	public function __construct() {
		parent::__construct();
		if (!$this -> ion_auth -> logged_in()) {
			redirect('/');
		}
		$this -> load -> model('menu');
		$this -> load -> model('supportmodel', 'support');
		$this -> load -> helper('html');
		$this -> load -> helper('support');

		$subMenuItems = new menuItem('additional-menu', '');
		$subMenuItems -> addChild('Создать тикет', '#support/tickets/add', 'icon-plus', 'modalVoid');
		$subMenuItems -> addChild('Все тикеты', '/support/tickets', 'icon-eye-open');
		if ($this->ion_auth->is_admin()) {
			$subMenuItems -> addChild('Группы вопросов', '/support/groups', 'icon-tags');
			//$subMenuItems -> addChild('Настройки', '/support/settings', 'icon-wrench');
		}
		$subMenu = $subMenuItems -> generate();
		$this->userInfo = $this -> ion_auth -> user() -> row();
		$this -> template -> set(array('tPath' => '/include/frontend/', 'subMenu' => $subMenu, 'user' => $this->userInfo));
		
		$scripts[] = 'support.js';
		$this -> template -> set('scripts', $scripts);
		
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('default');
		$this -> template -> set_partial('header', 'partials/header');
		$this -> template -> set_partial('footer', 'partials/footer');
	}
	
	public function index() {
		$this->tickets();
	}
	
	public function tickets($action='index') {
		
		
		if ($action!="index") {
			$args = func_get_args();
			unset($args[0]);
			call_user_func(array($this,'tickets_'.$action),$args);
			return;
		}
		
		$this -> template -> set('subtitle', 'Все тикеты');
		$data['tickets'] = $this -> support -> tickets();
		$this -> template -> title('Техническая поддержка') -> build('modules/support/tickets', $data);
	}
	
	public function tickets_add() {
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$data['groups'] = $this->support->groups();
		if ($this -> input -> post('status')=='save') {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('title', 'Имя', 'required');
			$this->form_validation->set_message('required', '"%s" - обязательное поле');
			if ($this -> form_validation -> run() == FALSE) {
				$result = array(
					'status'=>'edit',
					'data'=> $this -> template -> title('Техническая поддержка') -> build('modules/support/tickets_add', $data, true)
				);
			} else {
				$post = $this->input->post();
				$data = array(
					'title' => $post['title'],
					'group' => $post['group'],
					'importance' => $post['importance'],
					'description' => $post['description']
				 );
				$result = $this->support->addTicket($data);
				$this -> session -> set_flashdata('success', 'Тикет успешно создан!');
				$data = array(
					'message' => 'Тикет успешно создан!'
				);
				$result = array(
					'status'=>'ok',
					'data'=> $this -> template -> build('modules/success', $data, true)
				);
			}
			echo json_encode($result);
		} else {
			$result = array(
				'status'=>'edit',
				'data'=> $this -> template -> title('Техническая поддержка') -> build('modules/support/tickets_add', $data, true)
			);
			echo json_encode($result);
		}
	}

	public function tickets_message($pid) {
		$pid = $pid[1];
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		if ($this -> input -> post('status')=='save') {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('text', 'Текст', 'required');
			$this->form_validation->set_message('required', '"%s" - обязательное поле');
			if ($this -> form_validation -> run() == FALSE) {
				$result = array(
					'status'=>'edit',
					'data'=> $this -> template -> title('Техническая поддержка') -> build('modules/support/tickets_message', $data, true)
				);
			} else {
				$post = $this->input->post();
				$pid = $post['pid'];
				$data = array(
					'text' => $post['text']
				 );
				$result = $this->support->addMessage($pid, $data);
				$this->support->updateTicketStatus($pid,$post['m_status']);
				$this -> session -> set_flashdata('success', 'Сообщение успешно добавлено!');
				$data = array(
					'message' => 'Сообщение успешно добавлено!'
				);
				$result = array(
					'status'=>'ok',
					'data'=> $this -> template -> build('modules/success', $data, true)
				);
			}
			echo json_encode($result);
		} else {
			$data['pid'] = $pid;
			$result = array(
				'status'=>'edit',
				'data'=> $this -> template -> title('Техническая поддержка') -> build('modules/support/tickets_message', $data, true)
			);
			echo json_encode($result);
		}
	}

	public function tickets_delete() {
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$post = $this -> input -> post();
		if (isset($post['id'])) {
			$this->support->deleteTicket($post['id']);
			$data = array(
				'message' => 'Тикет успешно удален'
			);
			$result = array(
				'status'=>'ok',
				'data'=> $this -> template -> build('modules/success', $data, true)
			);
			echo json_encode($result);
		}
		
	}
	
	public function ticket($id) {
		$data['ticket'] = $this -> support -> ticket($id);
    $this->support->updateVisit($id, $this->userInfo->id);
    
    $data['unread'] = $this->support->unreadTickets($this->userInfo->id);
    $this->template->inject_partial('unread',$this->load->view('unread',$data,true));
    
		$this -> template -> set('subtitle', $data['ticket']->title);
		$this -> template -> title('Техническая поддержка') -> build('modules/support/ticket', $data);
	}
	
	public function groups($action="index") {
		if (!$this->ion_auth->is_admin()) {
			redirect('/');
		}
		if ($action!="index") {
			$args = func_get_args();
			unset($args[0]);
			call_user_func(array($this,'groups_'.$action),$args);
			return;
		}
		
		$this -> template -> set('subtitle', 'Группы запросов');
		$data['groups'] = $this -> support -> groups();
		$this -> template -> title('Техническая поддержка') -> build('modules/support/groups', $data);
	}

	public function groups_add() {
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		if ($this -> input -> post('status')=='save') {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('title', 'Имя', 'required');
			$this -> form_validation -> set_rules('alias', 'Alias', 'required');
			$this->form_validation->set_message('required', '"%s" - обязательное поле');
			if ($this -> form_validation -> run() == FALSE) {
				$result = array(
					'status'=>'edit',
					'data'=> $this -> template -> title('Техническая поддержка') -> build('modules/support/groups_edit', array(), true)
				);
			} else {
				$post = $this->input->post();
				$data = array(
					'title' => $post['title'],
					'alias' => $post['alias'],
					'description' => $post['description']
				 );
				$result = $this->support->addGroup($data);
				$this -> session -> set_flashdata('success', 'Группа успешно создана.');
				$data = array(
					'message' => 'Группа успешно создана'
				);
				$result = array(
					'status'=>'ok',
					'data'=> $this -> template -> build('modules/success', $data, true)
				);
			}
			echo json_encode($result);
		} else {
			$result = array(
				'status'=>'edit',
				'data'=> $this -> template -> title('Техническая поддержка') -> build('modules/support/groups_edit', array(), true)
			);
			echo json_encode($result);
		}
	}
	
	public function groups_edit() {
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$post = $this -> input -> post();
		if ($this -> input -> post('status')=='save') {
			$this -> form_validation -> set_rules('title', 'Имя', 'required');
			$this -> form_validation -> set_rules('alias', 'Alias', 'required');
			$this->form_validation->set_message('required', '"%s" - обязательное поле');
			if ($this -> form_validation -> run() == FALSE) {
				$result = array(
					'status'=>'edit',
					'data'=> $this -> template -> title('Техническая поддержка') -> build('modules/support/groups_edit', array(), true)
				);
			} else {
				$post = $this->input->post();
				$data = array(
					'title' => $post['title'],
					'alias' => $post['alias'],
					'description' => $post['description']
				 );
				$result = $this->support->updateGroup($post['id'],$data);
				$this -> session -> set_flashdata('success', 'Группа успешно изменена.');
				$data = array(
					'message' => 'Группа успешно изменена'
				);
				$result = array(
					'status'=>'ok',
					'data'=> $this -> template -> build('modules/success', $data, true)
				);
			}
			echo json_encode($result);
		} else {
			$data['group'] = $this->support->group($post['id']);
			$result = array(
				'status'=>'edit',
				'data'=> $this -> template -> title('Техническая поддержка') -> build('modules/support/groups_edit', $data, true)
			);
			echo json_encode($result);
		}
	}

	public function groups_delete() {
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$post = $this -> input -> post();
		if (isset($post['id'])) {
			$this->support->deleteGroup($post['id']);
			$data = array(
				'message' => 'Группа успешно удалена'
			);
			$result = array(
				'status'=>'ok',
				'data'=> $this -> template -> build('modules/success', $data, true)
			);
			echo json_encode($result);
		}
		
	}

	public function edit() {
		$data['user']=$this->users->get(1,0,1,$this->input->post('id'));
		$data['groups']=$groups = $this->ion_auth->groups()->result();
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
				
		if ($this -> input -> post('status')=='save') {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('first_name', 'Имя', 'required');
			$this -> form_validation -> set_rules('email', 'E-mail', 'required|email');
			if ($this->input->post('password')!='') {
				$this->form_validation->set_rules('password', 'Пароль', 'matches[passconf]');
				$this->form_validation->set_rules('passconf', 'Подтверждение пароля', '');
				$this->form_validation->set_message('matches', 'Пароль и его подтверждение не совпадают');	
			}
			$this->form_validation->set_message('required', '"%s" - обязательное поле');
			if ($this -> form_validation -> run() == FALSE) {
				$result = array(
					'status'=>'edit',
					'data'=> $this -> template -> build('modules/users/edit', $data, true)
				);
			} else {
				$post = $this->input->post();
				$data = array(
					'first_name' => $post['first_name'],
					'email' => $post['email'],
					'phone' => $post['phone'],
					'username' => $post['email']
				 );
				 
				 if ($post['password']!='') {
				 	$data['password'] = $post['password'];
				 }
				$result = $this->ion_auth->update($post['id'], $data);
				$this->users->updateGroups($post['id'],$post['group']);
				$this -> session -> set_flashdata('success', 'Профиль успешно изменен.');
				$result = array(
					'status'=>'ok',
					'data'=> $this -> template -> build('modules/users/edit_success', $data, true)
				);
			}
			echo json_encode($result);
		} else {
			$result = array(
				'status'=>'edit',
				'data'=> $this -> template -> build('modules/users/edit', $data, true)
			);
			echo json_encode($result);
		}
	}
	
	public function lock() {
		$post = $this->input->post();
		$this->users->lock($post['id']);
		$result = array(
			'status'=>'ok',
			'data'=> '<div class="alert alert-success">Пользователь успешно заблокирован</div><META HTTP-EQUIV="REFRESH" CONTENT="1">'
		);
		echo json_encode($result);
	}
	
	public function unlock() {
		$post = $this->input->post();
		$this->users->lock($post['id'],1);
		$result = array(
			'status'=>'ok',
			'data'=> '<div class="alert alert-success">Пользователь успешно разблокирован</div><META HTTP-EQUIV="REFRESH" CONTENT="1">'
		);
		echo json_encode($result);
	}
  
  public function test() {
    $this->support->setVisitsBase();
  }
}
