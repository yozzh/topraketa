<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

//include APPPATH.'controllers/projects.php';

class Calc  extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this -> load -> model('projectsModel', 'projects');
		$this->load->model('linksmodel','links');
		$this -> load -> helper('html');
	}

	function keywords_list($list) {
		//return false;
		$result = ($list != "");
		if (!$result) {
			$this -> form_validation -> set_message(__FUNCTION__, "Список запросов не должен быть пустым. Добавьте хотя бы один запрос");
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function addKeywords() {
		$post = $this -> input -> post();
		$keywords = explode("\n", $post['keywords_01']);
		$data['data'] = array();
		foreach ($keywords as $keyword) {
			$tmpKeyword = array();
			$tmpKeyword['title'] = $keyword;
			$tmpKeyword['ya_region'] = ($post['ya_region_hidden'] == '') ? 213 : $post['ya_region_hidden'];
			$tmpKeyword['ya_region2'] = ($post['ya_region_hidden2'] == '') ? 213 : $post['ya_region_hidden2'];
			$tmpKeyword['ya_region_title'] = isset($post['ya_region']) ? $post['ya_region'] : 'Москва';
			$tmpKeyword['g_region'] = isset($post['g_region']) ? $post['g_region'] : 0;
			$tmpKeyword['g_region_title'] = isset($post['g_region']) ? $post['g_region'] : 'Россия';
			$tmpKeyword['visits'] = 0;
			$tmpKeyword['views'] = 0;
			//var_dump($tmpKeyword['ya_region']);
			$budjet = $tmpKeyword['budget'] = $this -> hammermodel -> getRegionBudget($tmpKeyword['title'], $tmpKeyword['ya_region2']);
			$tmpKeyword['page'] = '/';
			if ($tmpKeyword['title'] != "") {
				$data['data'][] = $tmpKeyword;
			}

		}
		$data['result'] = (!empty($data['data'])) ? 'success' : 'error';
		echo json_encode($data);
	}
}
