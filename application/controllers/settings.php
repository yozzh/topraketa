<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

	private $userInfo;
	public function __construct()
   	{
        parent::__construct();
		if (!$this->ion_auth->logged_in()) {
			redirect('/main/login');
		}
		
		$this->load->model('menu');
		$this->load->model('settingsmodel','settings');
		$this->load->model('partnershipmodel','partnership');
		
		$subMenuItems = new menuItem('additional-menu','/settings');
		$subMenu = $subMenuItems->generate();
		$this->userInfo = $this->ion_auth->user()->row();
		$this->template->set(array(
			'tPath'=>'/include/frontend/',
			'subMenu' => $subMenu,
			'user' => $this->userInfo
		));
		
        $this -> template -> set_theme('frontend_second');
		$this->template->set_layout('default');		
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$data=array();
		if ($this -> input -> post()) {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('first_name', 'Имя', 'required');
			$this -> form_validation -> set_rules('email', 'E-mail', 'required|email');
			if ($this->input->post('password')!='') {
				$this->form_validation->set_rules('password', 'Пароль', 'matches[passconf]');
				$this->form_validation->set_rules('passconf', 'Подтверждение пароля', '');
				$this->form_validation->set_message('matches', 'Пароль и его подтверждение не совпадают');	
			}
			$this->form_validation->set_message('required', '"%s" - обязательное поле');
			

			if ($this -> form_validation -> run() == FALSE) {
				$this -> template -> set('subtitle',$this->userInfo->first_name.' ('.$this->userInfo->email.')');
				$this->template->title('Настройки профиля')->build('modules/settings/index',$data);
			} else {
				$post = $this->input->post();
				$data = array(
					'first_name' => $post['first_name'],
					'email' => $post['email'],
					'phone' => $post['phone'],
					'username' => $post['email']
				 );
				 
				 if ($post['password']!='') {
				 	$data['password'] = $post['password'];
				 }
				$result = $this->ion_auth->update($post['user_id'], $data);
				$this -> session -> set_flashdata('success', 'Профиль успешно изменен.');
				redirect('/settings/');
			}
		} else {
			$data['partnershipLink']=$this->partnership->link();
			$data['clients']=$this->partnership->getClients();
			$this -> template -> set('subtitle',$this->userInfo->first_name.' ('.$this->userInfo->email.')');
			$this->template->title('Настройки профиля')->build('modules/settings/index',$data);	
		}
	}
	
	public function edit($project_id=0)
	{
		$this->load->model('projectsModel','projects');
		
		
		if ($this->input->post()) {
			$post = $this->input->post();
			$result = $this->orders->save($post);
			redirect('/orders/');
		} else {
			$data['project_id']=$project_id;
			$data['projects']=$this->projects->items();
			$data['products']=$this->products->items();
			$this->template->title('Заказы')->build('modules/orders/add',$data);	
		}
	}
}