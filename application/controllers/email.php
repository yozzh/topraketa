<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
		$this->load->model('subscribemodel','subscribe');
	}
	
	public function test() {
		echo $this->subscribe->letter('index');
	}
}