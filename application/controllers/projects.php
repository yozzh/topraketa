<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Projects extends CI_Controller {
	private $userInfo;
	
	public function __construct() {
		parent::__construct();
		
		if (!$this -> ion_auth -> logged_in()) {
			redirect('/');
		}
		$this -> load -> model('menu');
		$this -> load -> model('projectsModel', 'projects');
		$this->load->model('linksmodel','links');
		$this -> load -> helper('html' );
		$this->load->helper('projects');

		$this -> menu -> setActive('projects');
		$mainMenu = $this -> menu -> menuItems -> generate();

		$subMenuItems = new menuItem('additional-menu', '/projects');
		$subMenuItems -> addChild('Все проекты', '', 'icon-eye-open');
		$subMenuItems -> addChild('Добавить проект', 'add', 'icon-plus');
		$subMenu = $subMenuItems -> generate();
		$this->userInfo = $this -> ion_auth -> user() -> row();
		$this -> template -> set(array('tPath' => '/include/frontend/', 'mainMenu' => $mainMenu, 'subMenu' => $subMenu, 'user' => $this->userInfo));

		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('default');
		$this -> template -> set_partial('header', 'partials/header');
		$this -> template -> set_partial('footer', 'partials/footer');
	}

	public function index() {
		$this -> template -> set('subtitle', 'Список проектов');
		$data['items'] = $this -> projects -> items();
		$ids = array();
		foreach ($data['items'] as $item) {
			$ids[] = $item -> hm_project_id;
		}
		$projects = $this -> hammermodel -> getProjects($ids);
		
		foreach ($projects as &$project) {
			$project['linkbudget'] = $this->benefit->calcKeywordBudget($project['linkbudget'],true);
			$project['linkcost'] = round($this->projects->getProjectCost($project['id'])/30,2);
		}
		
		$data['items'] = $projects;
		
		$frozen = $this->projects->frozen();
		
		if (is_array($frozen)) {
			foreach ($frozen as &$project) {
				$project->budget = $this->benefit->calcKeywordBudget($project->budget,true);
			}
		}
		$data['frozen'] = $frozen;
		
		$this -> template -> title('Проекты') -> build('modules/projects/index', $data);
		
	}

	public function add() {
		$scripts[] = 'add.js';
		$this -> template -> set('scripts', $scripts);
		$this -> template -> set('subtitle', 'Новый проект');
		$errors = array();
		if ($this -> input -> post()) {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('domain', 'Домен', 'required|valid_domain');
			$this -> form_validation -> set_rules('keywords_02', 'Список запросов', 'callback_keywords_list');
			$this -> form_validation -> set_message('required', '"%s" - обязательное поле');
			
			$userBalance = $this -> money -> calcUserMoney( $this->userInfo->id);
			
			if ($this -> form_validation -> run() == FALSE || count($errors)) {
				$data['errors']=$errors;
				$this -> template -> title('Проекты') -> build('modules/projects/add',$data);
			} else {
				$keywords = json_decode($post['keywords_02']);
				$tmpReg = 0;
				if ($userBalance > 0) {
					$hmId = $this -> hammermodel -> addProject($post['domain']);
					foreach ($keywords as $keyword) {
						$newBudget = $this->benefit->calcKeywordBudget($keyword->budget);
						$this -> hammermodel -> addKeyword($hmId, $keyword -> title, $keyword -> page, $newBudget, $keyword -> ya_region, $keyword -> g_region);
					}
					$post['hm_project_id'] = $hmId;
					$result = $this -> projects -> save($post);
					$this -> session -> set_flashdata('success', 'Проект успешно добавлен.');
					redirect('/projects/');	
				} else {
					$post['hm_project_id'] = -1;
					$post['status'] = 3;
					$projectId = $this -> projects -> save($post);
					foreach ($keywords as $keyword) {
						$newBudget = $this->benefit->calcKeywordBudget($keyword->budget);
						$this -> projects -> addTempKeyword($projectId, $keyword -> title, $keyword -> page, $newBudget, $keyword -> ya_region, $keyword -> g_region);
					}
					$this -> session -> set_flashdata('warning', 'Проект добавлен, но не запущен. Пополните баланс для успешного запуска проекта!');
					redirect('/projects/');
				}
				
				
				
			}

		} else {
			$this -> template -> title('Проекты') -> build('modules/projects/add');
		}
	}

	function startFrozen($projectId) {
		$result = $this->projects->startFrozen($projectId);
		if ($result) {
			$this -> session -> set_flashdata('success', 'Проект успешно добавлен.');
		} else {
			$this -> session -> set_flashdata('warning', 'Проект не запущен. Пополните баланс для успешного запуска проекта!');
		}
		redirect('/projects/');
	}

	function keywords_list($list) {
		//return false;
		$result = ($list != "");
		if (!$result) {
			$this -> form_validation -> set_message(__FUNCTION__, "Список запросов не должен быть пустым. Добавьте хотя бы один запрос");
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function removeKeyword() {
		$post = $this -> input -> post();
		$id = $post['id'];
		$result = $this -> hammermodel -> archiveKeyword($id);
    var_dump($result);
		$this -> session -> set_flashdata('success', 'Ключ успешно удален');
	}

	public function editKeyword() {
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$id = $this -> input -> post('id');
		if ($this -> input -> post('status') && $this -> input -> post('status') == 'save') {
			$post = $this -> input -> post();
			if ($post['budget']>=10) {
				$newBudget = $this->benefit->calcKeywordBudget($post['budget']);
				$this -> hammermodel -> updateKey($post['id'], $post['url'], $newBudget, $post['ya_region_hidden'], $post['g_region_hidden']);
				$this -> session -> set_flashdata('success', 'Информация о ключевом слове успешно обновлена<br/>В ближайшее время изменения вступят в силу.');
				$data['result'] = 'success';
				echo json_encode($data);	
			} else {
				$data['result'] = 'error';
				$data['body'] = 'Бюджет запроса не может быть менее 10 рублей!';
				echo json_encode($data);
			} 
		} else {
			$keyword = $this -> hammermodel -> getKey($id);
			$keyword['linkbudget'] = $this->benefit->calcKeywordBudget($keyword['linkbudget'],true);
			$this -> setRegionTitle($keyword);
			$data['keyword'] = $keyword;
			echo $this -> template -> build('modules/projects/keyword_edit', $data, true);
		}
	}

	public function edit($id) {
		if ($this -> input -> post()) {
			$post = $this -> input -> post();

			$result = $this -> projects -> update($post, $id);
			redirect('/projects/');
		} else {
			$data['project'] = $this -> projects -> item($id);
			$this -> template -> title('Проекты') -> build('modules/projects/edit', $data);
		}
	}

	public function delete() {
		$id = $this->input->post('id');
		if ($id) {
			$result = $this -> projects -> userDelete($id, false);
			if ($result) {
				$this -> session -> set_flashdata('success', 'Проект успешно удален.');
				$data['result'] = 'success';	
			} else {
				$this -> session -> set_flashdata('warning', 'Проект не найден.');
				$data['result'] = 'warning';
			}
			
			echo json_encode($data);
		}
	}
	
	public function deleteFrozen($id) {
		$result = $this -> projects -> userDelete($id, true);
		if ($result) {
				$this -> session -> set_flashdata('success', 'Проект успешно удален.');	
			} else {
				$this -> session -> set_flashdata('warning', 'Проект не найден.');
			}
		redirect('/projects/');
	}
	
	public function freeze($id) {
		$this->projects->freeze($id,10);
		$this -> session -> set_flashdata('success', 'Проект успешно заморожен.');
		redirect ('/projects');
	}
	
	

	public function project($id) {
		if (!$this->projects->checkUsersProject($id)) {
			redirect('/projects/');
		}
		$scripts[] = 'add.js';
		$this -> template -> set('scripts', $scripts);
		$subMenuItems = new menuItem('additional-menu', '/projects');
		$subMenuItems -> addChild('Все проекты', '', 'icon-eye-open');
		$subMenuItems -> addChild('Добавить ключи', 'addNewKeywords/' . $id, 'icon-plus');
		$subMenu = $subMenuItems -> generate();
		$this -> template -> set(array('subMenu' => $subMenu));

		$ids = array($id);
		$project = $this -> hammermodel -> getProjects($ids);
		$project = $project[0];
		//$keywords = $this -> hammermodel -> getKeys($id);
		$keywords = $this->projects->getKeys($id);
		$project['keywords'] = array();
		foreach ($keywords as $keyword) {
			$this -> setRegionTitle($keyword);
			$project['keywords'][] = $keyword;
		}

		$this -> template -> set('subtitle', $project['domain']);
		$data['project'] = $project;
		$this -> template -> title($project['domain']) -> build('modules/projects/project', $data);
	}

	public function frozen($id) {
		if (!$this->projects->checkUsersProject($id, true)) {
			redirect('/projects/');
		}
		
		$scripts[] = 'add.js';
		$this -> template -> set('scripts', $scripts);
		$subMenuItems = new menuItem('additional-menu', '/projects');
		$subMenuItems -> addChild('Все проекты', '', 'icon-eye-open');
		//$subMenuItems -> addChild('Добавить ключи', 'addNewKeywords/' . $id, 'icon-plus');
		$subMenu = $subMenuItems -> generate();
		$this -> template -> set(array('subMenu' => $subMenu));
		
		$project = $this -> projects -> item($id, true);
		$this -> template -> set('subtitle', $project->url);
		$data['project'] = $project;
		$this -> template -> title($project->url) -> build('modules/projects/frozen', $data);
	}

	private function setRegionTitle(&$keyword) {
		$yregion = $this -> regions -> getYaRegionById($keyword['ygeo']);
		$gregion = $this -> regions -> getGoogleRegionById($keyword['ggeo']);
		$keyword['ygeotitle'] = ($yregion) ? $yregion -> name : 'Не определен (' . $keyword['ygeo'] . ')';
		$keyword['ggeotitle'] = ($gregion) ? $gregion -> name : 'Не определен (' . $keyword['ggeo'] . ')';
	}

	public function addNewKeywords($projectId) {
		$scripts[] = 'add.js';
		$this -> template -> set('scripts', $scripts);
		$this -> template -> set('subtitle', 'Добавление запросов');

		$ids = array($projectId);
		$project = $this -> hammermodel -> getProjects($ids);
		$project = $project[0];

		if ($this -> input -> post()) {
			$post = $this -> input -> post();
			$this -> form_validation -> set_rules('keywords_02', 'Список запросов', 'callback_keywords_list');
			$this -> form_validation -> set_message('required', '"%s" - обязательное поле');

			if ($this -> form_validation -> run() == FALSE) {
				$data['projectId'] = $projectId;
				$data['projectDomain'] = $project['domain'];
				$this -> template -> title($project['domain']) -> build('modules/projects/add', $data);
			} else {
				$hmId = $projectId;
				$keywords = json_decode($post['keywords_02']);
				$tmpReg = 0;
				foreach ($keywords as $keyword) {
					$newBudget = $this->benefit->calcKeywordBudget($keyword->budget);
					$this -> hammermodel -> addKeyword($hmId, $keyword -> title, $keyword -> page, $newBudget, $keyword -> ya_region);
					$tmpReg = $keyword -> ya_region;
				}
				$this -> session -> set_flashdata('success', 'Запросы успешно добавлены.');
				redirect('/projects/project/' . $projectId);
			}

		} else {
			$data['projectId'] = $projectId;
			$data['projectDomain'] = $project['domain'];
			$this -> template -> title($project['domain']) -> build('modules/projects/add', $data);
		}

	}

	public function links($projectId, $keywordId = 0) {
		$data['items'] = array();
		$links = $this->links->get($projectId);
		if ($keywordId != 0) {
			foreach ($links as $link) {
				if ($link['keyid']==$keywordId) {
					$data['items'][]=$link;
				}
			}
		} else {
			$data['items'] = $links;
		}
		$this -> template -> set('subtitle', 'Ссылки проекта');		
		$this -> template -> title('Ссылки проекта') -> build('modules/projects/links', $data);
	}
	
	
	
	public function deleteLink() {
		$linkId = $this->input->post('id');
		$this -> session -> set_flashdata('success', 'Ссылка будет удалена в течение 3-5 минут.');
		$this->links->delete($linkId);
				
	}
	
	public function compare() {
		$this->db->where('hm_project_id > 0');
		$this->db->where_not_in('');
		$query = $this->db->get('projects');
		$pn = $query->result();
		var_dump(count($pn));
		
		$in = array();
		foreach ($pn as $p) {
			$in[]=$p->hm_project_id;
		}
		
		$in = array_unique($in);
		
		$this->db->where('hm_project_id > 0');
		$this->db->where_not_in('hm_project_id',$in);
		$this->db->join('users', 'users.id = projects_origin.user_id');
		$query = $this->db->get('projects_origin');
		$po = $query->result();
		
		foreach ($po as $p) {
			echo $p->url.' - '.$p->hm_project_id.' - '.$p->email.'<br/>';
		}
		var_dump(count($po));
		
		
		
		
	}
}
