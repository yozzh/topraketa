<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Services extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this -> load -> model('regionsmodel','regions');
	}

	public function getYaRegions() {
		$regions = $this -> regions -> getYaRegions('json');
		echo $regions;
	}
	
	public function getGoogleRegions() {
		$regions = $this -> regions -> getGoogleRegions('json');
		echo $regions;
	}
	
	public function getMainpageKeywords() {
		$keywords = $this->settingsmodel->get('keywordsItems');
		echo $keywords;
	}

}
?>

