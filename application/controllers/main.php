<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	private $captcha;

	public function __construct()
   	{
        parent::__construct();
        $userInfo = $this->ion_auth->user()->row();
		
		$this->load->model('pagesModel','pages');
		$this->template->set_theme('frontend_second');
		$this->template->set_layout('default');
		$this->load->model('menu');
		
		
		$this->template->set(array('tPath'=>'/include/frontend/'));
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
		
		$this->load->model('pagesModel','pages');
		
		$this->captcha = $this->pages->createCaptcha();	
		
		if (count($userInfo)) {
			
			$this->menu->setActive('/main');
			$mainMenu = $this->menu->menuItems->generate();
			
			$this->template->set(array(
				'mainMenu' => $mainMenu,
				'user' => $userInfo,
				'captcha' => $this->captcha
			));
		} else {
			//$this->pages->loadInformer();
		}
   	}
	
	public function index() {
		if (!$this->ion_auth->logged_in()) {
			//redirect('/main/login');
		}
		$this->template->set_layout('default');
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
		
		$this->load->model('ordersModel','orders');
		$this->load->model('projectsModel','projects');
		$this->load->helper('orders');
		$data['projects'] =$this->projects->items(5);
		$data['orders'] = $this->orders->items(5);
		
		$this->template->title('Личный кабинет')->build('modules/main/index',$data);
	}
	
	public function login()
	{
		$post=$this->input->post();
		
		if (isset($post['ajax']) && $post['ajax']=='true') {
			$this->ajaxLogin();
		} else {
			if ($this->ion_auth->logged_in() || $this->ion_auth->login($this->input->post('login_email'), $this->input->post('login_password'), true)) {
				redirect('/');
			} else {
				$this->template->set_layout('login');
				$this->template->title('Вход в систему')->build('modules/main/login');	
			}
		}
	}
	
	private function ajaxLogin() {
		$data=array();
		if ($this->ion_auth->logged_in() || $this->ion_auth->login($this->input->post('login_email'), $this->input->post('login_password'), true)) {
			$data['result']='success';
			$data['html']=$this->pages->loginSuccess();
		} else {
			$data['result']='error';
			$data['html']=$this->load->view('auth',array('error'=>true, 'email'=>$this->input->post('email')),true);
		}
				
		echo json_encode($data);
	}
	
	public function registration() {
		if ($this->ion_auth->logged_in()) {
			redirect('/projects');
		} else {
			
			if ($this->input->post()) {
				$this->form_validation->set_rules('name', 'Имя', 'required');
				$this->form_validation->set_rules('password', 'Пароль', 'required|matches[passconf]');
				$this->form_validation->set_rules('passconf', 'Подтверждение пароля', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required|email');
				$this->form_validation->set_rules('phone', 'Телефон', 'phone_validate');
				
				$this->form_validation->set_message('required', '"%s" - обязательное поле');
				$this->form_validation->set_message('matches', 'Пароль и его подтверждение не совпадают');
				$this->form_validation->set_message('alpha_dash', '"%s" - поле должно быть числом');
				
				if ($this->form_validation->run() == FALSE)
				{
					$this->template->title('Регистрация в системе')->build('modules/main/registration');
				}
				else
				{
					$addins=array();
					$addins['first_name'] = $this->input->post('name');
					$partnerId = $this->session->userdata('partnerId');
					if ($partnerId) {
						$addins['partner_id']=$partnerId;
					}
					$username = $this->input->post('email');
					$email = $this->input->post('email');
					$password = $this->input->post('password');
					
					if ( $this->ion_auth->register($username, $password, $email,$addins)) 
					{
						$this->ion_auth->login($username, $password, true);
						$this->session->set_flashdata('message', "Вы зарегистрированы!");
						redirect("/projects/add");
					} else {
						$data['errors'][]="Пользователь с данным почтовым ящиком уже зарегистрирован в системе";
						$this->template->title('Регистрация в системе')->build('modules/main/registration', $data);
					}
				}
			} else {
				$this->template->title('Регистрация в системе')->build('modules/main/registration');	
			}
			
		}
	}

	public function error404() {
		$this->template->title('404 - Страница не найдена')->build('modules/main/404');	
	}

	public function forgot() {
		if ($this->ion_auth->logged_in()) {
			redirect('/projects');
		} else {
			if ($this->input->post()) {
				$this->form_validation->set_rules('email', 'Email', 'required');
				$this->form_validation->set_message('required', '"%s" - обязательное поле');
				
				if ($this->form_validation->run() == FALSE)
				{
					$this->template->title('Восстановление пароля')->build('modules/main/forgot');
				}
				else
				{
					$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));
					if ($forgotten) { //if there were no errors
						$this->session->set_flashdata('message', $this->ion_auth->messages());
					
						$this -> template -> set_theme('frontend_second');
						$this -> template -> set_layout('ajax');
						$body = $this->template->build('modules/main/email/forgot',$forgotten, true);
						$this->subscribemodel->send($forgotten['identity'],'Восстановление пароля', $body);
						
						$this->template->set_theme('frontend_second');
						$this->template->set_layout('default');
						$this->template->title('Восстановление пароля')->build('modules/main/forgot_send');
					}
					else {
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect("/main/forgot", 'refresh');
					}
				}
			} else {
				$this->template->title('Восстановление пароля')->build('modules/main/forgot');
			}
		}
	}

	public function code($code) {
		$reset = $this->ion_auth->forgotten_password_complete($code);

		if ($reset) {  //if the reset worked then send them to the login page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			$this -> template -> set_theme('frontend_second');
			$this -> template -> set_layout('ajax');
			$body = $this->template->build('modules/main/email/forgot_success',array('newPassword'=>$reset['new_password']), true);
			$this->subscribemodel->send($reset['identity'],'Восстановление пароля', $body);
			
			$this->template->set_theme('frontend_second');
			$this->template->set_layout('default');
			$this->template->title('Восстановление пароля')->build('modules/main/forgot_success');
		}
		else { //if the reset didnt work then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}
	
	public function pay($orderId) {
		$this->load->model('moneymodel','money');
		$this->load->library('robokassa');
		
		$this->robokassa->inv_id = $orderId;
		$payment = $this->money->payment($orderId, false);
		
		if (!$payment) {
			$this->error404();
			return;
		}
		
		if ($payment->status == 1) {
			$this->robokassa->out_summ = $payment->value;
	        $data = array(
	            'payform' => $this->robokassa->layout(),
	            'default_value' => (int)$payment->value
	        );
			
			$this->template->set_theme('frontend_second');
			$this->template->set_layout('default');
			$this->template->title('Пополнение лицевого счета')->build('modules/main/pay',$data);
		} else {
			$this->error404();
		}
	}
	
	public function logout()
	{
		$this->ion_auth->logout();
		redirect('/');
	}
	
	/**
	 * Switch back to admin user
	 *
	 * @return bool
	 * @author  
	 */
	function unmagicEye() {
		$this->ion_auth->unmagicEye();
		redirect('/users');
	}
	
}