<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
		$this->load->model('pagesModel','pages');
		$this -> load -> model('menu');
		$this->template->set_theme('frontend');
		
		$this->template->set_layout('default');
		$this->template->set(array('tPath'=>'/include/frontend/'));
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$this->template->set('callback',$this->load->view('callback',array(),true));
		$this->template->title('Сервис эффективного продвижения ваших сайтов!')->build('pages/index');
	}
	
  public function help($url) {
     $userInfo = $this->ion_auth->user()->row();
    $this->template->set_theme('frontend_second');
    $this->template->set_layout('default');
    $this->load->model('menu');
    
    
    $this->template->set(array('tPath'=>'/include/frontend/'));
    $this->template->set_partial('header','partials/header');
    $this->template->set_partial('footer','partials/footer');
    
    $this->load->model('pagesModel','pages'); 
    
    if (count($userInfo)) {
      
      $this->menu->setActive('/main');
      $mainMenu = $this->menu->menuItems->generate();
      
      $this->template->set(array(
        'mainMenu' => $mainMenu,
        'user' => $userInfo
      ));
    }
    $data['page']=$this->pages->page($url,'url');
    $data['menu']=$this->pages->pages();
    $this->template->title('Справочная информация')->build('pages/help',$data);
  }
}