<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class error404 extends CI_Controller {

	public function __construct()
   	{
        parent::__construct();
		$this->load->model('pagesModel','pages');
		
		$this->template->set_theme('frontend');
		
		$this->template->set_layout('404');
		$this->template->set(array('tPath'=>'/include/frontend/'));
		$this->pages->loadInformer();
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('footer','partials/footer');
   	}
	
	public function index()
	{
		$data['message'] = 'К сожалению, ваша страница не может быть отображена';
		$this->template->title('Регистрация в системе')->build('modules/main/registration');
	}
	
}