<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cron extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->spark('template/1.9.0');
		$this->load->spark('ion_auth/2.4.0');
		$this -> load -> helper('file');
		$this -> load -> model('benefitmodel', 'benefit');
		$this -> load -> model('expensesmodel', 'expenses');
		$this -> load -> model('projectsmodel', 'projectsq');
		
	}

	public function updateYaRegions() {
		$regions = $this -> hammermodel -> getYaRegions();
		if (!write_file(APPPATH . 'data/ya_regions.json', json_encode($regions), 'w+')) {
			echo 'Unable to write the file';
		} else {
			echo 'File written!';
		}
	}

	public function updateGoogleRegions() {
		$regions = $this -> hammermodel -> getGoogleRegions();
		if (!write_file(APPPATH . 'data/google_regions.json', json_encode($regions), 'w+')) {
			echo 'Unable to write the file';
		} else {
			echo 'File written!';
		}
	}
	
	public function updateExpenses() {
		$this->expenses->updateExpenses();
	}
	
	public function startCheckBalance() {
		$users = $this->ion_auth->users();
		$users = $users->result();
		
		//TODO: Move this functions to separate Threads model
		$this->db->where('alias','checkBalance');
		$this->db->delete('threads');
		
		foreach ($users as $user) {
			$data = array(
				'alias'=>'checkBalance',
				'value'=>$user->id
			);
			$this->db->insert('threads',$data);
		}
	}
	
	public function checkBalance() {
		$this->expenses->checkBalance();
	}
	
	public function updateProjectsCost() {
		$this->projects->updateCosts();
	}
	
	public function getNullProjects() {
		$this->projects->getNull();
	}
	
	public function getLostProjects() {
		$this->db->where('hm_project_id > 0');
		$this->db->from('projects');
		$this->db->join('users','users.id = projects.user_id');
		$query = $this->db->get();
		
		$result = $query->result();
		$projectsArray = array();
		$projects = array();
		foreach ($result as $project) {
			/*$hmProject = $this -> hammermodel -> getProjects(array($project->hm_project_id));
			var_dump($hmProject);
			if (count($hmProject)==0) {
				echo "<br/><br/>";
				echo count($hmProject)." {$project->hm_project_id} <strong>{$project->url}</strong> ($project->email)<br/>";
				echo "Проект не найден в бирже";
				//var_dump($hmProject);
				echo "<br/><br/>";	
			}*/
			$projectsArray[$project->hm_project_id] = $project;
			$projects[]=$project->hm_project_id;
		}
		
		$hmProjects = $this -> hammermodel -> getProjects($projects);
		echo "<pre>";
		
		foreach ($hmProjects as $hmProject) {
			if(($key = array_search($hmProject['id'], $projects)) !== false) {
			    unset($projects[$key]);
			}
			
		}
		foreach ($projects as $key) {
			$project = $projectsArray[$key];
			echo "<br/><br/>";
			echo "{$project->id} {$project->hm_project_id} <strong>{$project->url}</strong> ($project->email)<br/>";
			echo "Проект не найден в бирже";
			var_dump($project);
			$this->db->where('hm_project_id',$project->hm_project_id);
			$this->db->delete('projects');
			echo "Проект удален";
			//var_dump($hmProject);
			echo "<br/><br/>";	
		}
		echo "</pre>";
	}
	
	public function startSyncProjects() {
		
		//TODO: Move this functions to separate Threads model
		$this->db->where('alias','syncProject');
		$this->db->delete('threads');
		
		$projects = $this->projects->items(1,-1,true);
		
		foreach ($projects as $project) {
			$data = array(
				'alias'=>'syncProject',
				'value'=>$project->id
			);
			$this->db->insert('threads',$data);
		}
	}
	
	public function syncProjects() {
		$this->projects->syncProjectThread();
	}
	
	public function syncProject() {
		$this->input->is_cli_request();
		$this->benefit->syncProject();
	}
	
	public function syncLinks($projectId) {
		$this->benefit->syncLinks($projectId);
	}
	
	public function keyrangetest($budget) {
		var_dump($this->benefit->calcKeywordBudget($budget,true));
	}
}
?>