<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $template['title']; ?> | Vk-group.ru</title>
        <meta name="description" content="">
		
		<link rel="stylesheet" href="<?=$tPath?>fonts/pt-sans.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
        <link rel="stylesheet" href="<?=$tPath?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=$tPath?>css/main.css">

        <script src="<?=$tPath?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- This code is taken from http://twitter.github.com/bootstrap/examples/hero.html -->

        <header id="head">
        	<div class="container">
        		<div class="row">
        			<section class="span6 offset3 text-center">
        				<a href="/"><img src="<?=$tPath?>img/logo.png" alt="" /></a>
        			</section>
        			<section class="span3">
        				<section class="informer">
        					<?=$template['partials']['informer']?>
        				</section>
        			</section>
        		</div>
        	</div>
        </header>
        <section class="mainMenu dynamicBg" id="mainMenu">
        	<div class="container">
        		<div class="row">
        			<section class="span12 menuContainer">
        				<div class="chain h2"></div>
        				<a href="/#" class="mainLink">О сервисе</a>
        				<div class="chain"></div>
        				<a href="/#cost" class="mainLink">Стоимость</a>
        				<div class="chain"></div>
        				<a href="/#tech" class="mainLink">Технология</a>
        				<div class="chain"></div>
        				<a href="/#comments" class="mainLink">Отзывы</a>
        				<div class="chain"></div>
        				<a href="/#contacts" class="mainLink">Контакты</a>
        				<div class="chain h2"></div>
        			</section>
        		</div>
        	</div>
        </section>
		<section class="costContainer dynamicBg" id="cost">
			<div class="container">
				<div class="row">
					<section class="span12">
						<h3><?php echo $template['title']; ?></h3>
						<p><?=$message?></p>
					</section>
				</div>
			</div>
		</section>
		<section class="contactContainer dynamicBg" id="contacts">
			<div class="container">
				<div class="row">
					<section class="span12">
						<h2>Обратный звонок</h2>
						<div class="callbackForm">
							<form action="#">
								<label for="name">Имя</label><input type="text" name="name" value="" id="name"/>
								<label for="phone">Телефон</label><input type="text" name="phone" value="" id="phone"/>
								<label for="email">E-mail</label><input type="email" name="email" value="" id="email"/>
								<input type="submit" value="Отправить" id=""/>
								
							</form>
							<div class="clearfix"></div>
						</div>
					</section>
				</div>
			</div>
		</section>
		<footer>
			<div class="container">
				<div class="row">
					<section class="span12 text-center">
						Все права защищены. 2009-2013
					</section>
				</div>
			</div>
		</footer>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?=$tPath?>js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
		<script src="<?=$tPath?>js/vendor/jquery-ui-1.10.3.custom.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$tPath?>js/vendor/jq.smooth.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?=$tPath?>js/vendor/bootstrap.min.js"></script>

        <script src="<?=$tPath?>js/main.js"></script>
    </body>
</html>
