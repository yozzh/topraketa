<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
	<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php echo $template['title']; ?> | Top-Raketa.ru</title>
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?=$tPath?>css/bootstrap.min.css">
		<link rel="stylesheet/less" type="text/css"" href="<?=$tPath?>css/main.less">
		<link rel="stylesheet" href="<?=$tPath?>js/vendor/jquery-loadmask-0.4/jquery.loadmask.css">
		<script src="<?=$tPath?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<link rel="icon" type="image/png" href="/include/favicon.png" />
	</head>
	<body class="mainPage">
		<div class="bodyWrapper">
			<header class="smartPart">
				<div class="headerWrapper" id="info">
					<div class="girlWrapper">
						<div class="container top">
							<div class="row">
								<section class="span5">
									<a href="/" class="logo"><img src="<?=$tPath?>img/logo.png" alt="" width="279" height="105" /></a>
								</section>
								<section class="span7">
									<?=$template['partials']['mainMenu']?>
								</section>
							</div>
						</div>
						<div class="infoWrapper">
							<div class="container">
								<div class="row">
									<section class="span9 offset3">
										<section class="informer" id="informer">
											<?=$template['partials']['informer']?>
										</section>
									</section>
								</div>
								<div class="row">
									<section class="span9 offset3">
										<section class="statistic" id="statistic">
											<?=$template['partials']['statistic']?>
										</section>
									</section>
								</div>
								<div class="row">
									<section class="offset3 span9">
										<div class="introText">
											<h1>Автоматический сервис эффективного продвижения сайтов</h1>
											<p>Представляем вам сервис для автоматического продвижения сайтов! Благодаря ему вы можете самостоятельно продвигать сайт своей компании, определять и полностью контролировать бюджет, избегая накруток и переплат.</p>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="mainParts" id="mainParts">
				<div class="whiteBg"></div>
				<section class="mainPart" id="calc">
					<div class="partHeader"></div>
					<div class="partMiddle">
						<div class="container">
							<div class="row">
								<section class="span12">
									<div class="partTitle shadow" style="margin-top: -35px;">
										Расчет стоимости
									</div>
								</section>
							</div>
							<div class="row">
								<section class="span4">
									<form action="/calc" method="post" accept-charset="utf-8">
										<div class="inputWrapper">
											<label for="domain">Адрес вашего сайта</label>
											<input type="text" name="domain" class="input-block-level" id="domain" placeholder="www.sitename.ru"/>
										</div>
										<div class="inputWrapper">
											<div class="control-group" id="selectYandexRegion">
												<label for="ya_is_region" class="checkbox">
													<input type="checkbox" name="ya_is_region" value="" id="ya_is_region"/>
													Регион продвижения (Яндекс/Google)</label>
												<input type="text" data-provide="typeahead" name="ya_region" id="ya_region" class="input-block-level" disabled="disabled" placeholder="Москва" autocomplete="off">
												<input type="hidden" name="ya_region_hidden" value="" id="ya_region_hidden"/>
												<input type="hidden" name="ya_region_hidden2" value="" id="ya_region_hidden2"/>
											</div>
										</div>
										<div class="inputWrapper">
										<label for="keywordsTotal">Итоговый список запросов</label>
										<textarea name="keywords_01" id="keywords_01" class="input-block-level" style="height:200px"></textarea>
									</div>
										<div class="inputWrapper">
											<a class="button blue addKeywords pull-right">
												Расчитать стоимость
											</a>
										</div>
									</form>
								</section>
								<section class="span8">
									<label class="keywordsFull">Итоговый список запросов</label>
									<div class="datagrid mainPage keywordsFull" id="keywords_02" style="display: none">
										<table id="keywordsTable" width="100%">
											<tbody>
											</tbody>
										</table>
									</div>
									<div class="totalDiv keywordsFull">
										<div class="preBudget">Итоговая стоимость</div>
										<div class="summaryBudgetMain"></div>
									</div>
									<div class="registration keywordsFull">
										Вы не можете проверить более 10 запросов.<br/>
										<a href="/main/registration">Зарегистрируйтесь</a>, чтобы взойти на борт "Ракеты"!
									</div>
									<div class="text-left keywordsEmpty" id="keywords_02_empty">
										<img src="/include/frontend/img/calc_bg.png" alt="" style="margin-top:20px;"/>
									</div>
									<input type="hidden" name="keywords_02" value="<?=set_value('keywords_02')?>" id="keywordsInput_02"/>
								</section>
							</div>
						</div>
					</div>
					<div class="partFooter"></div>
				</section>
				<section class="mainPart part02 smartPart" id="presentation">
					<div class="container">
						<div class="row">
							<section class="span11">
								<div class="partTitle">
									Презентация сервиса
									<div class="informerButtons">
										<a class="icon_presentation" href="/include/docs/presentation.pdf" target="_blank">Скачать PDF</a>
									</div>
								</div>
							</section>
						</div>
						<div class="row">
							<section class="span12">
								<div class="bigSlider">
									<div id="bigCarousel" class="carousel slide">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="active item">
												<div class="sliderImage" style="background-image: url('<?=$tPath?>img/slider/s01.jpg')"></div>
											</div>
											<div class="item">
												<div class="sliderImage" style="background-image: url('<?=$tPath?>img/slider/s02.jpg')"></div>
											</div>
											<div class="item">
												<div class="sliderImage" style="background-image: url('<?=$tPath?>img/slider/s03.jpg')"></div>
											</div>
											<div class="item">
												<div class="sliderImage" style="background-image: url('<?=$tPath?>img/slider/s04.jpg')"></div>
											</div>
											<div class="item">
												<div class="sliderImage" style="background-image: url('<?=$tPath?>img/slider/s05.jpg')"></div>
											</div>
										</div>
										<!-- Carousel nav -->
									</div>
									<a class="bigSlider-control left" href="#bigCarousel" data-slide="prev"></a>
									<a class="bigSlider-control right" href="#bigCarousel" data-slide="next"></a>
								</div>
							</section>
						</div>
					</div>
				</section>

				<section class="whiteContent ptx">
					<div class="container">
						<div class="row">
							<section class="span12">
								<div class="partTitle left smartPart" id="news">
									Мы принимаем к оплате
								</div>
							</section>
						</div>
						<div class="row">
							<section class="span12 text-center">
								<img src="/include/frontend/img/payments_line.jpg" alt="" />
							</section>

						</div>
						<div class="row">
							<section class="span12">
								<hr />
							</section>
						</div>
						<div class="row">
							<section class="span5">
								<div class="partTitle left smartPart" id="pay">
									Оплата и условия
								</div>
								<div class="partContent">
									<ul class="txtLarge">
										<li>
											<a href="/include/docs/rules.pdf" target="_blank" class="dotted">Условия предоставления услуг</a>
										</li>
										<li>
											<a href="/include/docs/oferta.pdf" target="_blank" class="dotted">Договор-оферта</a>
										</li>
										<li>
											<a href="/include/docs/politics.pdf" target="_blank" class="dotted">Политика конфиденциальности</a>
										</li>
									</ul>
								</div>
							</section>
							<section class="span7">
								<div class="partTitle left">
									Последние новости сервиса
								</div>
								<div class="partContent">
									<div class="newsSlider">
										<div id="newsCarousel" class="carousel slide">
											<!-- Carousel items -->
											<div class="carousel-inner">
												<div class="item active">
													<div class="newsItem">
														<div class="date">
															12 июля 2013
														</div>
														<div class="title">
															Бета-тестирование "Ракеты"!
														</div>
														<div class="description">
															Уважаемые пользователи! Наш сервис открыт в тестовом режиме и надеемся в ближайшее время значительно улучшить его с вашей помощью! Поэтому просим вас не только активно использовать сервис, но также оставлять свои отзывы о нем и предложения по модернизации. Заранее благодарны!
														</div>
													</div>
												</div>
											</div>
	
										</div>
										<!-- Carousel nav -->
										<!-- <a class="bigSlider-control left" href="#newsCarousel" data-slide="prev"></a>
										<a class="bigSlider-control right" href="#newsCarousel" data-slide="next"></a> -->
									</div>
								</div>
							</section>
						</div>
						<div class="row">
							<section class="span12">
								&nbsp;
							</section>
						</div>
					</div>
				</section>
			</div>
			<footer>
				<div class="footerWrapper">
					<div class="footerContent">
						<div class="container">
							<div class="row">
								<section class="span4">
									<div class="partTitle left smartPart">
										Связаться с нами
									</div>
									<div class="partContent">
										<div class="contactsBlock">
											<ul>
												<li>
													<span class="largeIcon">
														<img src="/include/frontend/img/contacts/contacts_icq.png" alt="" width="32" height="31" />
													</span>
													<span class="blue">ICQ:</span> 905 11 11
												</li>
												<li>
													<div class="largeIcon">
														<img src="/include/frontend/img/contacts/contacts_skype.png" alt="" width="32" height="34" />
													</div>
													<span class="blue">Skype:</span> top-raketa
												</li>
												<li><div class="largeIcon">
													<img src="/include/frontend/img/contacts/contacts_email.png" alt="" width="30" height="26" />
												</div>
													<span class="blue">E-mail:</span> <a href="mailto:support@top-raketa.ru">support@top-raketa.ru</a>
												</li>
												<li><div class="largeIcon">
													<img src="/include/frontend/img/contacts/contacts_phone.png" alt="" width="26" height="28" />
												</div>
													<span class="blue">Телефон:</span> 8 800-500-75-70
												</li>
											</ul>
										</div>
										<div class="callFromSiteWrapper">
											<a href="http://zingaya.com/widget/35192c472dd244bfaf5143ea23b8f450" onclick="window.open(this.href+'?referrer='+escape(window.location.href)+'', '_blank', 'width=236,height=220,resizable=no,toolbar=no,menubar=no,location=no,status=no'); return false" class="zingaya_button1374658362575"></a>
										</div>
									</div>
								</section>
								<section class="offset1 span7">
									<div class="partTitle left">
										Бесплатная помощь в запуске проекта:
									</div>
									<div class="partContent" id="callback">
										<?=$callback?>
									</div>
								</section>
							</div>
						</div>
					</div>
				</div>
				<div class="footerPlate">
					<div class="container">
						<div class="row">
							<section class="span12 posR">
								<p><a href="/include/docs/rules.pdf" target="_blank">Правовая информация</a><br />
									<span class="txtSmall">Copyright 2013 Top-Raketa.ru. Все права защищены</span>
								</p>
								<div class="rocket"></div>
							</section>
						</div>
					</div>
				</div>
			</footer>
		</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter21911587 = new Ya.Metrika({id:21911587,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21911587" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- RedHelper 
<script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" 
	src="https://web.redhelper.ru/service/main.js?c=redraketa">
</script> 
<!--/Redhelper -->

<!-- webim button generation date: 2013-08-29 version: 8.2.0 -->
<script type="text/javascript">
  document.write('<a class="webim_button" href="#" rel="webim"><img src="' + document.location.protocol + '//topraketaru.webim.ru/webim/button.php" border="0"/></a>');

  webim = {
    accountName: "topraketaru",
    domain: "topraketaru.webim.ru"
  };
  (function () {
      var s = document.createElement("script");
      s.type = "text/javascript";
      s.src = document.location.protocol + "//topraketaru.webim.ru/webim/js/button.js";
      document.getElementsByTagName("head")[0].appendChild(s);
  })();
</script>
<!-- /webim button -->

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>
			window.jQuery || document.write('<script src="<?=$tPath?>js/vendor/jquery-1.9.1.min.js"><\/script>')
		</script>

		<script src="<?=$tPath?>js/vendor/bootstrap.min.js"></script>
		<script src="<?=$tPath?>js/vendor/less.js"></script>
		<script src="<?=$tPath?>js/vendor/jquery-loadmask-0.4/jquery.loadmask.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$tPath?>js/vendor/jquery.transit.min.js" type="text/javascript" charset="utf-8"></script>
		<!-- <script src="<?=$tPath?>js/vendor/scrollspy.js" type="text/javascript" charset="utf-8"></script> -->
		<script src="<?=$tPath?>js/vendor/jq.smooth.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$tPath?>js/main.js"></script>
		<script src="<?=$tPath?>js/calc.js"></script>
	</body>
</html>
