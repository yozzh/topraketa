<div class="callbackMessage">
	<div class="alert alert-info text-center">
		Ваше сообщение успешно добавлено!<br />
		<h4>В ближайшее время с Вами свяжется наш менеджер.</h4>
		<div class="progress progress-striped progress-info active">
		  	<div class="bar" style="width: 100%;"></div>
		</div>
	</div>
</div>