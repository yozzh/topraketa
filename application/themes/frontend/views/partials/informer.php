<div class="registration">
	<a href="/main/logout">Выход</a>
</div>

<div class="moduleHeader">
	Личный кабинет
</div>

<div class="informerBlock">
	<div class="inputColumns">
		<div class="inputColumn w50">
			<div class="inputWrapper">
				<dl>
					<dt>Добро пожаловать,</dt>
					<dd><?=$user->first_name?> (<?=$user->email?>)</dd>
					<dt>Ваш баланс</dt>
					<dd>1 200 р. &nbsp;<a href="#" class="btn btn-mini btn-success"><i class="icon icon-plus icon-white"></i>Пополнить</a></dd>
				</dl>
			</div>
		</div>
		<div class="inputColumn w50">
			<div class="inputWrapper">
				<dl>
					<dt>Ежемесячный расход</dt>
					<dd>2 000 р.</dd>
					<dt>Средств хватит</dt>
					<dd>еще на 5 дней</dd>
				</dl>
			</div>
		</div>
	</div>
</div>
