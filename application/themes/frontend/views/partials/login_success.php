<div class="informerMessage">
	<div class="alert alert-info text-center">
		Авторизация пройдена успешно.<br />
		<h4>Добро пожаловать на борт!</h4>
		<div class="progress progress-striped progress-info active">
		  	<div class="bar" style="width: 100%;"></div>
		</div>
	</div>
</div>