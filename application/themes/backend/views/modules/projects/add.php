<?php
	$edit = isset($project); 
?>

<div class="grid">
	<div class="grid-title">
		<div class="pull-left">
			<div class="icon-title">
				<i class="icon-bookmark"></i>
			</div>
			
			<span><?=($edit)?'Редактирование проекта "'.$project->title.'"':'Новый проект'?></span>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="grid-content">
		<form action="/projects/<?=($edit)?'edit/'.$project->id:'add'?>" method="post" accept-charset="utf-8" class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="title">Название</label>
				<div class="controls">
					<input type="text" value="<?=($edit)?$project->title:''?>" name="title" id="title" placeholder="Название проекта">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="url">Адрес группы</label>
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on">http://vk.com/</span>
						<input type="text" value="<?=($edit)?$project->url:''?>" name="url" id="url" placeholder="Полный URL сообщества">
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="comment">Комментарий</label>
				<div class="controls">
					<textarea rows="5" name="comment" id="comment"><?=($edit)?$project->comment:''?></textarea>
				</div>
			</div>
			<hr />
			<input type="submit" value="Сохранить" class="btn btn-success"/>
		</form>
	</div>
</div>