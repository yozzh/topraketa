<div class="grid">
	<div class="grid-title">
		<div class="pull-left">
			<div class="icon-title"><i class="icon-eye-open"></i></div>
			<span>Список проектов</span>
		</div>
		<div class="pull-right">
			<div class="icon-title"><a href="/projects/add"><i class="icon-plus"></i></a></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="grid-content">
		<?php if (!count($projects)) { ?><div class="text-info text-center">Список проектов пуст</div><?php } else { ?>
		<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-mod-2 dataTable">
			<thead>
				<tr>
					<th width="70">HID</th>
					<th width="200">Проект</th>
					<th width="200">Бюджет</th>
					<th width="">Пользователь</th>
					<th width="65"></th>
				</tr>
			</thead>
			<tbody>
			<?php $currentStatus = $projects[0]->status; ?>
			<tr><td colspan="5"><h4><?=projectStatus($currentStatus)->title?></h4></td></tr>
			<?php foreach($projects as $project) : ?>
				<? if ($project->status != $currentStatus) {
					$currentStatus = $project->status;
					echo '<tr><td colspan="5"><h4>'.projectStatus($currentStatus)->title.'</h4></td></tr>';
				}?>
				<tr>
					<td class="text-center"><span class="label label-<?=($project->hm_project_id>0)?'success':'important'?>"><?=($project->hm_project_id>0)?$project->hm_project_id:'Не привязан';?></span></td>
					<td> <a href="http://<?=$project->url?>" target="_blank"><?=$project -> url ?></td>
					<td><?=$project->budget?> р.</td>
					<td><a href="/users/?email=<?=$project->email?>" target="_blank"><?=$project->email?></a></td>
					<td class="action-table tooltip-top">
						<a href="/admin/projects/delete/<?=$project -> id ?>" class="productDelete" data-t="tooltip" title="Удалить"><img src="<?=$tPath ?>images/icon/table_del.png" alt=""></a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<?php } ?>
	</div>
</div>