<div class="grid">
	<div class="grid-title">
		<div class="pull-left">
			<div class="icon-title"><i class="icon-eye-open"></i></div>
			<span>Список проектов</span>
		</div>
		<div class="pull-right">
			<div class="icon-title"><a href="/projects/add"><i class="icon-plus"></i></a></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="grid-content">
		<?php if (!count($items)) { ?><div class="text-info text-center">Список проектов пуст</div><?php } else { ?>
		<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-mod-2 dataTable">
			<thead>
				<tr>
					<th width="70">Статус</th>
					<th width="200">Проект</th>
					<th width="">Комментарий</th>
					<th width="65"></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($items as $item) : ?>
				<tr>
					<td><span class="label label-<?=($item->status==1)?'success':'important'?>"><?=($item->status==1)?'Одобрен':'Заблокирован'?></span></td>
					<td><?=$item -> title ?> <a href="http://vk.com/<?=$item->url?>" target="_blank"><img src="<?=$tPath ?>images/icon/table_vk.png" width="14" alt=""></a></td>
					<td><small><i><?=$item->comment?></i></small></td>
					<td class="action-table tooltip-top">
						<a href="/projects/edit/<?=$item -> id ?>" data-t="tooltip" title="Редактировать"><img src="<?=$tPath ?>images/icon/table_edit.png" alt=""></a>
						<a href="/projects/delete/<?=$item -> id ?>" class="productDelete" data-t="tooltip" title="Удалить"><img src="<?=$tPath ?>images/icon/table_del.png" alt=""></a>
						<a href="/orders/add/<?=$item -> id ?>" data-t="tooltip" title="Оформить заказ"><img src="<?=$tPath ?>images/icon/table_money.png" alt=""></a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<?php } ?>
	</div>
</div>