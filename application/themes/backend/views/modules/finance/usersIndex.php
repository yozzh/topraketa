		<form id="user-search-form" class="form-search form-horizontal pull-right" action="/admin/finance/users/">
		    <div class="input-append">
		        <input type="text" class="search-query" name="email" placeholder="Поиск по E-mail" value="<?=$this->input->get('email')?>">
		        <button type="submit" class="btn"><i class="icon-search"></i></button>
		    </div>
		</form>
		<div class="clearfix"></div>
		<?php if (!count($users)) {
				$message = ($this->input->get('email'))?'Ничего не найдено':'Список пользователей пуст';		
			 ?><div class="text-info text-center"><?=$message?></div><?php } else { ?>
			<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th width="20">id</th>
						<th class="text-left" width="250">Пользователь</th>
						<th>Поступления</th>
						<th>Расход</th>
						<th>Баланс</th>
						<th width="60"></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$i=1;
					//var_dump($items);
					foreach($users as $user) : ?>
					<?php
					$dateTime = new DateTime();
					$dateTime -> setTimestamp($user -> created_on);
					?>
					<tr data-key="<?=$user -> id ?>">
						<td><?=$user -> id ?></td>
						<td class="text-left">
							  <ul>
							  	<li>Имя: <strong><?=$user -> first_name ?></strong></li>
							  	<li>Email: <a href="mailto:<?=$user -> email?>"><?=$user -> email ?></a></li>
							  	<li>Телефон: <?=($user -> phone==0 || $user->phone == '')?'<span class="cGray">Не подключен</span>':'<strong>'.$user->phone.'</strong>'; ?></li>
							  </ul>
						</td>
						<td class="alert-success"><?=(int)$user->balance->plus?> р.</td>
						<td class="alert-danger"><?=(int)$user->balance->minus?> р.</td>
						<td class="alert-info"><?=(int)($user->balance->minus+$user->balance->plus)?> р.</td>
						<td class="action-table tooltip-top">
							<a href="#" data-t="tooltip" title="Подробности" class="btn btn-primary btn-mini metro openStat" >Статистика</a>
							
						</td>
					</tr>
				<?php $i++;
						endforeach;
	?>
				</tbody>
			</table>
		<div class="pagination pagination-centered">
			<?=$pagination?>
		</div>
		<?php } ?>
		
		<div id="user-modal" class="modal fade hide" style="display: none; " aria-hidden="true">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">×</button>
                This is a modal header
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec massa tellus, vestibulum consectetur vulputate eget, posuere adipiscing odio. Aliquam vestibulum sodales lectus, eu blandit lorem.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-primary metro" data-dismiss="modal">Close</a>
            </div>
        </div>
