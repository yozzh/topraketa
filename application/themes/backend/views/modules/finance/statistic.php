<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Месяц</th>
			<th>Поступления</th>
			<th>Расход</th>
			<th class="text-center">Баланс<br />на конец месяца</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($statistic as $row) : ?>
		<tr>
			<td><?php echo rusDate("F", strtotime($row->date));?></td>
			<td class="alert-success"><?=(int)$row->plus?> р.</td>
			<td class="alert-danger"><?=(int)$row->minus?> р.</td>
			<td class="alert-info"><?=(int)($row->balance)?> р.</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>