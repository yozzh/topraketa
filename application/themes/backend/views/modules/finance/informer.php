<h3>Статистика за текущий месяц</h3>
<div class="information-box-3">
    <div class="item alert-success">
      <div class="box-info"> 
        <div class="box-figures">Поступления</div>
        <div class="box-title"><?=$informer->plus?> Р.</div>
      </div>
    </div>
    <div class="item alert-danger">
        <div class="box-info"> 
        <div class="box-figures">Расходы</div>
        <div class="box-title"><?=$informer->minus?> Р.</div>
      </div>
    </div>
    <div class="item alert-info">
    	<div class="box-info"> 
        <div class="box-figures">Робокасса</div>
        <div class="box-title"><?=$informer->online?> Р.</div>
      </div>
    </div>
  </div>