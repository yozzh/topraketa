<div id="login" class="animate form position">
    <form action="/main/login" class="form-login" method="post"> 
        <div class="content-login">
        <div class="header">Вход в личный кабинет</div>
        
        <div class="inputs">
            <input name="email" type="email" placeholder="Электронная почта" />
            <input name="password" type="password"  placeholder="Пароль" />
        </div>
        <input type="submit" class="btn btn-danger pull-right" value="Войти" style="margin-right: 20px; margin-bottom:20px; margin-top:10px">
        <div class="link-1"><a href="/main/registration">Регистрация</a></div>
        <div class="clear"></div>
        </div>
        
        <!-- <div class="footer-login">
         <div class="pull-left">Войти через ВКонтакте</div>
         <div class="pull-right">
           <ul class="social-links">
              <li class="facebook"><a href="#"><span>facebook</span></a></li>
           </ul>
         </div>
         
        </div> -->
        
    </form>
</div>