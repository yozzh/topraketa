<div id="login" class="animate form position">
    <form action="/main/registration" class="form-login" method="post"> 
        <div class="content-login">
        <div class="header">Регистрация</div>
        <div class="alert alert-danger"><?php echo validation_errors(); ?></div>
        <div class="inputs">
        	<input type="text" name="name" value="<?php echo(isset($form))?$form['name']:'';?>" id="name" placeholder="Ваше имя" />
            <input name="email" type="email" value="<?php echo(isset($form))?$form['email']:'';?>" placeholder="Электронная почта" />
            <input name="password" type="password"  placeholder="Пароль" />
            <input name="passconf" type="password"  placeholder="Подтверждение пароля" />
        </div>
        <div class="clear"></div>
        <div class="button-login"><input type="submit" class="" value="Войти"></div>
        </div>
        
        <div class="footer-login">
         
         <div class="pull-left">Регистрация через ВКонтакте</div>
         <div class="pull-right">
           <ul class="social-links">
              <li class="facebook"><a href="#"><span>facebook</span></a></li>
           </ul>
         </div>
         <div class="clear"></div>
        </div>
    </form>
</div>