
<div class="row-fluid">
	<div class="grid span6">
		<div class="grid-title">
			<div class="pull-left">
				<div class="icon-title"><i class="icon-eye-open"></i></div>
				<span>Ваши проекты</span>
			</div>
			<div class="pull-right">
				<div class="icon-title"><a href="/projects/add"><i class="icon-plus"></i></a></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="grid-content">
			<?php if (!count($projects)) { ?><div class="text-info text-center">Список проектов пуст</div><?php } else { ?>
			<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-mod-2 dataTable">

				<tbody>
				<?php foreach($projects as $item) : ?>
					<tr>
						<td>
							<span class="label label-<?=($item -> status == 1) ? 'success' : 'important' ?>"><?=($item -> status == 1) ? 'Одобрен' : 'Заблокирован' ?></span>
							<?=$item -> title ?> <a href="http://vk.com/<?=$item -> url ?>" target="_blank"><img src="<?=$tPath ?>images/icon/table_vk.png" width="14" alt=""></a>
							
						</td>
						<td class="action-table tooltip-top">
							<a href="/projects/edit/<?=$item -> id ?>" data-t="tooltip" title="Редактировать"><img src="<?=$tPath ?>images/icon/table_edit.png" alt=""></a>
							<a href="/projects/delete/<?=$item -> id ?>" class="productDelete" data-t="tooltip" title="Удалить"><img src="<?=$tPath ?>images/icon/table_del.png" alt=""></a>
							<a href="/orders/add/<?=$item -> id ?>" data-t="tooltip" title="Оформить заказ"><img src="<?=$tPath ?>images/icon/table_money.png" alt=""></a>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<p class="text-center">
				<a href="/projects" class="btn btn-info">Все проекты</a>
			</p>
			<?php } ?>
		</div>
	</div>
	<div class="grid span6">
		<div class="grid-title">
			<div class="pull-left">
				<div class="icon-title"><i class="icon-eye-open"></i></div>
				<span>Последние 5 заказов</span>
			</div>
			<div class="pull-right">
				<div class="icon-title"><a href="/orders/add"><i class="icon-plus"></i></a></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="grid-content">
			<?php if (!count($orders)) { ?><div class="text-info text-center">Список заказов пуст</div><?php } else { ?>
			<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-mod-2 dataTable">
				<tbody>
				<?php foreach($orders as $item) : ?>
					<?php
					$orderStatus = orderStatus($item -> status);
					$datetime = date_create($item -> cdate);
					?>
					
					<tr>
						<td width="90" class="">
							<span class="label label-<?=$orderStatus['class'] ?>" title="<?=$orderStatus['title'] ?>">
								<?=date_format($datetime, 'Y-m-d')?>
							</span>
						</td>
						<td>
								Проект: <strong><?=$item -> projectTitle ?></strong> <a href="http://vk.com/<?=$item -> projectUrl ?>" target="_blank"><img src="<?=$tPath ?>images/icon/table_vk.png" width="14" alt=""></a><br />
								Заказ: <?=$item -> count ?> за <strong><?=$item -> sum ?> руб.</strong>
							</dl>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<p class="text-center">
				<a href="/orders" class="btn btn-info">Все заказы</a>
			</p>
			<?php } ?>
		</div>
	</div>
	
</div>