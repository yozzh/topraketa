<div class="grid">
	<div class="grid-title">
		<div class="pull-left">
			<div class="icon-title"><i class="icon-eye-open"></i></div>
			<span>Список продуктов</span>
		</div>
		<div class="pull-right">
			<div class="icon-title"><a href="/admin/products/add"><i class="icon-plus"></i></a></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="grid-content">
		<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-mod-2 dataTable">
			<thead>
				<tr>
					<th>id</th>
					<th>Наименование</th>
					<th>Стоимость за вступившего</th>
					<th>Статус</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($items as $item) : ?>
				<tr>
					<td><?=$item -> pid ?></td>
					<td><?=$item -> title ?></td>
					<td>
						<strong>Начальная стоимость: <?=$item -> base_cost ?> р.</strong><br />
						<?php if (count($item->cases)) : ?>
							<ul>
							<?php foreach ($item->cases as $case) : ?>
								<li>от <?=$case['count']?> шт. по <strong><?=$case['cost']?> р.</strong></li>							
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>
						
					</td>
					<td><?=$item -> status ?></td>
					<td class="action-table ">
						<a href="/admin/products/edit/<?=$item->pid?>"><img src="<?=$tPath?>images/icon/table_edit.png" alt=""></a>
						<a href="/admin/products/delete/<?=$item->pid?>" class="productDelete"><img src="<?=$tPath?>images/icon/table_del.png" alt=""></a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>