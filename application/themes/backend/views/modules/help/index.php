
<div class="row-fluid">
	<div class="grid span12">
		<div class="grid-title">
			<div class="pull-left">
				<div class="icon-title"><i class="icon-eye-open"></i></div>
				<span>Страницы помощи</span>
			</div>
			<div class="pull-right">
				<div class="icon-title"><a href="/projects/add"><i class="icon-plus"></i></a></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="grid-content">
			<?php if (!count($pages)) { ?><div class="text-info text-center">Список страниц пуст</div><?php } else { ?>
			<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-mod-2 dataTable">

				<tbody>
				<?php foreach($pages as $item) : ?>
					<tr>
					  <td width="10">
					    <?=$item->order?>
					  </td>
					  <td>
					    <strong><?=$item->title?></strong> (<?=$item->url?>)
					  </td>
						<td>
							<span class="label label-<?=($item -> status == 1) ? 'success' : 'important' ?>"><?=($item -> status == 1) ? 'Опубликована' : 'Заблокирован' ?></span>
						</td>
						<td class="action-table tooltip-top">
							<a href="/admin/help/edit/<?=$item -> id ?>" data-t="tooltip" title="Редактировать"><img src="<?=$tPath ?>images/icon/table_edit.png" alt=""></a>
							<a href="/admin/help/delete/<?=$item -> id ?>" class="productDelete" data-t="tooltip" title="Удалить"><img src="<?=$tPath ?>images/icon/table_del.png" alt=""></a>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<?php } ?>
		</div>
	</div>	
</div>