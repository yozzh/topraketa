<?php
  if (isset($page->order)) {
    $order = $page->order;
  } elseif (isset($newOrder)) {
    $order = $newOrder;
  } else {
    $order = 0;
  }
  
?>
<div class="grid">
  <div class="grid-title">
    <div class="pull-left">
      <div class="icon-title">
        <i class="icon-bookmark"></i>
      </div>
      <span>Добавить новую страницу в раздел Помощь</span>
      <div class="clearfix"></div>
    </div>
    <div class="pull-right">
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="grid-content">
    <form action="/admin/help/<?=(isset($page))?'edit':'add'?>" method="post">
    	<div class="formRow">
    	  <label>Заголовок: </label>
    	  <div class="formRight">
    	    <input type="text" id="title" name="title" class="span input" value="<?=(isset($page->title))?$page->title:''?>">
    	  </div>
    	</div>
    	<div class="formRow">
    	  <label>Алиас: </label>
    	  <div class="formRight">
    	    <input type="text" id="url" name="url" class="span input" value="<?=(isset($page->url))?$page->url:''?>">
    	  </div>
    	</div>
    	<div class="formRow">
        <label>Порядок: </label>
        <div class="formRight">
          <input type="text" id="title" name="order" class="span input" value="<?=$order?>">
        </div>
      </div>
    	<div class="formRow">
    	  <label>Текст страницы:</label>
    	  <div class="formRight">
    	    <textarea class="span input same-height-1" name="body" rows="3" id="content"><?=(isset($page->body))?$page->body:''?></textarea>
    	  </div>
    	</div>
    	<div class="clearfix"></div>
    	  <hr />
    	  <?php if (isset($page)) :?>
    	    <input type="hidden" name="id" value="<?=$page->id?>"/>
  	    <?php endif; ?>
    	  <input type="submit" value="Сохранить" class="btn btn-primary metro pull-right" />
    </form>
  <div class="clearfix"></div>
  </div>
</div>