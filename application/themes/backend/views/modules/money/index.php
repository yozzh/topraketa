<div class="grid">
	<div class="grid-title">
		<div class="pull-left">
			<div class="icon-title"><i class="icon-eye-open"></i></div>
			<span>Список платежей</span>
		</div>
		<div class="pull-right">
			<div class="icon-title"><a href="/money/add"><i class="icon-plus"></i></a></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="grid-content">
		<?php if (!count($items)) { ?><div class="text-info text-center">Список платежей пуст</div><?php } else { ?>
		<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-mod-2 dataTable">
			<thead>
				<tr>
					<th width="70">Статус</th>
					<th width="100">Сумма</th>
					<th width="">Платежная система</th>
				</tr>
			</thead>
			<tbody>
			<?php $total = 0; ?>
			<?php foreach($items as $item) : ?>
			<?php
				$itemStatus = moneyStatus($item->status);
				$total += $item->value;
			?>
				<tr>
					<td><span class="label label-<?=$itemStatus['class']?>"><?=$itemStatus['title']?></span></td>
					<td class="text-center"><?=moneySum($item->value)?></td>
					<td><?=paymentType($item->type)?></td>
				</tr>
			<?php endforeach; ?>
			<tfoot>
				<tr>
					<td class="text-right">Итого:</td>
					<td class="text-center"><strong><?=moneySum($total) ?></strong></td>
					<td></td>
				</tr>
			</tfoot>
			</tbody>
		</table>
		<?php } ?>
	</div>
</div>