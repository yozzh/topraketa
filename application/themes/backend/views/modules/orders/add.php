<?php
	$edit = isset($order); 
?>

<div class="grid">
	<div class="grid-title">
		<div class="pull-left">
			<div class="icon-title">
				<i class="icon-bookmark"></i>
			</div>
			
			<span><?=($edit)?'Редактирование заказа "'.$order->title.'"':'Новый заказ'?></span>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="grid-content">
		<form action="/orders/<?=($edit)?'edit/'.$order->id:'add'?>" method="post" accept-charset="utf-8" class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="title">Проект</label>
				<div class="controls">
					<select class="chzn-select chosen_select span4" name="project">
						<?php foreach ($projects as $project) : ?> 
							<?php $selected = ($project->id == $project_id)?'selected':''; ?>
                    		<option value="<?=$project->id?>" <?=$selected?>><?=$project->title?> (/<?=$project->url?>)</option> 
                    	<?php endforeach; ?>
              		</select> 
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="title">Продукт</label>
				<div class="controls">
					<select class="chzn-select chosen_select span4" name="product">
						<?php foreach ($products as $product) : ?> 
                    		<option value="<?=$product->pid?>"><?=$product->title?></option> 
                    	<?php endforeach; ?>
              		</select> 
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="count">Количество</label>
				<div class="controls">
					<input type="number" value="<?=($edit)?$project->title:''?>" class="span4 orderCount" name="count" id="count" placeholder="Точное количество вступивших">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="title">Сумма заказа</label>
				<div class="controls">
					<div class="well text-center orderSum">
						<p class="cost">0 руб.</p>
						<p class="cost_comment"></p>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="comment">Комментарий к заказу</label>
				<div class="controls">
					<textarea rows="5" class="span4" name="comment" id="comment"><?=($edit)?$order->comment:''?></textarea>
				</div>
			</div>
			
			
			<hr />
			<input type="submit" value="Сохранить" class="btn btn-success"/>
		</form>
		<hr />
		

	</div>
</div>