<div class="grid">
	<div class="grid-title">
		<div class="pull-left">
			<div class="icon-title"><i class="icon-eye-open"></i></div>
			<span>Список заказов</span>
		</div>
		<div class="pull-right">
			<div class="icon-title"><a href="/orders/add"><i class="icon-plus"></i></a></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="grid-content">
		<?php if (!count($items)) { ?><div class="text-info text-center">Список заказов пуст</div><?php } else { ?>
		<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-mod-2 dataTable">
			<thead>
				<tr>
					<th width="300">Проект</th>
					<th width="300">Заказ</th>
					<th width="">Комментарий</th>
					<th width="120"></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($items as $item) : ?>
				<?php
					$orderStatus = orderStatus($item->status);
					$datetime = date_create($item->cdate);
				?>
				
				<tr>
					<td>
						<dl class="dl-horizontal project tooltip-top">
							<dt>Проект:</dt>
							<dd><?=$item -> projectTitle ?> <a href="http://vk.com/<?=$item->projectUrl?>" target="_blank"><img src="<?=$tPath ?>images/icon/table_vk.png" width="14" alt=""></a></dd>
							<dt>Дата:</dt>
							<dd><?=date_format($datetime, 'Y-m-d').' <span class="cGray">('.date_format($datetime,'H:i:s').')</span>'?></dd>
							<dt>Пользователь:</dt>
							<dd><a data-t="tooltip" title="<?=$item->email?>"><?=$item->first_name?></a></dd>
						</dl>
					</td>
					<td>
						<dl class="dl-horizontal">
							<dt>Продукт:</dt>
							<dd><?=$item -> productTitle?></dd>
							<dt>Количество:</dt>
							<dd><?=$item -> count?></dd>
							<dt>Сумма:</dt>
							<dd><?=$item -> sum?> руб.</dd>
						</dl>
					</td>
					<td><small><i><?=$item->comment?></i></small></td>
					<td class="action-table tooltip-top text-center"><span class="label label-<?=$orderStatus['class']?>"><?=$orderStatus['title']?></span><br /><br />
						<?php if ($item->status==0) : ?>
							<a href="/orders/cancel/<?=$item -> id ?>" class="orderCancel" data-t="tooltip" title="Отменить заказ"><img src="<?=$tPath ?>images/icon/table_del.png" alt=""></a>
						<? endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<?php } ?>
		<hr />
		<div class="well"><strong>Примечание:</strong> Заказ может быть отменен только до начала его обработки!</div>
		
	</div>
</div>