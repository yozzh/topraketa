<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title><?php echo $template['title']; ?> | Личный кабинет</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?=$tPath?>css/style.css" rel="stylesheet">
    <link href="<?=$tPath?>css/bootstrap.css" rel="stylesheet">

    <link rel="stylesheet" href="<?=$tPath?>css/jquery-ui-1.8.16.custom.css" media="screen"  />
    <link rel="stylesheet" href="<?=$tPath?>css/fullcalendar.css" media="screen"  />
    <link rel="stylesheet" href="<?=$tPath?>css/chosen.css" media="screen"  />
    <link rel="stylesheet" href="<?=$tPath?>css/glisse.css?1.css">
    <link rel="stylesheet" href="<?=$tPath?>css/jquery.jgrowl.css">
    <link rel="stylesheet" href="<?=$tPath?>css/demo_table.css" >
    <link rel="stylesheet" href="<?=$tPath?>css/jquery.fancybox.css?v=2.1.4" media="screen" />
    
  	<link rel="stylesheet" href="<?=$tPath?>css/icon/font-awesome.css">    
    <link rel="stylesheet" href="<?=$tPath?>css/bootstrap-responsive.css">
    <link rel="stylesheet" href="<?=$tPath?>css/main.css" >
    
    <link rel="stylesheet" href="<?=$tPath?>js/aloha/css/aloha.css" type="text/css">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script language="JavaScript">
	Firefox = navigator.userAgent.indexOf("Firefox") >= 0;
	if(Firefox) document.write("<link rel='stylesheet' href='<?=$tPath?>css/moz.css' type='text/css'>"); 
	</script>
    
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="favicon.ico">
    
  </head>

  <body>
    <!--BEGIN HEADER-->
    <div id="header" role="banner">
       <a id="menu-link" class="head-button-link menu-hide" href="#menu"><span>Menu</span></a>
       <!--Logo--><a href="dashboard.html" class="logo"><h1>VK-GROUP</h1></a><!--Logo END-->
       
       <div class="right">
       
       <!--notification box-->
         <div class="dropdown left">
          <a class="dropdown-toggle head-button-link notification" data-toggle="dropdown" href="#"><!-- <span class="notice-new">2</span> --></a>
          
          
        </div>
       <!--notification box end-->
       
       <!--profile box-->
         <div class="dropdown left profile">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="double-spacer"></span>
            <div class="profile-avatar"><img src="<?=$tPath?>images/avatar.png" alt=""></div>
            <div class="profile-username"><span>Добро пожаловать,</span> <?=$user->first_name?></div>
            <div class="profile-caret"> <span class="caret"></span></div>
            <span class="double-spacer"></span>
          </a>
          <div class="dropdown-menu pull-right profile-box">
          <div class="triangle-3"></div>
          
            <ul class="profile-navigation">
              <li><a href="#"><i class="icon-cog"></i> Настройки</a></li>
              <li><a href="/main/logout"><i class="icon-off"></i> Выход</a></li>
            </ul>
          </div>
        </div>
        <div class="clearfix"></div>
       <!--profile box end-->
       
       </div>
       
      
    </div>
    <!--END HEADER-->
    
    <div id="wrap">
    
    
    	<!--BEGIN SIDEBAR-->
        <div id="menu" role="navigation">
          <?php 
          	if (isset($subMenu)) {
          		echo $subMenu;
          	}
          ?>
          <?php echo $mainMenu;?>
          
          
          
          <div class="clearfix"></div>
        </div>
        <!--SIDEBAR END-->
    
    	
        <!--BEGIN MAIN CONTENT-->
        <div id="main" role="main">
          <div class="block">
   		  <div class="clearfix"></div>
            <div class="pagetitle">
			    <h1><?php echo $template['title']; ?></h1>
			    <div class="clearfix"></div>
			 </div>
			 <div class="clearfix"></div>
			 <div class="preContent">
			 	<?php if (isset($subMenu) && isset($preContent)) { echo $preContent;} ?>
			 </div>
			 <?php if ($this->session->flashdata('success')!="") { ?>
				<div class="alert alert-success">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <?=$this->session->flashdata('success')?>
				</div>
			<?php } ?>
             
             
             