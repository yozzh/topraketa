<div class="money-informer media-files text-center">
  <div class="title">Состояние счета</div>
  	<p>
  		<strong>На счету</strong><br />
  		<span class="label label-info"><?=$currentSum?> руб.</span>
  	</p>
  	<p>
  		<strong>В работе</strong><br />
  		<span class="label label-inverse"><?=$currentOrders?> <?=proceedTextual($currentOrders, "заказов", "заказ", "заказа");?></span>
  	</p>
  <div class="text-center buttons">
  	<a href="/money/add" class="btn btn-success"><i class="icon-shopping-cart icon-white"></i> Пополнить</a>
  </div>
</div>