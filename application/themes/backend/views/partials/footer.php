	</div>
	<div class="clearfix"></div>
</div>
        <!--MAIN CONTENT END-->
    
    </div>
    <!--/#wrapper-->


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?=$tPath?>js/jquery.min.js"></script>
    <script src="<?=$tPath?>js/jquery-ui.min.js"></script>
   
    <script src="<?=$tPath?>js/bootstrap.min.js"></script>
    <script src="<?=$tPath?>js/google-code-prettify/prettify.js"></script>
   
    <script src="<?=$tPath?>js/graphtable.js"></script>
    <script src="<?=$tPath?>js/fullcalendar.min.js"></script>
    <script src="<?=$tPath?>js/chosen.jquery.min.js"></script>
    <script src="<?=$tPath?>js/autoresize.jquery.min.js"></script>
    <script src="<?=$tPath?>js/jquery.autotab.js"></script>
    <script src="<?=$tPath?>js/jquery.jgrowl_minimized.js"></script>
    <script src="<?=$tPath?>js/jquery.dataTables.min.js"></script>
    <script src="<?=$tPath?>js/jquery.stepy.min.js"></script>
    <script src="<?=$tPath?>js/jquery.validate.min.js"></script>
    <script src="<?=$tPath?>js/raphael.2.1.0.min.js"></script>
    <script src="<?=$tPath?>js/justgage.1.0.1.min.js"></script>
  	<script src="<?=$tPath?>js/glisse.js"></script>
  	<script src="<?=$tPath?>js/styleswitcher.js"></script>
  	<script src="<?=$tPath?>js/moderniz.js"></script>
    <script src="<?=$tPath?>js/jquery.sparkline.min.js"></script>
    <script src="<?=$tPath?>js/slidernav-min.js"></script>
    <script type="text/javascript" src="<?=$tPath?>js/tinymce/tinymce.min.js"></script>
    <?php
		if (isset($scripts)) {
			foreach ($scripts as $script) {
				echo '<script src="'.$tPath.'js/'.$script.'"></script>';
			}
		}
	?>
    <script type="text/javascript" src="<?=$tPath?>js/jquery.fancybox.js?v=2.1.4"></script>



    


  </body>
</html>

