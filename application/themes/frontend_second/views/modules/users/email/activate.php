<p>Поздравляем, Вы успешно зарегистрированы в системе автоматического продвижения Top-Raketa.ru!</p>
<p>Ваши регистрационные данные:</p>
<p>
	Логин: <strong><?=$email?></strong><br />
	Пароль: <strong><?=$password?></strong>
</p>
<?php if ($bonus > 0) : ?>
	<p>Чтобы испытать эффективность нашей системы в действии вам предоставлен бонусный баланс в <strong><?=$bonus?> р.</strong></p>
<?php endif; ?>
<p>Баланс на текущий момент: <strong><?=round($sum+$bonus,2)?> руб.</strong></p>

<p>Перейти на сайт: <a href="http://top-raketa.ru/">top-raketa.ru</a></p>