<?php
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
?>
<div class="row">
	<div class="span5">
		<? showErrors($errors)?>
	</div>
</div>
<div class="row">
	<form action="/users/addMoney" method="post" data-callback="usersSave">
	<div class="span5">
			<input type="hidden" name="id" value="<?=$user->id?>" id="id"/>
			<input type="hidden" name="status" value="save" id="save"/>
			<div class="control-group  <?=(form_error('cost')!='')?'error':'';?>">
				<label for="cost">Сумма пополнения*</label>
				<input type="text" name="cost" value="<?=set_value('cost');?>" id="cost" placeholder="Сумма" class="input-block-level" />
			</div>
			<p class="text-right">
				<input type="submit" class="ajaxSubmit button blue" value="Пополнить баланс"/>
			</p>
	</div>
	</form>
</div>