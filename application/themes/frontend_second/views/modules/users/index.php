

		<form id="user-search-form" class="form-search form-horizontal pull-right" action="/users/">
		    <div class="input-append">
		        <input type="text" class="search-query" name="email" placeholder="Поиск по E-mail" value="<?=$this->input->get('email')?>">
		        <button type="submit" class="btn"><i class="icon-search"></i></button>
		    </div>
		</form>
		<div class="clearfix"></div>
		<?php if (!count($users)) {
				$message = ($this->input->get('email'))?'Ничего не найдено':'Список пользователей пуст';		
			 ?><div class="text-info text-center"><?=$message?></div><?php } else { ?>
		<div class="datagrid" id="projectsGrid">
			<table border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th width="20">id</th>
						<th class="text-left" width="200">Пользователь</th>
						<th width="65">Группы</th>
						<th>Проекты (В работе/Заморожены)</th>
						<th>Баланс</th>
						<th width="60"></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$i=1;
					//var_dump($items);
					foreach($users as $user) : ?>
					<?php
					$dateTime = new DateTime();
					$dateTime -> setTimestamp($user -> created_on);
					?>
					<tr data-key="<?=$user -> id ?>">
						<td><?=$user -> id ?></td>
						<td class="text-left">
							  <ul>
							  	<li>Имя: <strong><?=$user -> first_name ?></strong></li>
							  	<li>Email: <a href="mailto:<?=$user -> email?>"><?=$user -> email ?></a></li>
							  	<li>Телефон: <?=($user -> phone==0 || $user->phone == '')?'<span class="cGray">Не подключен</span>':'<strong>'.$user->phone.'</strong>'; ?></li>
							  </ul>
						</td>
						<td class="text-left">
							<ul>
								<?php foreach ($user->groups as $group) :?>
									<li>
										<span class="label <?=$group->class?>"><?=$group->description ?></span>
									</li>
								<?php endforeach; ?>
							</ul>
						</td>
						<td><?=renderStatusArray($user->projectsCount)?></td>
						<td><?=round($user->balance,2)?> р.</td>
						<td class="action-table tooltip-top">
							<a href="#" data-t="tooltip" title="Редактировать" class="userEdit btn btn-mini btn-info"><i class="icon icon-edit icon-white"></i></a>
							<?php if(isset($locked)) { ?>
								<a href="#" class="userUnlock btn btn-mini btn-danger" data-t="tooltip" title="Разблокировать"><i class="icon icon-ok icon-white"></i></a>
							<?php } else { ?>
								<a href="#" class="userLock btn btn-mini btn-danger" data-t="tooltip" title="Заблокировать"><i class="icon icon-lock icon-white"></i></a>
							<?php } ?>
							<?php if ($this->ion_auth->is_admin()) : ?>
								<a href="#" data-t="tooltip" title="Пополнить баланс" class="addMoney btn btn-mini btn-success"><i class="icon icon-plus icon-white"></i></a>
								<a href="/users/magicEye/<?=$user->id?>" data-t="tooltip" title="Magic Eye" class="btn btn-mini btn-inverse"><i class="icon icon-eye-open icon-white"></i></a>
							<?php endif; ?>
						</td>
					</tr>
				<?php $i++;
						endforeach;
	?>
				</tbody>
			</table>
		</div>
		<div class="pagination pagination-centered">
			<?=$pagination?>
		</div>
		<?php } ?>
		
