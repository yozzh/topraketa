<?php
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
//var_dump($user);
?>
<div class="row">
	<div class="span12">
		<? showErrors($errors)?>
	</div>
</div>
<div class="row">
	<form action="/users/edit" method="post" data-callback="usersSave">
	<div class="span6">
			<input type="hidden" name="id" value="<?=$user->id?>" id="id"/>
			<input type="hidden" name="status" value="save" id="save"/>
			<div class="control-group  <?=(form_error('first_name')!='')?'error':'';?>">
				<label for="first_name">Ваше имя*</label>
				<input type="text" name="first_name" value="<?=(set_value('first_name')!='')?set_value('first_name'):$user->first_name; ?>" id="first_name" placeholder="Ваше имя" class="input-block-level" />
			</div>
			<div class="control-group  <?=(form_error('email')!='')?'error':'';?>">
				<label for="email">Ваш E-mail*</label>
				<input name="email" id="email" type="email" value="<?=(set_value('email')!='')?set_value('email'):$user->email; ?>" placeholder="Электронная почта" class="input-block-level" />
			</div>
			<div class="control-group  <?=(form_error('phone')!='')?'error':'';?>">
				<label for="phone">Контактный телефон</label>
				<input name="phone" id="phone" type="text" value="<?=(set_value('phone')!='')?set_value('phone'):$user->phone; ?>" placeholder="+7 *** *** ** **" class="input-block-level" />
			</div>
			<div class="control-group  <?=(form_error('password')!='')?'error':'';?>">
				<label for="password">Пароль и его подтверждение*</label>
				<div class="row">
					<div class="span3"><input name="password" id="password" type="password"  placeholder="Пароль" class="input-block-level" /></div>
					<div class="span3"><input name="passconf" id="passconf" type="password"  placeholder="Подтверждение пароля"  class="input-block-level"/></div>
				</div>
			</div>
			<div class="clear"></div>
			<p class="text-right">
				<input type="submit" class="ajaxSubmit button blue" value="Изменить профиль"/>
			</p>
		
		
	</div>
	<div class="span6">
		<?php if ($this->ion_auth->is_admin()) : ?>
			<div class="control-group  <?=(form_error('phone')!='')?'error':'';?>">
				<label for="phone">Группы пользователей</label>
				<?php foreach ($groups as $group) : ?>
					<ul>
						<li>
							<label for="group_<?=$group->id?>" class="checkbox">
								<input type="checkbox" name="group[]" id="group_<?=$group->id?>" value="<?=$group->id?>" <?=(isset($user->groups[$group->id]))?'checked="checked"':''?>/>
								<?=$group->description?>
							</label>
						</li>
					</ul>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>
	</div>
	</form>
</div>