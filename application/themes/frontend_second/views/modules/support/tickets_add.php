<?php
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
?>
<div class="row">
	<div class="span6">
		<? showErrors($errors)?>
	</div>
</div>
<div class="row">
	<form action="/support/tickets/add" method="post" data-callback="ticketAdd">
	<div class="span6">
			<?php if(isset($group)): ?>
				<input type="hidden" name="id" value="<?=$group->id?>" id="id"/>
			<?php endif; ?>
			<input type="hidden" name="status" value="save" id="save"/>
			<div class="control-group  <?=(form_error('title')!='')?'error':'';?>">
				<label for="title">Тема тикета*</label>
				<input type="text" name="title" value="<?=(set_value('title')!='')?set_value('title'):(isset($group->title))?$group->title:''; ?>" id="title" placeholder="Тема вашего вопроса" class="input-block-level" />
			</div>
			<div class="control-group  <?=(form_error('alias')!='')?'error':'';?>">
				<label for="group">Группа вопросов*</label>
				<select name="group" id="group">
					<?php foreach ($groups as $group) : ?>
							<option value="<?=$group->id?>"><?=$group->title?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="control-group  <?=(form_error('alias')!='')?'error':'';?>">
				<label for="importance">Важность*</label>
				<select name="importance" id="importance">
					<option value="-10">Не срочно</option>
					<Option Value="0" selected="true">Обычная</Option>
					<option value="10">Срочно</option>
				</select>
			</div>
			<div class="control-group  <?=(form_error('description')!='')?'error':'';?>">
				<label for="description">Ваш вопрос</label>
				<textarea name="description" id="description" class="input-block-level" style="height:120px;"><?=(set_value('description')!='')?set_value('description'):(isset($group->description))?$group->description:''; ?></textarea>
			</div>
			<div class="clear"></div>
			<div class="text-right">
				<input type="submit" class="ajaxSubmit button blue" value="Создать тикет"/>
			</div>
	</div>
	</form>
</div>