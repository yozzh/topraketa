<p>В Службу Поддержки был добавлен новый тикет!</p>
<dl>
	<dt><strong>Автор сообщения:</strong></dt>
	<dd><?=$user->first_name?></dd>
	<dt><strong>Тема:</strong></dt>
	<dd><?=$title?></dd>
	<dt><strong>Текст сообщения:</strong></dt>
	<dd><?=$description?></dd>
</dl>
<p>
	Быстрый переход к тикету: <a href="http://top-raketa.ru/support/ticket/<?=$ticketId?>">Перейти на сайт</a>
</p>