<?php
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
if (isset($group)) {
	$action = 'edit';
} else {
	$action = 'add';
}

?>
<div class="row">
	<div class="span6">
		<? showErrors($errors)?>
	</div>
</div>
<div class="row">
	<form action="/support/groups/<?=$action?>" method="post" data-callback="groupsSave">
	<div class="span6">
			<?php if(isset($group)): ?>
				<input type="hidden" name="id" value="<?=$group->id?>" id="id"/>
			<?php endif; ?>
			<input type="hidden" name="status" value="save" id="save"/>
			<div class="control-group  <?=(form_error('title')!='')?'error':'';?>">
				<label for="title">Название группы*</label>
				<input type="text" name="title" value="<?=(set_value('title')!='')?set_value('title'):(isset($group->title))?$group->title:''; ?>" id="title" placeholder="Название группы" class="input-block-level" />
			</div>
			<div class="control-group  <?=(form_error('alias')!='')?'error':'';?>">
				<label for="alias">Alias*</label>
				<input name="alias" id="alias" type="text" value="<?=(set_value('alias')!='')?set_value('alias'):(isset($group->alias))?$group->alias:''; ?>" placeholder="Alias" class="input-block-level" />
			</div>
			<div class="control-group  <?=(form_error('description')!='')?'error':'';?>">
				<label for="description">Краткое описание</label>
				<textarea name="description" id="description" class="input-block-level"><?=(set_value('description')!='')?set_value('description'):(isset($group->description))?$group->description:''; ?></textarea>
			</div>
			<div class="clear"></div>
			<p class="text-right">
				<input type="submit" class="ajaxSubmit button blue" value="Сохранить группу"/>
			</p>
	</div>
	</form>
</div>