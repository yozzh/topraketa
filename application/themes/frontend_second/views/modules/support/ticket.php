		<div class="row">
			<section class="offset2 span8">
				<div class="btn-toolbar pull-right">
				  <div class="btn-group">
				    <a class="btn btn-success modalVoid" href="#support/tickets/message/<?=$ticket->id?>"><i class="icon-envelope icon-white"></i> Написать ответ</a>
				  </div>
				</div>
				<h3 data-key="<?=$ticket->id?>" id="ticket"><?=$ticket->title?></h3>
				<div class="clearfix"></div>
				<?php if (!count($ticket->messages)) { ?><div class="text-info text-center">Список сообщений пуст</div><?php } else { ?>
					<ul class="messages">
						<?php foreach ($ticket->messages as $message) : ?>
						<?php
							$datetime = date_create($message->cdate);
						?>
						<li class="message">
							<div class="media <?=($message->userGroup!=2)?'raketa':''?>"></div>
							<div class="info">
								<div class="title"><?=$message->userName?> <?=($message->userGroup==2)?' ('.$message->userEmail.')':''?> - <?=date_format($datetime, 'Y-m-d').' <span class="cGray">('.date_format($datetime,'H:i:s').')</span>'?></div>
								<div class="text"><?=$message->text?></div>
							</div>
							<div class="clearfix"></div>
						</li>
						<?php endforeach; ?>
					</ul>
				<?php } ?>
				<div class="clearfix"></div>
				<div class="btn-toolbar pull-right">
				  <div class="btn-group">
				    <a class="btn btn-success modalVoid" href="#support/tickets/message/<?=$ticket->id?>"><i class="icon-envelope icon-white"></i> Написать ответ</a>
				  </div>
				</div>
			</section>
		</div>
