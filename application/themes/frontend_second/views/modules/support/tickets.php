		<?php if (!count($tickets)) { ?><div class="text-info text-center">Список тикетов пуст</div><?php } else { ?>
		<div class="datagrid" id="groupsGrid">
			<table border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th width="20">id</th>
						<th width="60">Важность</th>
						<th width="60">Статус</th>
						<th width="80">Последнее сообщение</th>
						<th class="text-left" width="200">Пользователь</th>
						<th class="text-left">Тикет</th>
						<th width="90"></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					foreach($tickets as $ticket) : ?>
					<?php
						$datetime = date_create($ticket->lastMessage);
					?>
					<tr data-key="<?=$ticket -> id ?>" class="">
						<td><?=$ticket -> id ?></td>
						<td><?=ticketImportance($ticket->importance);?></td>
						<td>
						  <?=($ticket->unreadDiffence>0)?'<span class="label label-info">Новое сообщение</span><br/>':'';?>
						  <?=ticketStatus($ticket->status);?>
						</td>
						<td><?=date_format($datetime, 'Y-m-d').' <span class="cGray">('.date_format($datetime,'H:i:s').')</span>'?></td>
						<td class="text-left">
							  <?=$ticket->userName?> <span class="cGray">(<?=$ticket->userEmail?>)</span>
						</td>
						<td class="text-left">
							<strong><?=$ticket->title?></strong><br />
							Описание тикета: <em><?=$ticket->description?></em><br />
							Сообщений: <strong><?=$ticket->messagesCount?></strong>
						</td>
						<td class="action-table tooltip-top">
							<a href="/support/ticket/<?=$ticket -> id ?>" data-t="tooltip" title="Ответить" class="btn btn-mini btn-success"><i class="icon icon-envelope icon-white"></i></a>
							<a href="#support/tickets/delete" class="modalVoid btn btn-mini btn-danger" data-t="tooltip" title="Удалить"><i class="icon icon-remove icon-white"></i></a>
							<?php /*if(isset($locked)) { ?>
								<a href="#" class="groupUnlock btn btn-mini btn-warning" data-t="tooltip" title="Разблокировать"><i class="icon icon-ok icon-white"></i></a>
							<?php } else { ?>
								<a href="#" class="groupLock btn btn-mini btn-warning" data-t="tooltip" title="Заблокировать"><i class="icon icon-lock icon-white"></i></a>
							<?php } */ ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<?php } ?>
		
