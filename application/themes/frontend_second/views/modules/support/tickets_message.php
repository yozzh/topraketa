<?php
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
?>
<div class="row">
	<div class="span6">
		<? showErrors($errors)?>
	</div>
</div>
<div class="row">
	<form action="/support/tickets/message/<?=$pid?>" method="post" data-callback="ticketAdd">
	<div class="span6">
		<input type="hidden" name="pid" value="<?=$pid?>" id="id"/>
		<input type="hidden" name="status" value="save" id="save"/>
		<div class="control-group  <?=(form_error('text')!='')?'error':'';?>">
			<label for="text">Сообщение</label>
			<textarea name="text" id="text" class="input-block-level" style="height:140px;"><?=set_value('text');?></textarea>
		</div>
		<?php if($this->ion_auth->is_admin()) { ?>
		<div class="control-group  <?=(form_error('status')!='')?'error':'';?>">
			<label for="m_status">Изменить статус тикета</label>
			<select name="m_status" id="m_status">
				<option value="1" selected="true">Отвечено</option>
				<option value="2">Закрыто</option>
				<option value="3">Отложен</option>
			</select>
		</div>
		<?php } else { ?>
		<input type="hidden" name="m_status" value="0" id="m_status"/>
		<?php } ?>
		<div class="clear"></div>
		<p class="text-right">
			<input type="submit" class="ajaxSubmit button blue" value="Отправить сообщение"/>
		</p>
	</div>
	</form>
</div>