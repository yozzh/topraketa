<?php
//var_dump($users);
?>
		<div class="btn-toolbar">
		  <div class="btn-group">
		    <a class="btn modalVoid" href="#support/groups/add"><i class="icon-plus"></i> Добавить группу</a>
		  </div>
		</div>
		<?php if (!count($groups)) { ?><div class="text-info text-center">Список групп пуст</div><?php } else { ?>
		<div class="datagrid" id="groupsGrid">
			<table border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th width="20">id</th>
						<th class="text-left" width="200">Название</th>
						<th class="text-left">Описание</th>
						<th width="90"></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					foreach($groups as $group) : ?>
					<tr data-key="<?=$group -> id ?>">
						<td><?=$group -> id ?></td>
						<td class="text-left">
							  <?=$group->title?> <span class="cGray">(<?=$group->alias?>)</span>
						</td>
						<td class="text-left">
							<em><?=$group->description?></em>
						</td>
						<td class="action-table tooltip-top">
							<a href="#support/groups/edit" data-t="tooltip" title="Редактировать" class="btn btn-mini btn-info modalVoid"><i class="icon icon-edit icon-white"></i></a>
							<a href="#support/groups/delete" class="modalVoid btn btn-mini btn-danger" data-t="tooltip" title="Удалить"><i class="icon icon-remove icon-white"></i></a>
							<?php if(isset($locked)) { ?>
								<a href="#" class="groupUnlock btn btn-mini btn-warning" data-t="tooltip" title="Разблокировать"><i class="icon icon-ok icon-white"></i></a>
							<?php } else { ?>
								<a href="#" class="groupLock btn btn-mini btn-warning" data-t="tooltip" title="Заблокировать"><i class="icon icon-lock icon-white"></i></a>
							<?php } ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<?php } ?>
		
