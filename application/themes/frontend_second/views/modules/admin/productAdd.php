<?php
	$edit = isset($product); 
?>

<div class="grid">
	<div class="grid-title">
		<div class="pull-left">
			<div class="icon-title">
				<i class="icon-bookmark"></i>
			</div>
			
			<span><?=($edit)?'Редактирование продукта "'.$product->title.'"':'Новый продукт'?></span>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="grid-content">
		<form action="/admin/products/<?=($edit)?'edit/'.$product->pid:'add'?>" method="post" accept-charset="utf-8" class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="title">Название</label>
				<div class="controls">
					<input type="text" value="<?=($edit)?$product->title:''?>" name="title" id="title" placeholder="Название продукта">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="base_cost">Начальная стоимость</label>
				<div class="controls">
					<div class="input-append">
						<input class="span2" value="<?=($edit)?$product->base_cost:''?>" id="base_cost" type="text" name="base_cost">
						<span class="add-on">руб.</span>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="base_cost">Пороги стоимости</label>
				<div class="controls">
					<div class="productCases">
						<?php
							if ($edit && count($product->cases)) {
								foreach ($product->cases as $case) : ?>
								<div class="productCase">
									<div class="caseCount">
										<span><?=$case['count']?> шт.</span>
									</div>
									<div class="caseCost">
										<span><?=$case['cost']?> р.</span>
									</div>
									<div class="caseSave hidden">
										<a class="btn btn-small btn-success save">Сохранить</a>
									</div>
									<div class="caseActions">
										<a class="edit"><img src="<?=$tPath ?>images/icon/table_edit.png" alt=""></a>
										<a class="delete"><img src="<?=$tPath ?>images/icon/table_del.png" alt=""></a>
									</div>
									<div class="hidden">
										<input type="hidden" name="count[]" value="<?=$case['count']?>" class="count"/>
										<input type="hidden" name="cost[]" value="<?=$case['cost']?>" class="cost"/>
									</div>
									<div class="clearfix"></div>
								</div>
						<?php 	
								endforeach;
							}
						?>
					</div>
					<div class="clearfix"></div>
					<input class="addCase btn btn-info" type="button" value="Добавить" />
					<div class="hidden newCase">
						<div class="productCase">
							<div class="caseCount">
								<input type="number" name="f_count" value="0" id="f_count" class="span1"/>
							</div>
							<div class="caseCost">
								<input type="number" name="f_cost" value="0" id="f_cost" class="span1"/>
							</div>
							<div class="caseSave">
								<a class="btn btn-small btn-success save">Сохранить</a>
							</div>
							<div class="caseActions hidden">
								<a class="edit"><img src="<?=$tPath ?>images/icon/table_edit.png" alt=""></a>
								<a class="delete"><img src="<?=$tPath ?>images/icon/table_del.png" alt=""></a>
							</div>
							<div class="hidden">
								<input type="hidden" name="count[]" value="0" class="count"/>
								<input type="hidden" name="cost[]" value="0" class="cost"/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
			<hr />
			<input type="submit" value="Сохранить" class="btn btn-success"/>
		</form>
	</div>
</div>