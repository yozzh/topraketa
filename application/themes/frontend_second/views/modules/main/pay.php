<div class="row addMoney">
	<section class="offset4 span4"><p>
		<div class="addMoneyBlock text-center">
			<?
				if (isset($payform)) {
			?>
			<div class="alert alert-info">
				Пополнение счета на <?=$default_value?> <?=proceedTextual($default_value, "рублей", "рубль", "рубля");?>.
			</div>
					<?=$payform;?>
			<?php
				} else {
			?>
				Укажите нужную сумму в рублях и перейдите к оплате 
			</p>
			<?
			if (isset($error))
				echo '<div class="alert alert-error">' . $error . '</div>';
			?>
			<form action="" method="post">
				<div class="input-append text-center">
					<input type="text" name="count" value="<?=$default_value ?>" />
					<input type="submit" name="submit" value="Пополнить" class="btn btn-success" />
				</div>
			</form>
			<?php
				}
			?></div>
		</section>
</div>