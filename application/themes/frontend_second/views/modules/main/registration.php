<?php
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
?>
<div class="row">
	<div class="offset3 span6">
		<? showErrors($errors)
			?>
	</div>
</div>
<div class="row">
	<div class="offset3 span6">
		<form action="/main/registration" class="form-login" method="post">
			<div class="control-group <?=(form_error('name')!='')?'error':'';?>">
				<label for="name">Ваше имя*</label>
				<input type="text" name="name" value="<?=set_value('name') ?>" id="name" placeholder="Ваше имя" class="input-block-level" />
			</div>
			<div class="control-group <?=(form_error('email')!='')?'error':'';?>">
				<label for="email">Ваш E-mail*</label>
				<input name="email" type="email" value="<?=set_value('email') ?>" placeholder="Электронная почта" class="input-block-level" />
			</div>
			<div class="control-group <?=(form_error('phone')!='')?'error':'';?>">
				<label for="phone">Контактный телефон</label>
				<input name="phone" type="text" value="<?=set_value('phone') ?>" placeholder="+7 *** *** ** **" class="input-block-level" />
			</div>
			<div class="control-group <?=(form_error('password')!='')?'error':'';?>">
				<label for="password">Пароль и его подтверждение*</label>
				<div class="row">
					<div class="span3"><input name="password" type="password"  placeholder="Пароль" class="input-block-level" /></div>
					<div class="span3"><input name="passconf" type="password"  placeholder="Подтверждение пароля"  class="input-block-level"/></div>
				</div>
			</div>
		
			<div class="clear"></div>
			<p class="text-right">
				<input type="submit" class="button blue" value="Регистрация"/>
			</p>
		
		</form>
	</div>
</div>
