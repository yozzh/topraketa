<?php
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
?>
<div class="row">
	<div class="offset3 span6">
		<? showErrors($errors)
			?>
	</div>
</div>
<div class="row">
	<div class="offset3 span6">
		<form action="/main/forgot" class="form-login" method="post">
			<div class="control-group <?=(form_error('email')!='')?'error':'';?>">
				<label for="email">Ваш E-mail, указанный при регистрации</label>
				<input name="email" type="email" value="<?=set_value('email') ?>" placeholder="Электронная почта" class="input-block-level" />
			</div>		
			<div class="clear"></div>
			<p class="text-right">
				<input type="submit" class="button blue" value="Восстановить пароль"/>
			</p>
		
		</form>
	</div>
</div>
