		<?php if (!count($items)) { ?><div class="text-info text-center">Список платежей пуст</div><?php } else { ?>
		<div class="datagrid" id="projectsGrid">
			<table border="0" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th width="70">Статус</th>
					<th width="70">Дата</th>
					<th width="100">Сумма</th>
					<th class="text-left">Описание</th>
					
				</tr>
			</thead>
			<tbody>
			<?php $total = 0; ?>
			<?php foreach($items as $item) : ?>
			<?php
				$itemStatus = moneyStatus($item->status);
				if ($item->status==2) {
					$total += $item->value;	
				}
				$dateTime = ($item->date!=0) ? new DateTime($item->date):null;
				
			?>
				<tr class="<?=($item->status==3)?'error':'';?>">
					<td><span class="label label-<?=$itemStatus['class']?>"><?=$itemStatus['title']?></span></td>
					<td><span class="cGray"><?=$dateTime -> format('Y-m-d') ?></span> <span class="cLightGray"><?=$dateTime -> format('H:i:s') ?></td>
					<td class="text-center"><?=moneySum($item->value)?></td>
					<td class="text-left"><em><?=$item->comment?></em></td>
				</tr>
			<?php endforeach; ?>
			<tfoot>
				<tr>
					<td></td>
					<td class="text-right">Итого:</td>
					<td class="text-center"><strong><?=moneySum($total) ?></strong></td>
					<td></td>
				</tr>
			</tfoot>
			</tbody>
		</table>
	</div>
		<?php } ?>