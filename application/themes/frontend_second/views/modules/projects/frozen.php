<?php
	//var_dump($project);
	$haveKeywords = (bool)(count($project->keywords));
?>
<div class="keyList">
	<h4>Список запросов проекта</h4>
	<label>Ниже представлен итоговый список запросов, по которому будет продвигаться ваш сайт</label>
	<div class="datagrid <?=($haveKeywords)?'':'hidden';?>" id="keywordsTable">
		<table id="keywordsTable">
			<thead>
				<tr>
					<th width="40">Регион</th>
					<th width="200" class="text-left">Ключевая фраза</th>
					<th>Страница</th>
					<th width="100">Бюджет</th>
					<th width="90"></th>
				</tr>
			</thead>
			<tbody>
				<?php $sum = 0;
					//var_dump($project);
				?>
				<?php foreach ($project->keywords as $keyword ) : ?>
					<tr data-key="<?=$keyword->id?>">
						<td>
							<div class="regions tooltip-right">
								<a class="yaIcon" data-toggle="tooltip" title="<?//=$keyword['ygeotitle']?>"></a>
								<a class="gIcon" data-toggle="tooltip" title=""></a>						
							</div>
						</td>
						<td class="text-left"><a href="/projects/links/<?=$project->id?>/<?=$keyword->id?>"><?=$keyword->text ?></a></td>
						<td class="text-left"><a href="<?='http://' . $project->url . $keyword->url ?>" class="longString" target="_blank"><?=$keyword->url ?></a></td>
						<td class="text-left" width="200">
							<ul>
								<li>Бюджет: <strong><?=$keyword->budget?> р./мес</strong></li>
								
							</ul>
						</td>
						<td class="action-table tooltip-top" width="90">
							 <!-- <a href="#" class="keywordEdit btn btn-mini btn-info" data-toggle="tooltip" title="Редактировать"><i class="icon icon-edit icon-white"></i></a> -->
							<!-- <a href="#" class="keywordDelete btn btn-mini btn-danger" data-toggle="tooltip" title="Удалить"><i class="icon icon-remove icon-white"></i></a> -->
							<?php /*<a href="/projects/keywordStat/<?=$keyword['id'] ?>" class="btn btn-mini btn-success" data-toggle="tooltip" title="Позиции"><i class="icon icon-signal icon-white"></i></a> */ ?>
						</td>
					</tr>
					<?php $sum+=$keyword->budget; ?>
				<?php endforeach; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3" class="text-right">Итоговый бюджет:</td>
					<td class="summaryBudget"><?=$sum?> р./мес</td>
					<td colspan="1"></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<div class="alert alert-info text-center <?=($haveKeywords)?'hidden':'';?>" id="keywords_02_empty">Список запросов пуст</div>
</div>