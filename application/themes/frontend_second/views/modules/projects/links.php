<?php
	$notLinkUsers = array(53,4);
	$user = $this->ion_auth->user()->row();
	$notLinks = (in_array($user->id, $notLinkUsers));
?>
		<?php if (!count($items)) { ?><div class="text-info text-center">Список ссылок пуст</div><?php } else { ?>
		<div class="datagrid" id="projectsGrid">
			<table border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th width="20"><input type="checkbox" value="1" class="totalCheckbox"/></th>
						<th class="text-left">Страница</th>
						<th width="400" class="text-left">Анкор</th>
					<?php if (!$notLinks) : ?>
						<th width="65">Размещена</th>
					<?php endif;?>
						<th width="100">Стоимость</th>
						<th width="30"></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$i=1;
					$sum=0;
					foreach($items as $item) : ?>
					<?php
					 
					$dateTime = ($item['stamp']!=0) ? new DateTime($item['stamp']):null;
					?>
					<tr data-key="<?=$item['id']?>">
						<td><input type="checkbox" name="link[]" value="1"/></td>
						<td class="text-left">
							<a href="http://<?=$item['url']?>" target="_blank"><?=longString($item['url'],40);?></a>  
						</td>
						<td class="text-left"><a href="<?=$item['anchor']?>" target="_blank"><?=$item['anchor']?></a></td>
					<?php if (!$notLinks) : ?>
						<td>
							<?php if ($dateTime != null) { ?>  
								<span class="cGray"><?=$dateTime -> format('Y-m-d') ?></span> <span class="cLightGray"><?=$dateTime -> format('H:i:s') ?>
							<?php } else { ?> 
								<span class="cGray">Неизвестно</span>
							<?php } ?>
						</td>
					<?php endif;?>
						<td><?=$item['linkcost'] ?> р./день</td>
						<td class="action-table tooltip-top">
							<a href="/projects/delete/<?=$item['id'] ?>" class="linkDelete btn btn-mini btn-danger" data-t="tooltip" title="Удалить"><i class="icon icon-remove icon-white"></i></a>
						</td>
					</tr>
				<?php $i++;
				$sum+=$item['linkcost'];
						endforeach;
	?>
				</tbody>
				<tfoot>
					<?php $colspan = ($notLinks)?3:4; ?>
				<tr>
					<td colspan="<?=$colspan?>" class="text-right">Итоговая сумма:</td>
					<td class="summaryBudget"><?=$sum?> р./день</td>
					<td colspan="2"></td>
				</tr>
			</tfoot>
			</table>
		</div>
		<?php } ?>