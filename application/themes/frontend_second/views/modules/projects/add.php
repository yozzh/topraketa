<?php
$edit = isset($project);
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
if (isset($projectId)) {
	$domainValue = $projectDomain;
	$formUrl = "/addNewKeywords/".$projectId;
	$buttonTitle = 'Добавить запросы';
} else {
	$domainValue = set_value('domain');
	$formUrl = ($edit) ? 'edit/' . $project -> id : 'add';
	$buttonTitle = 'Добавить проект';
}
?>
<form action="/projects/<?=$formUrl?>" method="post" accept-charset="utf-8">
	<div class="row">
		<section class="span12">
			<? showErrors($errors);	?>
		</section>
	</div>
	<div class="row">
		<section class="span1">
			<div class="formNumber">1</div>
		</section>
		<section class="span5">
			<div class="control-group <?=(form_error('domain')!='')?'error':'';?>">
				<h4>Продвигаемый сайт:</h4>
				<label for="domain">Доменное имя сайта, который вы хотите продвигать</label>
				<input type="text" name="domain" value="<?=$domainValue?>" id="domain" placeholder="yoursite.ru" class="input-block-level" <?=(isset($projectId))?'disabled="disabled"':''?>'/>
			</div>
		</section>
		<section class="span6">
			<div class="row">
				<section class="span6">
					<h4>Регион продвижения:</h4>
				</section>
			</div>
			<div class="row">
				<section class="span3">
					<div class="control-group" id="selectYandexRegion">
						<label for="ya_is_region" class="checkbox">
							<input type="checkbox" name="ya_is_region" value="" id="ya_is_region"/>
							Яндекс</label>
						<input type="text" data-provide="typeahead" name="ya_region" id="ya_region" class="input-block-level" disabled="disabled" placeholder="Москва" autocomplete="off">
						<input type="hidden" name="ya_region_hidden" value="198" id="ya_region_hidden"/>
						<input type="hidden" name="ya_region_hidden2" value="213" id="ya_region_hidden2"/>
					</div>
				</section>
				<section class="span3">
					<div class="control-group" id="selectGoogleRegion">
						<label for="g_is_region" class="checkbox">
							<input type="checkbox" name="g_is_region" value="" id="g_is_region"/>
							Google</label>
						<input type="text" data-provide="typeahead" name="g_region" id="g_region" value="" class="input-block-level" disabled="disabled" placeholder="Россия" autocomplete="off">
						<input type="hidden" name="g_region_hidden" value="0" id="g_region_hidden"/>
					</div>
				</section>
			</div>
		</section>
	</div>
	<div class="row">
		<section class="span1">
			<div class="formNumber">2</div>
		</section>
		<section class="span11">
			<div class="control-group <?=(form_error('keywords_02')!='')?'error':'';?>">
				<h4>Продвигаемые запросы:</h4>
				<label for="keywords_01">Введите запросы (каждый запрос с новой строки):</label>
				<textarea name="keywords_01" id="keywords_01" class="input-block-level" style="height: 200px;"></textarea>
				<div class="text-right mt10">
					<a class="btn btn-info addKeywords"><i class="icon icon-download icon-white"></i> Добавить запросы</a>
				</div>
			</div>
		</section>
	</div>
	<div class="row">
		<section class="span1">
			<div class="formNumber">3</div>
		</section>
		<section class="span11">
			<h4>Итоговый список запросов:</h4>
			<label>Ниже представлен итоговый список запросов, по которому будет продвигаться ваш сайт</label>
			<div class="datagrid" id="keywords_02" style="display: none">
				<table id="keywordsTable">
					<thead>
						<tr>
							<th width="40">Регион</th>
							<th width="200">Ключевая фраза</th>
							<th>Посетителей</th>
							<th>Показов</th>
							<th>Бюджет</th>
							<th>Страница</th>
							<th width="20"></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4" class="text-right">Итоговая сумма:</td>
							<td class="summaryBudget"></td>
							<td colspan="2"></td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="alert alert-info text-center" id="keywords_02_empty">Список запросов пуст</div>
			<input type="hidden" name="keywords_02" value="<?=set_value('keywords_02')?>" id="keywordsInput_02"/>
		</section>
	</div>
	<div class="row mt10">
		<section class="offset1 span11">
			<hr />
		</section>
	</div>
	<div class="row mt10">
		<section class="offset1 span5">
			<a href="#" class="btn btn-danger">Отмена</a>
		</section>
		<section class="span6 text-right">
			<a href="#" class="btn btn-success" id="addProject"><?=$buttonTitle?></a>
		</section>
	</div>
</form>

