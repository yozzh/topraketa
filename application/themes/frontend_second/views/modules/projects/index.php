
		<?php if (is_array($frozen) && count($frozen)) { ?>
			<h4>Замороженные проекты</h4>
			<div class="datagrid gray" id="projectsGrid">
				<table border="0" cellspacing="0" cellpadding="0">
					<thead>
						<tr>
							<th width="20">#</th>
							<th class="text-left">Проект</th>
							<th width="100">Бюджет</th>
							<th width="60"></th>
						</tr>
					</thead>
					<tbody>
					<?php 
						$i=1;
						$sum = 0;
						//var_dump($items);
						foreach($frozen as $item) : ?>
						<?php
						$dateTime = new DateTime($item->cdate);
						?>
						<tr>
							<td><?=$i ?></td>
							<td class="text-left">
								<i class="icon icon-globe"></i> <a href="/projects/frozen/<?=$item->id?>" class="projectLink"><?=$item->url ?></a><br />
								<i class="icon icon-calendar"></i> <span class="cGray"><?=$dateTime -> format('Y-m-d') ?></span> <span class="cLightGray"><?=$dateTime -> format('H:i:s') ?></span>  
							</td>
							<td><?=($item->budget>0)?round($item->budget,2).' р./мес':'Обрабатывается'?></td>
							<td class="action-table tooltip-top">
								<? /*<a href="#projects/edit/<?=$item['id'] ?>" data-t="tooltip" title="Редактировать" class="modalVoid btn btn-mini btn-info"><i class="icon icon-edit icon-white"></i></a> */?>
								<a href="/projects/startFrozen/<?=$item->id ?>" data-t="tooltip" title="Запустить проект" class="btn btn-mini btn-success"><i class="icon icon-refresh icon-white"></i></a>
								<a href="/projects/deleteFrozen/<?=$item->id ?>" class="productDelete btn btn-mini btn-danger" data-t="tooltip" title="Удалить"><i class="icon icon-remove icon-white"></i></a>
								
							</td>
						</tr>
					<?php $i++;
						  $sum+=$item->budget;
							endforeach;
		?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" class="text-right">Итого:</td>
							<td colspan="1"><?=round($sum,2) ?> р./мес</td>
							<td colspan="1"></td>
						</tr>
					</tfoot>
				</table>
			</div>
		<?php } ?>
		<h4>Список запущенных проектов</h4>
		<?php if (!count($items)) { ?>
			
			<div class="text-info text-center">Пока список проектов пуст<br /><br />
				<a href="/projects/add" class="btn btn-success"><i class="icon-white icon-plus"></i> Добавить проект</a>
			</div>
		<?php } else { ?>
		<div class="datagrid" id="projectsGrid">
			<table border="0" cellspacing="0" cellpadding="0">
				<thead>
					<tr>
						<th width="20">#</th>
						<th class="text-left">Проект</th>
						<th width="100">ТОП-10</th>
						<th width="65">Куплено</th>
						<?php //<th width="65">Тексты</th>?>
						<th width="100">Бюджет</th>
						<th width="100">Расход</th>
						<th width="60"></th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$i=1;
					$sum = 0;
					//var_dump($items);
					foreach($items as $item) : ?>
					<?php
					$dateTime = new DateTime($item['created']);
					?>
					<tr data-key="<?=$item['id']?>">
						<td><?=$i ?></td>
						<td class="text-left">
							<i class="icon icon-globe"></i> <a href="/projects/project/<?=$item['id']?>" class="projectLink"><?=$item['domain'] ?></a><br />
							<i class="icon icon-calendar"></i> <span class="cGray"><?=$dateTime -> format('Y-m-d') ?></span> <span class="cLightGray"><?=$dateTime -> format('H:i:s') ?></span>  
						</td>
						<td class="large"><strong><span class="cRed"><?=$item['yposcounttop10']?></span> / <span class="cBlue"><?=$item['gposcountTop10']?></span></strong></td>
						<td><a href="/projects/links/<?=$item['id']?>"><?=$item['linkcount'] ?></a></td>
						<?php /*<td><?=$item['linktexts'] ?></td> */?>
						<td><?=($item['linkbudget']>0)?round($item['linkbudget'],2).' р./мес':'Обрабатывается'?></td>
						<td><span class="<?=($item['linkcost']==0)?'cGray':'';?>"><?=$item['linkcost']?> р./день</span></td>
						<td class="action-table tooltip-top">
							<? /*<a href="#projects/edit/<?=$item['id'] ?>" data-t="tooltip" title="Редактировать" class="modalVoid btn btn-mini btn-info"><i class="icon icon-edit icon-white"></i></a> */?>
							<?php //if ($this->ion_auth->is_admin()) : ?>
								<a href="/projects/freeze/<?=$item['id'] ?>" data-t="tooltip" title="Заморозить проект" class="btn btn-mini btn-info"><i class="icon icon-asterisk icon-white"></i></a>
							<?php //endif; ?>
							<a href="" class="projectDelete btn btn-mini btn-danger" data-t="tooltip" title="Удалить"><i class="icon icon-remove icon-white"></i></a>
							
						</td>
					</tr>
				<?php $i++;
					  $sum+=$item['linkbudget'];
						endforeach;
	?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4" class="text-right">Итого:</td>
						<td colspan="1"><?=$sum ?> р./мес</td>
						<td colspan="2"></td>
					</tr>
				</tfoot>
			</table>
		</div>
		<?php } ?>