<div class="keywordEditDiv">
	<h5>Редактирование запроса "<?=$keyword['word'] ?>"</h5>
	<form action="/projects/editKeyword" method="post" data-callback="keywordSaved">
		<input type="hidden" name="id" value="<?=$keyword['id']?> "/>
		<input type="hidden" name="status" value="save"/>
		<div class="row-fluid">
			<section class="span3">
				<div class="control-group">
					<label for="url">Домен проекта</label>
					<input type="text" name="url" id="url" class="input-block-level" value="<?=$keyword['url'] ?>">
				</div>
			</section>
			<section class="span3">
				<div class="control-group" id="selectYandexRegion">
					<label for="ya_region">Яндекс.Регион</label>
					<input type="text" data-provide="typeahead" value="<?=$keyword['ygeotitle'] ?>" name="ya_region" id="ya_region" class="input-block-level ya_region" placeholder="Москва" autocomplete="off">
					<input type="hidden" name="ya_region_hidden" value="<?=$keyword['ygeo'] ?>" id="ya_region_hidden"/>
				</div>
				<div class="control-group" id="selectGoogleRegion">
					<label for="g_region">Google.Регион</label>
					<input type="text" data-provide="typeahead" value="<?=$keyword['ggeotitle'] ?>" name="g_region" id="g_region" value="" class="input-block-level g_region" placeholder="Россия" autocomplete="off">
					<input type="hidden" name="g_region_hidden" value="<?=$keyword['ggeo'] ?>" id="g_region_hidden"/>
				</div>
			</section>
			<section class="span3">
				<div class="control-group">
					<label for="url">Бюджет</label>
					<input type="text" name="budget" id="budget" class="input-block-level" value="<?=$keyword['budget'] ?>">
				</div>
			</section>
			<section class="span3">
				<label for="url">Сохранить измененные данные?</label>
				<a href="#" class="btn btn-success saveKeyword input-block-level ajaxSubmit">Сохранить</a>
			</section>
		</div>
	</form>
</div>