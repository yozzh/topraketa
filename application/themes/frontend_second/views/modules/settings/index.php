<?php
$errors = (isset($errors)) ? $errors : array();
$validation = errorsConvert(validation_errors());
$errors = array_merge($errors, $validation);
$firstName = ($user->first_name == $user->email)?"":$user->first_name;
//var_dump($user);
?>
<div class="row">
	<div class="offset2 span8">
		<? showErrors($errors)
			?>
	</div>
</div>
<div class="row">
	<div class="offset2 span8">
		<form action="/settings" class="form-login" method="post">
		<ul class="nav nav-pills tabWidget">
		  <li class="active"><a href="#profile" data-toggle="tab">Профиль</a></li>
		  <li><a href="#partnership" data-toggle="tab">Партнерская программа</a></li>
		  <li><a href="#messages" data-toggle="tab">Уведомления</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="profile">
					<div class="control-group  <?=(form_error('first_name') != '') ? 'error' : ''; ?>">
						<label for="first_name">Ваше имя*</label>
						<input type="text" name="first_name" value="<?=(set_value('first_name') != '') ? set_value('first_name') : $firstName; ?>" id="first_name" placeholder="Ваше имя" class="input-block-level" />
					</div>
					<div class="control-group  <?=(form_error('email') != '') ? 'error' : ''; ?>">
						<label for="email">Ваш E-mail*</label>
						<input name="email" id="email" type="email" value="<?=(set_value('email') != '') ? set_value('email') : $user -> email; ?>" placeholder="Электронная почта" class="input-block-level" />
					</div>
					<div class="control-group  <?=(form_error('phone') != '') ? 'error' : ''; ?>">
						<label for="phone">Контактный телефон</label>
						<input name="phone" id="phone" type="text" value="<?=(set_value('phone') != '') ? set_value('phone') : $user -> phone; ?>" placeholder="+7 *** *** ** **" class="input-block-level" />
					</div>
					<div class="control-group  <?=(form_error('password') != '') ? 'error' : ''; ?>">
						<label for="password">Пароль и его подтверждение*</label>
						<div class="row">
							<div class="span3"><input name="password" id="password" type="password"  placeholder="Пароль" class="input-block-level" /></div>
							<div class="span3"><input name="passconf" id="passconf" type="password"  placeholder="Подтверждение пароля"  class="input-block-level"/></div>
						</div>
					</div>
					<input type="hidden" name="user_id" value="<?=$user -> id ?>" id="user_id"/>
					
				
			</div>
			<div class="tab-pane" id="partnership">
				<div class="control-group">
					<h4>Ваша реферальная ссылка</h4>
					<div class="alert alert-info text-center copyContainer">
						<h6><?=(isset($partnershipLink) ? $partnershipLink : '') ?></h6>
						<a href="#" class="btn btn-mini btn-info" title="Ссылка скопирована в буфер обмена" data-clipboard-text="<?=(isset($partnershipLink) ? $partnershipLink : '') ?>" id="copy-button">
							<i class="icon-white icon-share"></i> Скопировать в буфер обмена
						</a>
					</div>
					<h4>Привлеченные клиенты</h4>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Дата</th>
								<th>Email</th>
								<th>Статус</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($clients as $client): ?>
							<?php
								$firstName = ($client->first_name == $client->email)?"<span class=\"cRed\">Не заполнено!</span>":$client->first_name;
								$dateTime = ($client->created_on!=0) ? new DateTime():null;
								if ($dateTime!=null) {
									$dateTime->setTimestamp($client->created_on);
								}
							?>
								<tr  class="<?=($client->remember_code==NULL)?'error':'success';?>">
									<td><?=$dateTime -> format('Y-m-d') ?></span> <span class="cLightGray"><?=$dateTime -> format('H:i:s') ?></td>
									<td>Имя: <strong><?=$firstName?></strong><br />E-mail: <?=$client->email?></td>
									<td><?=($client->remember_code==NULL)?'Не активирован':'Активирован';?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="tab-pane" id="messages">...</div>
			
		</div>
		<div class="clear"></div>
		<p class="text-right">
			<input type="submit" class="button blue" value="Сохранить изменения"/>
		</p>
		</form>
	</div>
</div>