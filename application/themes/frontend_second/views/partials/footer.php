</div>
								</section>
							</div>
						</div>
					</div>
					<div class="partFooter"></div>
				</section>
			</div>
			<footer>
				<div class="footerWrapper">
					<div class="footerContent">
						
					</div>
				</div>
				<div class="footerPlate">
					<div class="container">
						<div class="row">
							<section class="span12 posR">
								<p><a  href="/include/docs/rules.pdf" target="_blank">Правовая информация</a><br />
									<span class="txtSmall">Copyright 2013 Rocket. Все права защищены</span>
								</p>
								<div class="rocket"></div>
							</section>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter21911587 = new Ya.Metrika({id:21911587,
webvisor:true,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21911587" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- webim button generation date: 2013-08-29 version: 8.2.0 -->
<script type="text/javascript">
  document.write('<a class="webim_button" href="#" rel="webim"><img src="' + document.location.protocol + '//topraketaru.webim.ru/webim/button.php" border="0"/></a>');

  webim = {
    accountName: "topraketaru",
    domain: "topraketaru.webim.ru"
  };
  (function () {
      var s = document.createElement("script");
      s.type = "text/javascript";
      s.src = document.location.protocol + "//topraketaru.webim.ru/webim/js/button.js";
      document.getElementsByTagName("head")[0].appendChild(s);
  })();
</script>
<!-- /webim button -->


		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>
			window.jQuery || document.write('<script src="<?=$tPath?>js/vendor/jquery-1.9.1.min.js"><\/script>')
		</script>

		<script src="<?=$tPath?>js/vendor/bootstrap.min.js"></script>
		<script src="<?=$tPath?>js/vendor/less.js"></script>
		<script src="<?=$tPath?>js/vendor/scrollspy.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$tPath?>js/vendor/jquery-loadmask-0.4/jquery.loadmask.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$tPath?>js/vendor/zero-clipboard/zero-clipboard.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?=$tPath?>js/main.js"></script>
		<script src="<?=$tPath?>js/cab.js"></script>
		<?php
			if (isset($scripts)) {
				foreach ($scripts as $script) {
					echo '<script src="'.$tPath.'js/'.$script.'"></script>';
				}
			}
		?>
		
	</body>
</html>
