<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
	<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php echo $template['title']; ?> | Top-Raketa.ru</title>
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,700,400italic,700italic&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?=$tPath?>css/bootstrap.min.css">
		<link rel="stylesheet/less" type="text/css"" href="<?=$tPath?>css/main.less">
		<link rel="stylesheet" href="<?=$tPath?>css/second.css">
		<link rel="stylesheet" href="<?=$tPath?>css/ui.css">
		<link rel="stylesheet" href="<?=$tPath?>js/vendor/jquery-loadmask-0.4/jquery.loadmask.css">
		<?=$template['metadata']?>
		<script src="<?=$tPath?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<link rel="icon" type="image/png" href="/include/favicon.png" />
	</head>
	<body>
		<?=magicEyeInformer($this->session)?>
		<div class="bodyWrapper">
			<header class="smartPart">
				<div class="headerWrapper" id="info">
					<div class="girlWrapper">
						<div class="container top">
							<div class="row">
								<section class="span5">
									<a href="/projects" class="logo"><img src="<?=$tPath?>img/logo.png" alt="" width="279" height="105" /></a>
								</section>
								<section class="span7">
									<?=$template['partials']['mainMenu']?>
								</section>
							</div>
						</div>
						<div class="infoWrapper">
							<div class="container">
								<div class="row">
									<section class="span9 offset3">
										<section class="informer" id="informer">
											<?=$template['partials']['informer']?>
										</section>
										<?php if ($this->ion_auth->logged_in()) { ?>
										<section class="menu">
											<ul>
												<li>
													<a href="/projects" class="menuItemProjects"><span class="title">Проекты</span><span class="subtitle">СПИСОК ВСЕХ ВАШИХ<br />АКТУАЛЬНЫХ ПРОЕКТОВ</span></a>
												</li>
												<li>
													<a href="/money" class="menuItemFinance"><span class="title">Финансы</span><span class="subtitle">Пополнение баланса и<br />контроль расходов</span></a>
												</li>
												<li>
													<a href="/support" class="menuItemSupport">
													  <?=$template['partials']['unread']?>
													  <span class="title">Поддержка</span><span class="subtitle">Решение технических и<br />финансовых вопросов</span></a>
												</li>
											</ul>
										</section>
										 <?php } ?>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="mainParts" id="mainParts">
				<div class="whiteBg"></div>
				<section class="mainPart">
					<div class="partHeader"></div>
					<div class="partMiddle">
						<div class="container">
							<div class="row">
								<section class="span12">
									<div class="partTitle shadow" id="h1">
										<?php echo $template['title']; ?>
									</div>
								</section>
							</div>
							<div class="row">
								<section class="span12">
									<? if (isset($subtitle) || isset($subMenu)) : ?>
									<div class="navbar">
										<div class="navbar-inner">
											<? if (isset($subtitle) && $subtitle!="") :?>
												<a class="brand" href="#"><?=$subtitle?></a>
											<? endif; ?>
											<? if (isset($subMenu) && $subMenu!=""): ?>
												<?=$subMenu?>
											<? endif; ?>		
										</div>
									</div>
									<? endif; ?>
									
									<div class="partContent">
										<?php if ($this->session->flashdata('success')!="") { ?>
											<div class="alert alert-success">
											  <button type="button" class="close" data-dismiss="alert">&times;</button>
											  <?=$this->session->flashdata('success')?>
											</div>
										<?php } ?>
										<?php if ($this->session->flashdata('warning')!="") { ?>
											<div class="alert alert-warning">
											  <button type="button" class="close" data-dismiss="alert">&times;</button>
											  <?=$this->session->flashdata('warning')?>
											</div>
										<?php } ?>
									