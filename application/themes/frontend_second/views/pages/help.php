<div class="row">
	<div class="span3">
	  <h3>Навигация</h3>
	  <ul class="nav nav-pills nav-stacked">
	    <?php foreach ($menu as $item) : ?>
	      <li <?=($item->url==$page->url)?'class="active"':'';?>><a href="/help/<?=$item->url?>"><?=$item->title?></a></li>
	    <?php endforeach; ?>
	  </ul>
	</div>
	<div class="span9">
	  <h3><?=$page->title?></h3>
	  <?=$page->body?>
	</div>
</div>
