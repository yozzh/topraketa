<!DOCTYPE html>
<html lang="en"  class="body-error"><head>
    <meta charset="utf-8">
    <title><?php echo $template['title']; ?> | Личный кабинет</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?=$tPath?>css/login.css" rel="stylesheet">
    <link href="<?=$tPath?>css/bootstrap.css" rel="stylesheet">


	<link rel="stylesheet" href="<?=$tPath?>css/icon/font-awesome.css">    
    <link rel="stylesheet" href="<?=$tPath?>css/bootstrap-responsive.css">
	
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="/images/icons/favicon.ico">
    
  </head>

  <body>

        <div id="wrapper">
            <?php echo $template['body']; ?>
    	</div>  
    
   
    
    

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>


  </body>
</html>

