<?php
	function orderStatus($id) {
		$orderStates = array(
			0 => array (
				'title' => 'В ожидании',
				'class' => 'danger'
			),
			1 => array (
				'title' => 'В обработке',
				'class' => 'warning'
			),
			2 => array (
				'title' => 'Выполнен',
				'class' => 'success'
			),
			3 => array (
				'title' => 'Отменен',
				'class' => 'important'
			),
		);
		
		return $orderStates[$id];
	}
?>