<?php
function ticketImportance($value) {
	$title = 'Обычная';
	$class = 'label-success';
	switch ($value) {
		case -10:
			$title = 'Не срочно';
			$class = '';
			break;
		case 10:
			$title = 'Срочно';
			$class = 'label-important';
			break;
	}
	$html = '<span class="label %s">%s</span>';
	return sprintf($html,$class,$title);	
}

function ticketStatus($value) {
	$title = 'Ждет ответа';
	$class = 'label-danger';
	switch ($value) {
		case 1:
			$title = 'Отвечено';
			$class = 'label-success';
			break;
		case 2:
			$title = 'Закрыто';
			$class = '';
			break;
		case 3:
			$title = 'Отложен';
			$class = 'label-info';
			break;
	}
	$html = '<span class="label %s">%s</span>';
	return sprintf($html,$class,$title);	
}
?>
