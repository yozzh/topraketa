<?php
class HammerUrl {
	
	public static $START = 'http://api.seohammer.ru/start.asmx?WSDL';
	public static $PROJECTS = 'http://api.seohammer.ru/projects.asmx?WSDL';
	public static $KEYS = 'http://api.seohammer.ru/keys.asmx?WSDL';
	public static $TEXTS = 'http://api.seohammer.ru/texts.asmx?WSDL';
	public static $DICTIONARY = 'http://api.seohammer.ru/dictionary.asmx?WSDL';
	public static $FINANCE = 'http://api.seohammer.ru/finance.asmx?WSDL';
	public static $DEEP = 'http://api.seohammer.ru/vip/deep.asmx?wsdl';	
	public static $LINKS = 'http://api.seohammer.ru/links.asmx?WSDL';

}
?>