<?php
function showErrors($errors) {
	if (!empty($errors)) {
		echo '
			<div class="alert alert-danger">
				<h5>Ошибка</h5>
				<ul>';

					foreach ($errors as $key => $error) {
						if ($error!='') {
							echo '<li>'.$error.'</li>';
						}
					}
		echo '					
				</ul>
			</div>
		';
	}
}

function errorsConvert($string) {
	$errors = explode("\n",$string);
	if (count($errors==1) && $errors[0]=="") return array();
	return $errors;
	
}
?>