<?php
	function renderPositions($ypos,$yold,$gpos,$gold) {
		//$ypos = 10; $yold=11; $gpos = 10; $gold=8;
		
		$ypos = ($ypos < 0 )?'-':$ypos;
		$gpos = ($gpos < 0 )?'-':$gpos;
		$yDifference = ($yold>0)?-1*($ypos-$yold):0;
		$gDifference = ($gold>0)?-1*($gpos-$gold):0;
		$html = '<div class="positions">';
		$html .= '<div class="yandexPos '.getDefClass($yDifference).'"><span class="pos">'.$ypos.'</span>';
		if ($yDifference!=0) {
			$yDifference=($yDifference>0)?'+'.$yDifference:$yDifference;
			$html.='<span class="def">'.$yDifference.'</span>';
		}
		$html .='</div>';
		$html .= '<div class="googlePos '.getDefClass($gDifference).'"><span class="pos">'.$gpos.'</span>';
		if ($gDifference!=0) {
			$gDifference=($gDifference>0)?'+'.$gDifference:$gDifference;
			$html.='<span class="def">'.$gDifference.'</span>';
		}
		$html .='</div>';
		$html .= '</div>';
		return $html;
	}
	
	function getDefClass($def) {
		if ($def==0) return '';
		if ($def>0) return 'posPlus';
			else return 'posMinus';
	}
	
	function longString($string, $length=30) {
		if (strlen($string)>$length) {
			$newString = substr($string, 0, $length).'...';
			$newString = sprintf('<span data-toggle="tooltip" title="%s">%s</span>',$string,$newString);
			return $newString;
		} else {
			return $string;
		}
	}

	function magicEyeInformer($session) {
		if ($session->userdata('adminId')) {
			$html = '<div class="alert alert-danger" style="margin-bottom:0;"><div class="container"><div class="row"><div class="span12">
				<a href="/main/unmagicEye" class="btn btn-mini btn-danger pull-right"><i class="icon icon-eye-close icon-white"></i> Вернутся обратно</a>
				Внимание, Вы находитесь в режиме MagicEye! Будьте предельно аккуратны!
			</div></div></div></div>';
			echo $html;
		}
	}
	
	function rusDate() {
	// Перевод
	 $translate = array(
	 "am" => "дп",
	 "pm" => "пп",
	 "AM" => "ДП",
	 "PM" => "ПП",
	 "Monday" => "Понедельник",
	 "Mon" => "Пн",
	 "Tuesday" => "Вторник",
	 "Tue" => "Вт",
	 "Wednesday" => "Среда",
	 "Wed" => "Ср",
	 "Thursday" => "Четверг",
	 "Thu" => "Чт",
	 "Friday" => "Пятница",
	 "Fri" => "Пт",
	 "Saturday" => "Суббота",
	 "Sat" => "Сб",
	 "Sunday" => "Воскресенье",
	 "Sun" => "Вс",
	 "January" => "Январь",
	 "Jan" => "Янв",
	 "February" => "Февраль",
	 "Feb" => "Фев",
	 "March" => "Март",
	 "Mar" => "Мар",
	 "April" => "Апрель",
	 "Apr" => "Апр",
	 "May" => "Май",
	 "May" => "Май",
	 "June" => "Июнь",
	 "Jun" => "Июн",
	 "July" => "Июль",
	 "Jul" => "Июл",
	 "August" => "Август",
	 "Aug" => "Авг",
	 "September" => "Сентябрь",
	 "Sep" => "Сен",
	 "October" => "Октябрь",
	 "Oct" => "Окт",
	 "November" => "Ноябрь",
	 "Nov" => "Ноя",
	 "December" => "Декабрь",
	 "Dec" => "Дек",
	 "st" => "ое",
	 "nd" => "ое",
	 "rd" => "е",
	 "th" => "ое"
	 );
	 
	 // если передали дату, то переводим ее
	 if (func_num_args() > 1) {
	 $timestamp = func_get_arg(1);
	 return strtr(date(func_get_arg(0), $timestamp), $translate);
	 } else {
	 	
	 // иначе текущую дату
	 return strtr(date(func_get_arg(0)), $translate);
	 }
	 }
?>