<?php
	function moneyStatus($id) {
		$moneyStates = array(
			0 => array (
				'title' => 'В ожидании',
				'class' => 'danger'
			),
			1 => array (
				'title' => 'В обработке',
				'class' => 'warning'
			),
			2 => array (
				'title' => 'Подтвержден',
				'class' => 'success'
			),
			3 => array (
				'title' => 'Отменен',
				'class' => 'important'
			),
		);
		
		return $moneyStates[$id];
	}
	
	function moneySum($sum) {
		$colorClass="text-success";
		$sumPrefix = "+";
		if ($sum<0) {
			$colorClass="text-error";
			$sumPrefix = "-";
		}
		
		if ($sum==0) {
			$sumPrefix = "";
		}
		
		$sum = abs (round($sum,2));
		$html = "<span class=\"{$colorClass}\">{$sumPrefix}{$sum} руб.</span>";
		return $html;
	}
	
	function paymentType($id) {
		$id = ($id=='')?$id=0:$id;
		$paymentTypes = array (
			0 => 'Неизвестная система',
			1 => 'Робокасса'
		);
		
		return $paymentTypes[$id];
	}
?>