<?php
	function projectStatus($id) {
		$states = array(
			0 => array (
				'title' => 'Не определено',
				'class' => 'danger'
			),
			1 => array (
				'title' => 'В работе',
				'class' => 'success'
			),
			3 => array (
				'title' => 'Заморожен на старте из-за нехватки средств',
				'class' => 'error'
			),
			5 => array (
				'title' => 'Удален пользователем',
				'class' => 'danger'
			),
			10 => array (
				'title' => 'Заморожен из-за нехватки средств',
				'class' => 'error'
			),
			15 => array (
				'title' => 'Откреплен от биржи',
				'class' => 'error'
			)
			
		);
		
		return json_decode(json_encode($states[$id]), FALSE);
	}
	
	function renderStatusArray($array) {
		$html = '<ul>';
		foreach ($array as $key=>$value) {
			$projectStatus = projectStatus($key);
			$html.= sprintf('<li><span class="label label-%s">%s: %d</span></li>',$projectStatus->class,$projectStatus->title, $value);
		}
		$html .= '</ul>';
		return $html;
	}
?>