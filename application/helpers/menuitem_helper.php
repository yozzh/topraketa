<?php
class menuItem {
	public $title;
	public $href;
	public $icon;
	public $class;
	public $active = false;
	public $childs = array();
	
	function __construct($title,$href='',$icon='', $class='')
    {
    	 $this->title = $title;
		 $this->href = $href;
		 $this->icon = $icon;
		 $this->class = $class;
    }
	
	public function addChild ($title,$href='',$icon='', $class='') {
		$tmpItem = new menuItem($title,$href,$icon, $class);
		$this->childs[]=$tmpItem;
	}
	
	public function setActive($href) {
		if ($this->href == $href) {
			$this->active = true;
		} else {
			foreach ($this->childs as $child) {
				if ($child->href == $href) {
					$child->active = true;
				}							
			}	
		}
		
	}
	
	public function generate() {
		$html="<ul class=\"nav {$this->title}  pull-right\">";
		foreach ($this->childs as $child) {
			$tmpItem = ($child->active)?'<li class="active">':'<li>';
			
			$link = ($this->href=='')?$child->href:"{$this->href}/{$child->href}";
			$tmpItem .= "<a href=\"{$link}\" class=\"{$child->class}\"><i class=\"{$child->icon} icon-white\"></i> {$child->title}</a>";
			$tmpItem .= '</li>';
			$html.=$tmpItem;
		}
		$html.="</ul>";
		return $html;
	}
	
	public function getActive() {
		foreach ($this->childs as $child) {
			if ($child->active) {
				return $child;
			}							
		}
		
		return false;
	}
    
}
?>
