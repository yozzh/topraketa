<?php
$daysPrefix = ($daysLeft<=5)?'Всего':'Еще';
$firstName = ($user->first_name == $user->email)?"<span class=\"cRed\">Не заполнено!</span>":$user->first_name;
$daysMessage = ($daysLeft==="Не ограничено")?'Надолго':"$daysPrefix на $daysLeft ";
$daysMessage .=proceedTextual($daysLeft, "дней", "день", "дня");
?>
<div class="informerButtons">
	<a class="icon_cabinet" href="/settings">Мой профиль</a>
	<a class="icon_registration" href="/main/logout">Выход</a>
</div>

<div class="moduleHeader">
	Личный кабинет
</div>

<div class="informerBlock">
	<div class="inputColumns">
		<div class="inputColumn w50">
			<div class="inputWrapper">
				<dl>
					<dt>Добро пожаловать,</dt>
					<dd><?=$firstName?> (<?=$user->email?>)</dd>
					<dt>Ваш баланс</dt>
					<dd><?=round($currentSum,2)?> р. &nbsp;<a href="/money/add" class="payButton">Пополнить баланс</a></dd>
				</dl>
			</div>
		</div>
		<div class="inputColumn w50">
			<div class="inputWrapper">
				<dl>
					<dt>Ежедневный расход</dt>
					<dd><?=round($totalCost,2)?> р./день</dd>
					<dt>Средств хватит</dt>
					<?php if ($currentSum>0) { ?> 
						<dd class="<?=(is_int($daysLeft) && $daysLeft<=5)?'text-error':'text-success';?>"><?=$daysMessage?></dd>	
					<?php } else { ?>
						<dd class="cRed">Недостаточно средств</dd>
					<?php } ?>
					
				</dl>
			</div>
		</div>
	</div>
</div>
