<form action="/callback" class="callbackForm" method="POST" data-callback="ajaxCallback">
	<table>
		<tr>
			<td>
			<div class="inputWrapper control-group <?=(form_error('name')!='')?'error':'';?>">
				<input type="text" name="name" class="input-block-level" id="name" placeholder="Ваше имя" value="<?=set_value('name') ?>"/>
			</div></td>
			<td>
			<div class="inputWrapper control-group <?=(form_error('phone')!='')?'error':'';?>">
				<input type="text" name="phone" class="input-block-level" id="phone" placeholder="Контактный телефон" value="<?=set_value('phone') ?>"/>
			</div></td>
		</tr>
		<tr>
			<td>
			<div class="inputWrapper control-group <?=(form_error('email')!='')?'error':'';?>">
				<input type="email" name="email" class="input-block-level" id="email" placeholder="Электронная почта" value="<?=set_value('email') ?>"/>
			</div></td>
			<td>
			<div class="inputWrapper control-group <?=(form_error('title')!='')?'error':'';?>">
				<input type="text" name="title" class="input-block-level" id="title" placeholder="Тема сообщения" value="<?=set_value('title') ?>"/>
			</div></td>
		</tr>
		<tr>
			<td colspan="2">
			<div class="inputWrapper control-group <?=(form_error('message')!='')?'error':'';?>">
				<textarea name="message" id="message" rows="8" class="input-block-level" placeholder="Сообщение"><?=set_value('message') ?></textarea>
			</div></td>
		</tr>
		<tr>
			<td>
				<div class="inputWrapper control-group captcha <?=(form_error('captcha')!='')?'error':'';?>">
					<?=$this->pages->createCaptcha()?>
					<input type="text" name="captcha" value="" id="captcha" placeholder="Код безопасности"/></div>
			</td>
			<td>
			<button class="button white ajaxSubmit">
				Отправить сообщение
			</button></td>
		</tr>
	</table>
</form>