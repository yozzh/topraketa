	<div class="keywordRotator">
		<div class="moduleHeader">
			Доставили в ТОP-10
		</div>
			<div class="keywords">
				<section class="keyword">
				  <div class="plane"></div>
				</section>
				<section class="keyword">
				  <div class="plane"></div>
				</section>
				<section class="keyword">
				  <div class="plane"></div>
				</section>
			</div>
	</div>
	<ul class="statisticItems">
		<li class="statUsers">
			<div class="content">
				<span class="number"><?=$users ?></span> <strong><?=proceedTextual($users, "пользователей", "пользователь", "пользователя");?></strong>
				
			</div>
		</li>
		<li class="statProjects">
			<div class="content">
				<span class="number"><?=$projects ?></span> <strong><?=proceedTextual($projects, "проектов", "проект", "проекта");?> в системе</strong>
				
			</div>
		</li>
		<li class="statKeywords">
			<div class="content">
				<span class="number"><?=$keywords ?></span> <strong><?=proceedTextual($keywords, "запросов", "запрос", "запроса");?> летят в ТОП</strong>
				
			</div>
		</li>
	</ul>

