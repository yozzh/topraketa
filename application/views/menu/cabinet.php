<nav>
	<ul class="twoLines">
		<li>
			<a href="/projects/add">Запустить проект</a>
		</li>
		<li>
			<a href="/projects">Мои проекты</a>
		</li>
		<li>
			<a href="/support">Служба поддержки</a>
		</li>
		<li>
			<a href="/money">Финансы</a>
		</li>
		<li>
			<a href="/settings#partnership">Партнерам</a>
		</li>
		<li>
      <a href="/help/index">Помощь</a>
    </li>
	</ul>
</nav>