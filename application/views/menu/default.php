<nav>
	<ul class="twoLines">
		<li>
			<a href="/#presentation">О сервисе</a>
		</li>
		<li>
			<a href="/#calc">Расчет проекта</a>
		</li>
		<li>
			<a href="/#news">Новости</a>
		</li>
		<li>
			<a href="/#pay">Условия</a>
		</li>
		<li>
			<a href="/#callback">Контакты</a>
		</li>
		<li>
      <a href="/help/index">Помощь</a>
    </li>
	</ul>
</nav>