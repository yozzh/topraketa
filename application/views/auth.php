<div class="informerButtons">
	<a class="icon_registration" href="/main/registration">Регистрация</a>
</div>

<div class="moduleHeader">
	Вход в личный кабинет
</div>

<div class="loginForm">
	<form action="/main/login" method="post" data-callback="ajaxLogin">
	<div class="buttonColumn">
		<button class="button blue ajaxSubmit" tabindex="3">
			Войти в кабинет
		</button>
	</div>
	<div class="formWrapper">
		
			<div class="inputColumns">
				<div class="inputColumn w50">
					<div class="inputWrapper">
						<div class="control-group <?=(isset($error))?'error':'';?>">
							<label for="email">Электронная почта</label>
							<input type="text" name="login_email" value="<?=(isset($email))?$email:'';?>" id="email" class="input-block-level" tabindex="1"/>
						</div>
						<label for="remember" class="checkbox">
							<input type="checkbox" name="remember" value="" id="remember"/>
							Запомнить меня</label>
					</div>
				</div>
				<div class="inputColumn w50">
					<div class="inputWrapper">
						<div class="control-group <?=(isset($error))?'error':'';?>">
							<label for="password">Пароль</label>
							<input type="password" name="login_password" value="" id="password" class="input-block-level" tabindex="2"/>
						</div>
						<a href="/main/forgot">Забыли пароль?</a>
					</div>
				</div>
			</div>		
	</div>
	</form>
</div>