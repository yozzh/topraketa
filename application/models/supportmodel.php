<?php
class SupportModel extends CI_Model {

	private $user;

	function __construct() {
		parent::__construct();
		$this -> user = $this -> ion_auth -> user() -> row();
	}
	
	public function tickets() {
		$sql = '
			SELECT 
			     support_tickets.*, users.first_name as userName, users.email as userEmail,
				  (SELECT COUNT(*) FROM support_messages WHERE support_messages.pid=support_tickets.id) AS messagesCount,
				  @lastMessage:=(SELECT MAX(support_messages.cdate) FROM support_messages WHERE support_messages.pid=support_tickets.id) AS lastMessage,
          @lastCheck:=support_visits.last_check as lastCheck,
          (unix_timestamp(@lastMessage)-unix_timestamp(@lastCheck))AS unreadDiffence
			FROM support_tickets
			LEFT JOIN 
			  users ON support_tickets.user_id = users.id
			LEFT JOIN
			  support_visits ON support_visits.ticket_id = support_tickets.id
			%s
			GROUP BY support_tickets.id
			ORDER BY unreadDiffence DESC
		';
		
		$group = array('admin', 'ticket_moderator', 'ticket_admin');
		if (!$this->ion_auth->in_group($group)) {
			$where = " WHERE support_tickets.user_id = ".$this->user->id;
		} else {
			$where = '';
		}
		$sql = sprintf($sql,$where);
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	public function ticket($id) {
		$sql = '
			SELECT support_tickets.*, users.username as userName, users.email as userEmail,
				(SELECT COUNT(*) FROM support_messages WHERE support_messages.pid=support_tickets.id) AS messagesCount
			FROM support_tickets
			LEFT JOIN users ON support_tickets.user_id = users.id
			WHERE support_tickets.id = '.$id.'
		';
		$query = $this->db->query($sql);
		$ticket = $query->row();
		
		$this->db->select('support_messages.*, users.first_name as userName, users.email as userEmail, MAX(users_groups.group_id) as userGroup');
		$this->db->where('pid',$id);
		$this->db->order_by('cdate','asc');
    $this->db->group_by('support_messages.id');
		$this->db->join('users','support_messages.user_id = users.id');
    $this->db->join('users_groups','users_groups.user_id = users.id');
		$query = $this->db->get('support_messages');
		$ticket->messages = $query->result();
		return $ticket;
	}
	
	public function addTicket($data) {
		$data['cdate'] = date("Y-m-d H:i:s",time());
		$data['user_id'] = $this->user->id;
		$moderators = $this->ion_auth->users(3)->result();
		
		$this->db->insert('support_tickets',$data);
		$pid = $this->db->insert_id();
		
		$message['user']=$this->user;
		$message['ticketId']=$pid;
		$message['title']=$data['title'];
		$message['description']=$data['description'];
		
		
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$body = $this->template->build('modules/support/email/new_ticket',$message, true);
		
		foreach ($moderators as $moderator) {
			$this->subscribemodel->send($moderator->email,'Служба поддержки - Новый тикет', $body);	
		}
		$this->addMessage($pid,array('text'=>$data['description']));
	}

	public function addMessage($pid, $data) {
		$data['pid'] = $pid;
		$data['user_id'] = $this->user->id;
		$data['cdate'] = date("Y-m-d H:i:s",time());
		$this->db->insert('support_messages',$data);
    
    $ticket = $this -> support -> ticket($pid);
    
    $message['ticketId']=$pid;
    $message['title']=$ticket->title;
    $message['description']=$data['text'];
    $users = $this->getTicketUsers($pid);
    
    $body = $this->template->build('modules/support/email/reply',$message, true);
    foreach ($users as $user) {
      $this->subscribemodel->send($user->email,$ticket->title.' - Новое сообщение в тикете', $body);
    }
	}
  
  /**
   * Return all users from ticket
   *
   * @return Users array
   * @author  
   */
  function getTicketUsers($ticketId) {
    $this->db->select('users.id as id, users.email as email');
    $this->db->where('pid',$ticketId);
    $this->db->join('users','support_messages.user_id = users.id');
    $this->db->group_by('user_id');
    $query = $this->db->get('support_messages');
    $users = $query->result();
    return $users;
  }
  
  /**
   * Returns count of user's unread tickets
   *
   * @return int
   * @author  
   */
  function unreadTickets($userId) {
    $count = 0;

    $sql = "SELECT support_tickets.id, support_messages.user_id, support_visits.last_check as last_check FROM support_tickets
            INNER JOIN support_messages ON support_messages.pid = support_tickets.id
            INNER JOIN support_visits ON support_visits.ticket_id = support_tickets.id
            WHERE support_messages.user_id = {$userId}
            GROUP BY support_tickets.id";

    $query = $this->db->query($sql);
    $tickets = $query->result();
    
    foreach ($tickets as $ticket) {
      $sql = "SELECT MAX(support_messages.cdate) as cdate FROM support_messages WHERE pid = {$ticket->id}";
      $q = $this->db->query($sql);
      $date = $q->row();
      
      $count+=($date->cdate > $ticket->last_check)?1:0;
    }

    return $count;
  }

	public function updateTicketStatus($id,$status) {
		$data = array(
			'status'=> $status
		);
		$this->db->where('id',$id);
		$this->db->update('support_tickets',$data);
	}
	
	public function deleteTicket($id) {
		$this->db->where('id',$id);
		$this->db->delete('support_tickets');
		
		$this->db->where('pid',$id);
		$this->db->delete('support_messages');
	}

	public function groups() {
		
		$query = $this->db->get('support_groups');
		return $query->result();
	}
	
	public function group($id) {
		$this->db->where('id',$id);
		$query = $this->db->get('support_groups');
		return $query->row();
	}
	
	public function addGroup($data) {
		$this->db->insert('support_groups',$data);
	}
	
	public function deleteGroup($id) {
		$this->db->where('id',$id);
		$this->db->delete('support_groups');
	}

	private function getGroupClass($groupName) {
		$groupClass = '';
		switch ($groupName) {
			case 'admin' :
				$groupClass = 'label-important';
				break;
			case 'ticket_admin' :
				$groupClass = 'label-warning';
				break;
			case 'members' :
				$groupClass = 'label-info';
				break;
			case 'ticket_moderator' :
				$groupClass = 'label-success';
				break;
		}
		return $groupClass;
	}

	public function updateGroups($userId, $groups) {
		$this -> db -> where('user_id', $userId);
		$this -> db -> delete('users_groups');

		foreach ($groups as $group) {
			$data = array('user_id' => $userId, 'group_id' => $group);

			$this -> db -> insert('users_groups', $data);
		}
	}

	public function lock($userId, $active = 0) {
		$data = array ('active'=>$active);
		$this -> db -> where('id', $userId);
		$this -> db -> update('users',$data);
	}
  
  public function updateVisit($ticketId, $userId) {
    $sql = "INSERT INTO support_visits(ticket_id, user_id, last_check)
              VALUES ({$ticketId}, {$userId}, NOW())
              ON DUPLICATE KEY
              UPDATE last_check = NOW();";
              
    $query = $this->db->query($sql);
    return true;
  }
  
  /**
   * Function that update all users visits for correct informer work
   *
   * @return void
   * @author  
   */
  function setVisitsBase() {
    $sql = "SELECT user_id FROM support_messages GROUP BY user_id";
    $query = $this->db->query($sql);
    $users = $query->result();
    
    foreach ($users as $index => $user) {
      $sql = "SELECT support_tickets.id, support_messages.user_id FROM support_tickets
              INNER JOIN support_messages ON support_messages.pid = support_tickets.id
              WHERE support_messages.user_id = {$user->user_id}
              GROUP BY support_tickets.id";  
      $query = $this->db->query($sql);
      $tickets = $query->result();
      
      foreach ($tickets as $ticket) {
        $sql = "SELECT MAX(support_messages.cdate) as cdate FROM support_messages WHERE pid = {$ticket->id}";
        $q = $this->db->query($sql);
        $date = $q->row();
        
        $data = array(
          'ticket_id'=>$ticket->id,
          'user_id'=>$user->user_id,
          'last_check'=>$date->cdate
        );
        
        $this->db->insert('support_visits',$data);
      }
    }
    
    
  }

}
?>