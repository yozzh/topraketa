<?php
class Menu extends CI_Model {
	public $menuItems;
	
	function __construct()
    {
        parent::__construct();
		
		$this->load->spark('template/1.9.0');
		$this->load->spark('ion_auth/2.4.0');
		
		$this->loadInformer();
		$this->loadStatistic();
		$this->loadMenu();
		
		$this->menuItems = new menuItem('main-menu');
		$this->menuItems->addChild('Кабинет', 'main', 'general');
		$this->menuItems->addChild('Проекты', 'projects', 'projects');
		$this->menuItems->addChild('Заказы', 'orders', 'orders');
		$this->menuItems->addChild('Поддержка', 'support', 'support');
		$this->menuItems->addChild('Партнерам', 'partnership', 'partnership');
		$this->menuItems->addChild('Правила', 'rules', 'rules');
		
		
		if ($this->ion_auth->is_admin()) {
			$this->menuItems->addChild('Админ', 'admin', 'general');	
		}
    }
	
	public function setActive($href) {
		$this->menuItems->setActive($href);
	}
	
	private function loadInformer()
	{		
		if ($this->ion_auth->logged_in()) {
			$this->load->model('moneyModel','money');
			$this->load->model('projectsmodel','projects');
      $this->load->model('supportmodel','support');
			$currentSum = $this->money->calcUserMoney();
			$totalCost = $this->projects->callProjectCostByUser();
			$daysLeft = ($totalCost==0)?'Не ограничено':floor($currentSum/$totalCost);
			$data = array(
				'user'=>$this->ion_auth->user()->row(),
				'currentSum'=>$currentSum,
				'totalCost'=>$totalCost,
				'daysLeft'=>(int)$daysLeft
			);
			
			$this->template->inject_partial('informer',$this->load->view('informer',$data,true));
      
      $data['unread'] = $this->support->unreadTickets($data['user']->id);
      $this->template->inject_partial('unread',$this->load->view('unread',$data,true));
		} else {
			$this->template->inject_partial('informer',$this->load->view('auth',array(),true));
		}
		
		//$this->template->set_partial('informer','partials/informer');
	}
	
	private function loadStatistic() {
		$data['users'] = $this->settingsmodel->get('users');
		$data['projects'] = $this->settingsmodel->get('projects');
		$data['keywords'] = $this->settingsmodel->get('keywords');
		$this->template->inject_partial('statistic',$this->load->view('statistic',$data,true));
	}
	
	private function loadMenu()
	{		
		if ($this->ion_auth->logged_in()) {
			$this->template->inject_partial('mainMenu',$this->load->view('menu/cabinet',array(),true));	
		} else {
			$this->template->inject_partial('mainMenu',$this->load->view('menu/default',array(),true));
		}
	}
}

?>