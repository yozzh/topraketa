<?php
class SettingsModel extends CI_Model {
	private $settings = array();

	function __construct() {
		parent::__construct();
		$query = $this->db->get('settings');
		foreach ($query->result() as $set) {
			$this->settings[$set->alias] = $set->value;
		}
	}

	public function get($setting) {
		return $this->settings[$setting];
	}

	public function changePassword($userid, $newpassword) {

	}

}
?>