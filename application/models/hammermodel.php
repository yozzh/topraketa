<?php
class HammerModel extends CI_Model {
	public $menuItems;
	private $login = 'raketa01';
	private $password;
	private $connections = array();
	private $cookies;
	
	function __construct()
    {
    	parent::__construct();
		
		$this -> load -> model('regionsmodel', 'regions');
		$this -> load -> model('benefitmodel', 'benefit');
    	require_once(APPPATH.'libraries/nusoap/nusoap'.EXT);
		$this->password = '57C31470B8BB5CA1258D349E95B1F2A2';
		$this->initConnections();
		$this->connections['LOGIN'] = new nusoap_client(HammerUrl::$START, true);
		$this->connections['LOGIN']->setUseCurl(1); 
		$result = $this->connections['LOGIN']->call('LogInMd5Password', array('Login' => $this->login, 'MD5Password' => $this->password));
		$this->cookies = $this->connections['LOGIN']->getCookies();
		$this->initCookies();
    }	
	
	private function initConnections() {
		$connections = get_class_vars('HammerUrl');
		foreach ($connections as $alias => $url) {
			$this->connections[$alias] = new nusoap_client($url, true);
			$this->connections[$alias]->decode_utf8 = 0; 
			$this->connections[$alias]->soap_defencoding  = 'UTF-8';
		}
	}
	
	private function initCookies() {
		foreach ($this->connections as &$connection) {
			$this->prepareCall($connection);
		}
	}
	
	private function prepareCall(&$call) {
		foreach ($this->cookies as $cookie) {
			$call->setCookie($cookie['name'],$cookie['value']);
		}
	}
	
	public function getBalance($currency = 1) {
		$balance = $this->connections['LOGIN']->call('GetBalance', array('BalanceType'=>$currency));
		return $balance['GetBalanceResult']; 
	}
	
	public function getBudget($query) {
		$response = $this->connections['DEEP']->call('GetBudget', array('query'=>$query));
		return $response['GetBudgetResult'];
	}
	
	public function getRegionBudget($query, $regionId) {
		
		$response = $this->connections['DEEP']->call('GetRegionBudget', array('query'=>$query,'regionId'=>$regionId));
		$budget = (float)$response['GetRegionBudgetResult'];
		//echo 'fsh: '.$budget;		
		$budget = $this->calcBudget($budget);
		return $budget;
	}
	
	public function calcBudget($budget, $month = 1) {
		
		$costPercent = $this->settingsmodel->get('costMultiplier');
		if ($budget <= $this->settingsmodel->get('minValueCase') && $budget != 0)  {
			$budget = $this->settingsmodel->get('minValue');
		} else {
			//$budget = $budget*((100+$costPercent)/100);
			$budget = $this->benefit->calcKeywordBudget($budget,true, true);	
		}
		$budget = round($budget/$month,2);
		return $budget;
	}
	
	public function calcLinkBudget($budget, $month = 1) {
		
		$costPercent = $this->settingsmodel->get('costMultiplier');
		$budget = $budget*((100+$costPercent)/100);
		$budget = round($budget/$month,2);
		return $budget;
	}
	
	public function getProjects($ids=false) {
		$response = $this->connections['PROJECTS']->call('GetAllProjects',array());
		$allProjects = $response['GetAllProjectsResult']['Project'];
		$returnProjects = array();
		
		if ($ids == false && !is_array($ids)) {
			$returnProjects = $allProjects;
		} else {
			if (is_array($allProjects)) { 
				foreach ($allProjects as $i => $project) {
					if (in_array($project['id'],$ids)) {
						$project['ygeo'] = $this->regions->getYaRegionById($project['ygeo']);
						$returnProjects[] = $project;
					}
				}
			}
		}
		return $returnProjects;
	}
	
	public function addProject($domain,$dotexts=true) {
		$response = $this->connections['PROJECTS']->call('AddProject',array('domain'=>$domain,'dotexts'=>$dotexts));
		if (!$response) {
			$this->debug($this->connections['PROJECTS']);
			return false;
		} else {
			return $response['newprjid'];
		}
	}
	
	public function deleteProject($id) {
		$response = $this->connections['PROJECTS']->call('DeleteProject',array('id'=>$id));
		if (!$response) {
			$this->debug($this->connections['PROJECTS']);
			return false;
		} else {
			return true;
		}
	}
	
	public function archiveProject($id) {
		$response = $this->connections['PROJECTS']->call('ArchiveProject',array('id'=>$id));
		if (!$response) {
			$this->debug($this->connections['PROJECTS']);
			return false;
		} else {
			return true;
		}
	}
	
	public function getKeys($projectId) {
		$response = $this->connections['KEYS']->call('GetAllKeysByProject', array(
			'prjid'=>$projectId
		));
		if (!$response || $response['GetAllKeysByProjectResult']=="") {
			//$this->debug($this->connections['KEYS']);
			return false;
		} else {

		if (isset($response['GetAllKeysByProjectResult']['Key'][0])) {
			foreach ($response['GetAllKeysByProjectResult']['Key'] as $link) {
				$returnKeys[] = $link;
			}
		} else {
			$returnKeys[] = $response['GetAllKeysByProjectResult']['Key'];
		}
			return $returnKeys;
		}
	}
	
	public function getKey($id) {
		$response = $this->connections['KEYS']->call('GetKey', array(
			'id'=>$id
		));
		if (!$response) {
			$this->debug($this->connections['KEYS']);
			return false;
		} else {
			return $response['GetKeyResult'];
		}
	}
	
	public function updateKey($keyId, $url, $budget, $yregion, $gregion) {
		$response = $this->connections['KEYS']->call('ChangeUrl', array(
			'id'=>$keyId,
			'newurl'=>$url
		));
		
		$response = $this->connections['KEYS']->call('UpdateKeyYandexRegion', array(
			'id'=>$keyId,
			'yandexid'=>$yregion
		));
		
		$response = $this->connections['KEYS']->call('UpdateKeyGoogleRegion', array(
			'id'=>$keyId,
			'googleid'=>$gregion,
			'googlelanguage'=>0
		));
		
		
		$response = $this->connections['KEYS']->call('UpdateKeyLinkBudget', array(
			'id'=>$keyId,
			'linkbudget'=>$budget
		));
		
	}
	
	public function addKeyword($projectId, $text, $url, $budget, $yregion = 213, $gregion = 0, $speed = 'Auto') {
		$response = $this->connections['KEYS']->call('KeyAdd', array(
			'prjid'=>$projectId,
			'word'=>$text,
			'url'=>$url,
			'speed'=>$speed,
			'linkbudget'=>$budget
		));
		
		$keyId = (isset($response['keyid']))?$response['keyid']:-1;
		//var_dump($keyId, $region);
		
		if ($keyId!=-1) {
			$response2 = $this->connections['KEYS']->call('UpdateKeyYandexRegion', array(
				'id'=>$keyId,
				'yandexid'=>$yregion
			));
			
			$response2 = $this->connections['KEYS']->call('UpdateKeyGoogleRegion', array(
				'id'=>$keyId,
				'googleid'=>$gregion,
				'googlelanguage'=>0
			));

		} else {
			return $response;
		}
	}
	
	public function removeKeyword($id) {
		$response = $this->connections['KEYS']->call('DeleteKey', array(
			'id' => $id
		));
		//var_dump($response);
	}
  
  public function archiveKeyword($id) {
    $response = $this->connections['KEYS']->call('ArchiveKey', array(
      'id' => $id
    ));
    return $response;
  }
	
	public function getYaRegions() {
		$response = $this->connections['DICTIONARY']->call('YandexRegions',array());
		return $response['YandexRegionsResult']['YandexRegion'];
	}
	
	public function getGoogleRegions() {
		$response = $this->connections['DICTIONARY']->call('GoogleRegions',array());
		return $response['GoogleRegionsResult']['GoogleRegion'];
	}
	
	public function getLinks($projectId) {
		$response = $this->connections['LINKS']->call('GetLinksByProject', array(
			'prjid'=>$projectId
		));
		if (!$response || $response['GetLinksByProjectResult']=="") {
			//$this->debug($this->connections['KEYS']);
			return false;
		} else {
			$returnLinks = array();
			if (isset($response['GetLinksByProjectResult']['Link'][0])) {
				foreach ($response['GetLinksByProjectResult']['Link'] as $link) {
					//$link['cost'] = $this->calcBudget($link['cost'],30);
					$returnLinks[] = $link;
				}
			} else {
				$returnLinks[] = $response['GetLinksByProjectResult']['Link'];
			}
			return $returnLinks;
		}
	}
	
	public function deleteLink($id) {
		$response = $this->connections['LINKS']->call('DeleteLink', array(
			'id' => $id
		));
	}
	
	private function debug (&$client) {
		echo '<h2>Отладка</h2>'; 
		echo '<pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>'; 
	}
}
//$this->load->spark('ion_auth/2.4.0');
?>