<?php
class CallbackModel extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
		$this->load->library('email');
    }
	
	public function sendMessage($post) {
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$data['post']=$post;
		$data['message']='
		Получено сообщение через форму обратной связи!
		<ul>
			<li><strong>Имя:</strong> '.$post['name'].'</li>
			<li><strong>Телефон:</strong> '.$post['phone'].'</li>
			<li><strong>E-mail:</strong> '.$post['email'].'</li>
			<li><strong>Заголовок:</strong> '.$post['title'].'</li>
			<li><strong>Сообщение:</strong> '.$post['message'].'</li>
		</ul>
		';
		$body = $this->template->build('modules/callback/message',$data, true);
		$this->subscribemodel->send('d.m.smirnov@yandex.ru, lads8800@gmail.com, bukatov@bk.ru, info@top-raketa.ru','Сообщение с формы обратной связи', $body);
	}
	
	public function messageSuccess() {
		$this->template->set_theme('frontend');
		$this->template->set_layout('ajax');
		return $this->template->build('partials/message_success',array(),true);
	}
}

?>