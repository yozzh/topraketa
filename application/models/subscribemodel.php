<?php
class SubscribeModel extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
		$this->load->spark('template/1.9.0');
		$this->load->spark('ion_auth/2.4.0');
	}
	
	public function send($email, $title, $body) {
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		
		$this->email->from('noreply@top-raketa.ru', 'Top-Raketa.ru');
		$this->email->to($email); 
		$this->email->subject('Top-Raketa.ru - '.$title);
		
		$message = $this->letter($body, $title);
		$this->email->message($message);	
		
		$this->email->send();
	}
	
	public function letter($body, $title) {
		$data = array(
			'body' => $body,
			'title' => $title
		);
		return $this->load->view('letter',$data,true);
	}
}