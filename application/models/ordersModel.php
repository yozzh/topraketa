<?php
class OrdersModel extends CI_Model {
	private $user;
	
	function __construct()
    {
        parent::__construct();
		$this->user = $this->ion_auth->user()->row();
    }
	
	public function items($limit=0) {
		$this->db->select('orders.*, projects.title as projectTitle, projects.url as projectUrl, projects.user_id as uid, products.title as productTitle, users.first_name, users.email');
		$this->db->from('orders');
		$this->db->join('projects','orders.project_id = projects.id','left outer');
		$this->db->join('products','orders.product_id = products.id','left outer');
		$this->db->join('users','projects.user_id = users.id','left outer');
		if (!$this->ion_auth->is_admin()) {
			$this->db->where('projects.user_id',$this->user->id);
		}
		$this->db->order_by('orders.status ASC, orders.cdate DESC'); 
		if ($limit>0) {
			$this->db->limit($limit);
		} 
		$query = $this->db->get();
		
		return $query->result();
	}
	
	public function item($id)
	{
		$this->db->select('*');
		$this->db->from('orders');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$order = $query->result();
		if (count($order)) {
			return $order[0];	
		} else {
			return false;
		}
		
	}
	
	public function calcUserOrdersByStatus($status_id=-1)
	{
		$this->db->select('COUNT(*) as count');
		$this->db->join('projects','orders.project_id = projects.id','left outer');
		if ($status_id!=-1) {
			$this->db->where('status',$status_id);
		}
		$this->db->where('projects.user_id',$this->user->id);
		$query = $this->db->get('orders');
		$order = $query->result();
		if (count($order)) {
			return $order[0]->count;	
		} else {
			return false;
		}
	}
	
	public function save($post)
	{
		$data['project_id'] = $post['project'];
		$data['count'] = $post['count'];
		$order = $this->products->calcOrder($post['product'],$post['count']);
		$data['sum'] = $order['sum'];
		$data['comment'] = $post['comment'];
		$data['product_id'] = $post['product'];
		$data['cdate'] = date("Y-m-d H:i:s",time());
		$data['status'] = 0;
		
		$result = $this->db->insert('orders',$data);
	}
	
	public function update($post,$id) {
		$data['title'] = $post['title'];
		$data['url'] = $post['url'];
		$data['comment'] = $post['comment'];
		$this->db->where('id',$id);
		$result = $this->db->update('orders',$data);
	}
	
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('projects');
	}
	
	public function cancel($id)
	{
		$this->db->where('id',$id);
		$this->db->set('status',3);
		$this->db->update('orders');
	}
}

?>