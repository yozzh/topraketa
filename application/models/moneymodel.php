<?php
class MoneyModel extends CI_Model {
	
	private $user;
	
	function __construct()
    {
        parent::__construct();
		$this->user = $this->ion_auth->user()->row();
    }
	
	public function payments($limit=0) {
		$this->db->select('*');
		$this->db->from('payments');
		$this->db->where('user_id',$this->user->id);
		$this->db->order_by('payments.date DESC'); 
		if ($limit>0) {
			$this->db->limit($limit);
		} 
		$query = $this->db->get();
		
		return $query->result();
	}
	
	public function payment($id, $byUser = true)
	{
		$this->db->select('*');
		$this->db->from('payments');
		$this->db->where('id',$id);
		if ($byUser) {
			$this->db->where('user_id',$this->user->id);	
		}
		
		$query = $this->db->get();
		$payment = $query->result();
		if (count($payment)) {
			return $payment[0];	
		} else {
			return false;
		}
	}
	
	public function calcUserMoney($userId=false) {
		
		$userId = ($userId)?$userId:$this->user->id;
		$this->db->select('SUM(value) as sum');
		$this->db->where('user_id',$userId);
        $this->db->where('status',2);
		$query = $this->db->get('payments');
		$sumQ = $query->result();
		$sum = $sumQ[0]->sum;
		if ($sum!=NULL) {
			return $sum;	
		} else {
			return 0;
		}
	}
	
	public function save($post)
	{
		$data['project_id'] = $post['project'];
		$data['count'] = $post['count'];
		$order = $this->products->calcOrder($post['product'],$post['count']);
		$data['sum'] = $order['sum'];
		$data['comment'] = $post['comment'];
		$data['product_id'] = $post['product'];
		$data['cdate'] = date("Y-m-d H:i:s",time());
		$data['status'] = 0;
		
		$this->db->insert('orders',$data);
		return $this->db->insert_id();
	}
	
	public function update($post,$id) {
		$data['title'] = $post['title'];
		$data['url'] = $post['url'];
		$data['comment'] = $post['comment'];
		$this->db->where('id',$id);
		$result = $this->db->update('orders',$data);
	}
	
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('projects');
	}
	
	public function cancel($id)
	{
		$this->db->where('id',$id);
		$this->db->set('status',3);
		$this->db->update('orders');
	}
    
    // Пополнение баланса
    public function updateBalans($post)
    {
		$data['user_id'] = $post['user_id'];
		$data['value'] = $post['value'];
		$data['date'] = date("Y-m-d H:i:s",time());
		$data['status'] = 1; // перешел на кассу но пока не проплатил
		$data['comment'] = (isset($post['comment']))?$post['comment']:'';
		$data['type'] = (isset($post['type']))?$post['type']:'';
		$result = $this->db->insert('payments',$data); 
		return $this->db->insert_id(); 		
    }

	public function generateLink($orderId) {
		return 'http://top-raketa.ru/pay/'.$orderId;
	}

	// Пополнение баланса
	public function addExpense($data) {
		$result = $this->db->insert('payments',$data); 
		return $this->db->insert_id(); 	
	}
    
    // Изменение статуса  транзакции проплаты
    public function updatePaymentStatus($id)
    {
    	$this->db->where('id',$id);
		$this->db->where('status',1);
		$this->db->from('payments');
		$paymentCount = $this->db->count_all_results();
		
		if ($paymentCount == 0) return;
		
		$this->db->where('id',$id);
		$this->db->set('status',2);
		$this->db->update('payments');
		
		//Дополнительные действия по типу заказа
		$this->callAddins($id);      
    }
	
	// Увеличивает пополненную сумму на указанный процент
	public function setBonus($INV_ID,$percent)
	{
		$query = $this->db->query("SELECT * FROM `payments` WHERE id=$INV_ID");
		if ($query->num_rows() < 1)
		{
			return;
		}
		$data = $query->row_array();
		unset($data['id']);
		$data['value'] = round($data['value'] / 100 * $percent);
		$data['type'] = 'bonus'; // добавлен бонус
		$data['status'] = 2; 
		$result = $this->db->insert('payments',$data); 
        return $this->db->insert_id(); 
	}
	
	// Получает список сегодняшних проплат
	public function getCurdatePayments()
	{
		$query = $this->db->query("
			SELECT p.id,p.date, p.value, u.first_name, u.email
			FROM payments p
			LEFT JOIN users u ON p.user_id = u.id
			WHERE p.value >=0
			AND p.status =2
			AND ISNULL( p.type ) 
			AND p.date >= CURDATE( )
			ORDER BY p.date
		");
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
	}
	
	// Получает сумму сегодняшних проплат
	public function getCurdatePaymentsSum()
	{
		$query = $this->db->query("
			SELECT SUM(value) total
			FROM payments 
			WHERE value >=0
			AND status =2
			AND ISNULL( type ) 
			AND date >= CURDATE( ) 
		");
		if ($query->num_rows() > 0)
		{
			return $query->row()->total;
		}
	}
	
	public function callAddins($orderId) {
		$payment = $this->payment($orderId, false);
		$this->load->model('usersmodel','users');
		switch ($payment->type) {
			case 'api':
				/*$bonus = 0;
				
				$data = array(
					'user_id'=>$payment->user_id,
					'value'=>$bonus,
					'date' => date("Y-m-d H:i:s",time()),
					'status' => 2,
					'target_id' => '',
					'type' => 'bonus',
					'comment' => 'Бонусное начисление при регистрации'
				);
				
				//$result = $this->money->addExpense($data);*/

				$this->users->activate($payment->user_id, $payment->value, $bonus);
				
				
				
				break;
			
			default:
				
				break;
		}
	}
}

?>