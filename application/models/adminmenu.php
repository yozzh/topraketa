<?php
class AdminMenu extends CI_Model {
	public $menuItems;
	
	function __construct()
    {
        parent::__construct();
		
		$this->load->spark('template/1.9.0');
		$this->load->spark('ion_auth/2.4.0');
		
		$this->loadInformer();
		$this->loadMenu();
		
		$this->menuItems = new menuItem('main-menu');
		$this->menuItems->addChild('Кабинет', '/admin/panel', 'general');
		$this->menuItems->addChild('Проекты', '/admin/projects', 'projects');
		$this->menuItems->addChild('Финансы', '/admin/finance', 'orders');
		$this->menuItems->addChild('Раздел помощь', '/admin/help', 'support');
		$this->menuItems->addChild('Партнерка', 'partnership', 'partnership');
		$this->menuItems->addChild('Настройки', 'admin/settings', 'rules');
		
    }
	
	public function setActive($href) {
		$this->menuItems->setActive($href);
	}
	
	private function loadInformer()
	{		
		if ($this->ion_auth->logged_in()) {
			$this->load->model('moneyModel','money');
			$this->load->model('projectsmodel','projects');
			$currentSum = $this->money->calcUserMoney();
			$totalCost = $this->projects->callProjectCostByUser();
			$daysLeft = ($totalCost==0)?'Не ограничено':floor($currentSum/$totalCost);
			$data = array(
				'user'=>$this->ion_auth->user()->row(),
				'currentSum'=>$currentSum,
				'totalCost'=>$totalCost,
				'daysLeft'=>(int)$daysLeft
			);
			
			$this->template->inject_partial('informer',$this->load->view('informer',$data,true));	
		} else {
			$this->template->inject_partial('informer',$this->load->view('auth',array(),true));
		}
		
		//$this->template->set_partial('informer','partials/informer');
	}
	
	private function loadMenu()
	{		
		if ($this->ion_auth->logged_in()) {
			$this->template->inject_partial('mainMenu',$this->load->view('menu/cabinet',array(),true));	
		} else {
			$this->template->inject_partial('mainMenu',$this->load->view('menu/default',array(),true));
		}
	}
	
	public function getForm($group,$callback) {
		$this->db->where('group',$group);
		$query = $this->db->get('settings');
		$return = form_open($callback);
		foreach ($query->result() as $set) {
			$fieldPath = $set->group.'_'.$set->alias;
			$return.=form_label($set->desc,$fieldPath);
			$return.=form_input($fieldPath,$set->value,"id=\"$fieldPath\"");
		}
		$return.= '<hr/>';
		$return.= form_submit('submit','Сохранить','class="btn btn-success"');
		$return.= form_close();
		return $return;
	}
	
	public function saveForm($post) {
		foreach ($post as $key => $value) {
			$fieldPath = explode('_',$key);
			if (count($fieldPath)<=1) continue;
			
			$group = $fieldPath[0];
			$alias = $fieldPath[1];
			$this->db->where('group',$group);
			$this->db->where('alias',$alias);
			$this->db->update('settings',array('value'=>$value));
		}
	}
}

?>