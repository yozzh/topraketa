<?php
class ProjectsModel extends CI_Model {
	
	private $user;
	
	function __construct()
    {
        parent::__construct();
		$this->user = $this->ion_auth->user()->row();	
		$this -> load -> model('benefitmodel', 'benefit');
    }
	
	public function items($user=true, $limit=0, $all = false) {
		
		$this->db->select('*');
		$this->db->from('projects');
		if (!$all) {
			if ($user===true) {
				$this->db->where('user_id',$this->user->id);	
			} else {
				$this->db->where('user_id',$user);
			}
		}
		$this->db->where('status',1);
		$this->db->order_by('cdate DESC');
		if ($limit>0) {
			$this->db->limit($limit);
		} 
		
		$query = $this->db->get();
		$result = $query->result();
		return $query->result();
	}
	
	public function frozen($userId = false, $all = false) {
		
		$userId = ($userId)?$userId:$this->user->id;
		
		if (!$all) {
			$where = ' AND projects.user_id = '.$userId;
			$statuses = '0, 3, 10, 15';
		} else {
			$where = '';
			$statuses = '0, 5, 10, 15';
		}
		
		$sql = 'SELECT projects.*,
					SUM(projects__tmp_keywords.budget) as budget,
					users.email as email
				FROM projects 
				LEFT JOIN projects__tmp_keywords ON projects.id = projects__tmp_keywords.project_id
				LEFT JOIN users ON projects.user_id = users.id
				WHERE projects.status IN ('.$statuses.') '.$where.'
					GROUP BY projects.id ORDER BY projects.status DESC, projects.cdate DESC';
		$query = $this->db->query($sql);
		$result = $query->result();
		if (count($result)==0) {
			return false;
		} else {
			return $result;
		}
	}
	
	public function item($id, $frozen = false, $byHm = FALSE)
	{
		$field = ($byHm)?'hm_project_id':'id';
		
		$this->db->select('*');
		$this->db->from('projects');
		$this->db->where($field,$id);
		$query = $this->db->get();
		$project = $query->row();
		
		
		if ($project) {
			if ($frozen) {
				$this->db->select('*');
				$this->db->from('projects__tmp_keywords');
				$this->db->where('project_id',$id);
				$query = $this->db->get();
				$project->keywords = $query->result();
				
				foreach ($project->keywords as &$keyword) {
					$keyword->budget = $this->benefit->calcKeywordBudget($keyword->budget, true);
				}
			}
			return $project;	
		} else {
			return false;
		}
	}
	
	public function checkUsersProject($projectId, $frozen = false) {
		if (!$frozen) {
			$this->db->where('hm_project_id',$projectId);	
		} else {
			$this->db->where('id',$projectId);
		}
		
		$this->db->where('user_id',$this->user->id);
		$this->db->from('projects');
		$result = $this->db->count_all_results();
		return ($result>0)?true:false;
	}
	
	public function getKeys($id) {
		$keywords = $this -> hammermodel -> getKeys($id);
		$keyCost = $this->benefit->getKeyCost($id);
		
		$keys = array();
		
		if (is_array($keywords)) {
			foreach ($keywords as $keyword) {
				$keyword['linkbudget'] = $this->benefit->calcKeywordBudget($keyword['linkbudget'],true);
				$keyword['linkcost'] = (isset($keyCost[$keyword['id']]))?$keyCost[$keyword['id']]:0;
				$keys[] = $keyword;
			}	
		}
		
		return $keys;
	}
	
	public function save($post)
	{
		$data['url'] = $post['domain'];
		$data['hm_project_id']  = $post['hm_project_id'];
		$data['user_id'] = (isset($post['user_id']))?$post['user_id']:$this->user->id;
		$data['cdate'] = date("Y-m-d H:i:s",time());
		$data['status'] = (isset($post['status']))?$post['status']:1;
		
		$this->db->insert('projects',$data);
		$result = $this->db->insert_id();
		return $result;
	}
	
	public function getProjectCost($projectId) {
		$this->load->model('linksmodel','links');
		$cost = $this->links->getCostByProject($projectId);
		return $cost;
	}
	
	public function getProjectsCount($user=false) {
		//$this->db->where('status','1');
		if ($user) {
			$this->db->where('user_id',$user);	
		}
		$query = $this->db->get('projects');
		$return=array();
		foreach ($query->result() as $project) {
			$return[$project->status]=(isset($return[$project->status]))?$return[$project->status]+1:1;
		}
		return $return;
	}
	
	public function getYaRegions() {
		$filePath = APPPATH .'/data/region.csv';
		$regionsCsv = file_get_contents($filePath);
		$regions = explode("\n",$regionsCsv);
		$resultArray = array();
		foreach ($regions as $region) {
			if ($region!="") {
				$region = explode(";",$region);
				$tmpRegion['id']=$region[0];
				$tmpRegion['label']=$region[1];
				$resultArray[]=$tmpRegion;	
			}
		}
		return $resultArray;
	}
	
	public function update($post,$id) {
		$data['title'] = $post['title'];
		$data['url'] = $post['url'];
		$data['comment'] = $post['comment'];
		$this->db->where('id',$id);
		$result = $this->db->update('projects',$data);
	}
	
	public function startFrozenByUser($userId) {
		$projects = $this->frozen($userId);
		foreach ($projects as $project) {
			$this->startFrozen($project->id);
		}
	}
	
	public function startFrozen($id) {
		$project = $this->projects->item($id, true);
		$userBalance = $this -> money -> calcUserMoney( $this->user->id);
		if ($userBalance > 0) {
			
			switch ($project->status) {
				case '3':
				case '15':
					$hmId = $this -> hammermodel -> addProject($project->url);
					foreach ($project->keywords as $keyword) {
						$newBudget = $this->benefit->calcKeywordBudget($keyword->budget);
						$this -> hammermodel -> addKeyword($hmId, $keyword -> text, $keyword -> url, $newBudget, $keyword -> yregion, $keyword -> gregion);
					}		
					$this->db->where('id', $id);
					$this->db->update('projects',array('status'=>1,'hm_project_id'=>$hmId));
				
					$this->db->where('project_id',$id);
					$this->db->delete('projects__tmp_keywords');
					break;
				case '5':
					break;
				case '10':
					$this->db->where('id', $id);
					$this->db->update('projects',array('status'=>1));
				
					$this->db->where('project_id',$id);
					$this->db->delete('projects__tmp_keywords');
					break;
				default:
					
					break;
			}
			return true;	
		} else {
			return false;
		}
	}
	
	public function callProjectCostByUser($user=true) {
		$data['items'] = ($user===true)?$this -> items():$this -> items($user);
		$ids = array();
		foreach ($data['items'] as $item) {
			$ids[] = $item -> hm_project_id;
		}
		$projects = $this -> hammermodel -> getProjects($ids);
		
		$totalCost = 0;
		foreach ($projects as &$project) {
			$totalCost += round($this->getProjectCost($project['id'])/30,2);
		}
		return $totalCost;
	}
	
	public function addTempKeyword($projectId, $text, $url, $budget, $yregion = 213, $gregion = 0, $speed = 'Auto') {
		$data['url'] = $url;
		$data['text'] = $text;
		$data['project_id'] = $projectId;
		$data['budget'] = $budget;
		$data['yregion'] = $yregion;
		$data['gregion'] = $gregion;
		
		$this->db->insert('projects__tmp_keywords',$data);
	}
	
	public function freeze($id, $status = 0) {
		$ids = array($id);
		$project = $this -> hammermodel -> getProjects($ids);
		$project = $project[0];
		
		
		//$data['hm_project_id'] = -1;
		$data['status'] = $status;
		
		$this->db->where('hm_project_id',$project['id']);
		$query = $this->db->get('projects');
		$localProject = $query->row();
		
		$this->saveKeywords($id);
		
		$this->db->where('id',$localProject->id);
		$this->db->update('projects',$data);
	}
	
	public function userDelete($id, $frozen = false) {
		$field = ($frozen)?'id':'hm_project_id';
		$this->db->where($field,$id);
		$this->db->where('user_id', $this->user->id);
		$query = $this->db->get('projects');
		$localProject = $query->row();
		if ($localProject) {
			switch ($localProject->status) {
				case 1:
					$this->saveKeywords($localProject->hm_project_id);
					$this->changeStatus($localProject->id, 5);
					break;
				case 3:
				case 15:
					$this->hardDelete($localProject->id);
					break;
				case 10:
					$this->changeStatus($localProject->id, 5);
					break;
			}
			return true;
		} else {
			return false;
		}
	}
	
	public function delete($id, $frozen = false, $status = 0)
	{
		if ($status != 0) {
			$this->saveKeywords($id);
			
			$data['status'] = $status;
			$this->db->where('hm_project_id',$id);
			$this->db->update('projects',$data);

		} else {
			$this->db->where('id',$id);
			$query = $this->db->get('projects');
			$localProject = $query->row();

			if (!$frozen) {
				$this->db->where('hm_project_id',$id);
				$this->db->delete('projects');
				//$this->hammermodel->deleteProject($localProject->hm_project_id);	
			} else {
				switch ($localProject->status) {
					case 5:
						$this->hammermodel->deleteProject($localProject->hm_project_id);
						
						// Удаляем все данные о проекте
						$this->hardDelete($id);
						break;
					case 10:
						// Удаляем проект из биржи
						$this->hammermodel->deleteProject($localProject->hm_project_id);
						
						// Обозначаем, что проект откреплен от биржи
						$this->db->where('id', $id);
						$this->db->update('projects',array('status'=>15, 'hm_project_id'=>-1));
						
						break;
					default:
						break;
				}
				
			}
		}
		
	}

	private function changeStatus($id, $status) {
		$this->db->where('id', $id);
		$this->db->update('projects',array('status'=>5));
	}

	private function hardDelete($id) {
		$this->db->where('id', $id);
		$this->db->delete('projects');
		
		$this->db->where('project_id',$id);
		$this->db->delete('projects__tmp_keywords');
	}
	
	public function saveKeywords($projectId) {
		$keywords = $this -> hammermodel -> getKeys($projectId);
		$this->db->where('hm_project_id',$projectId);
		$query = $this->db->get('projects');
		$localProject = $query->row();
		foreach ($keywords as $keyword) {
			$newBudget = $keyword['linkbudget'];
			$this -> addTempKeyword($localProject->id, $keyword['word'], $keyword['url'], $newBudget, $keyword['ygeo'], $keyword['ggeo']);
		}
	}
	
	public function updateCosts() {
		$this->load->model('usersmodel','users');
		$users = $this -> users -> get(1,1,-1);
		
		foreach ($users as $user) {
			$userMoney = $this -> money -> calcUserMoney($user -> id);
			$projectCost = $this->callProjectCostByUser($user -> id);
			echo $user->email.' - '.$userMoney.' - '.$projectCost."<br/>";
		}
	}

	public function getNull() {
		$projects = $this->hammermodel->getProjects();
		
		$hammerIds = array();
		foreach ($projects as $project) {
			$localProject = $this->item($project['id'],false,true);
			$hammerIds[]=$project['id'];
			if ($localProject!=null) {
				$user = $this->ion_auth->user($localProject -> user_id)->row();
				$userMoney = $this -> money -> calcUserMoney($user -> id);
				$projectCost = $this->callProjectCostByUser($user -> id);
				$projectResult = $localProject->url.': '.$user->email.' - '.$userMoney.' - '.$projectCost;
				if ($projectCost>=$userMoney) {
					$projectResult.='<strong>У пользователя недостаточно денег!</strong>';
				}
				
				if ($project['removed']>0) {
					$projectResult .= ' <strong>Проект удалён</strong>';
				}
				
				if ($projectCost==0) {
					$projectResult.='<br/>-- <small>'.print_r($project,true).'</small>';
				}
				
			} else {
				if ($project['removed']>0) {
					$projectResult = 'Проект удалён';
				} else {
					$projectResult = '<strong>Проект в работе!</strong>';
				}
			}
			echo $project['id'].' --> '.$projectResult."<br/>";
		}
		
		var_dump($hammerIds);
		echo implode(',',$hammerIds);
	}

	function syncProjectThread() {
		
		$group = time();
		$data['group']=$group;
		$this->db->where('alias','syncProject');
		$this->db->where('group',0);
		$this->db->order_by('value','asc');
		$this->db->limit(30,0);
		$this->db->update('threads',$data);
		
		$this->db->where('group',$group);
		$query = $this->db->get('threads');
		$projects = $query->result();
		
		foreach ($projects as $projectThread) {
			$project = $this->item($projectThread->value);
			$keyCost = $this->syncLinks($project->hm_project_id);
			$this->syncKeywords($project->hm_project_id, $keyCost);
		}
		
		$this->db->where('group',$group);
		$query = $this->db->delete('threads');
	}
	
	function syncKeywords($projectId, $keyCost) {
		$hmKeywords = $this->hammermodel->getKeys($projectId);
		$costMultiplier = $this->settingsmodel->get('costMultiplier');
		if ($hmKeywords) {
			foreach ($hmKeywords as $hmKeyword) {
				//var_dump($hmKeyword);
				//$budget = $this->hammermodel->calcBudget($hmKeyword['linkcost'],1);
				if (isset($keyCost[$hmKeyword['id']])) {
					$budget = $keyCost[$hmKeyword['id']];
				} else {
					$budget = $this->hammermodel->calcBudget($hmKeyword['linkcost'],1);
				}
				$sql = "INSERT INTO projects__keywords (`project_id`,`budget`, `budget_m`, `hm_keyword_id`)
							VALUES ({$projectId},{$budget},{$costMultiplier},{$hmKeyword['id']})
						ON DUPLICATE KEY
							UPDATE budget = {$budget};";
				$query = $this->db->query($sql);
			}
		}
	}
	
	function syncLinks($projectId) {
		$hmLinks = $this->hammermodel->getLinks($projectId);
		$costMultiplier = $this->settingsmodel->get('costMultiplier');
		$keyCost = array();
		
		$this->db->where('hm_project_id',$projectId);
		$this->db->update('projects__links',array('status'=>0));
		$i=1;
		if ($hmLinks) {
			//var_dump($hmLinks);
			foreach ($hmLinks as $hmLink) {
				//var_dump($hmKeyword);
				$budget = $this->hammermodel->calcLinkBudget($hmLink['cost'],1);
				if (!isset($keyCost[$hmLink['keyid']])) {
					$keyCost[$hmLink['keyid']] = $budget;
				} else {
					$keyCost[$hmLink['keyid']] += $budget;
				}
				$sql = "INSERT INTO projects__links (`hm_project_id`,`hm_key_id`,`budget`, `budget_m`, `hm_link_id`, `status`)
							VALUES ({$projectId},{$hmLink['keyid']},{$budget},{$costMultiplier},{$hmLink['id']}, 1)
						ON DUPLICATE KEY
							UPDATE `status` = 1;";
				//echo $i.': '.$sql."\n\r\n\r";
				$i++;
				$query = $this->db->query($sql);
			}
			
			
			
			return $keyCost;
		}
	}
}

?>