<?php
class Products extends CI_Model {
	function __construct()
    {
        parent::__construct();
    }
	
	public function items() {
		$this->db->select('*, products.id as pid');
		$this->db->from('products');
		$this->db->join('product_cases','product_cases.product_id = products.id','left outer');
		$this->db->order_by('products.id ASC, product_cases.count ASC'); 
		
		$query = $this->db->get();
		$products = $this->parseQueryResult($query);
		return $products;
	}
	
	public function item($id)
	{
		$this->db->select('*, products.id as pid');
		$this->db->from('products');
		$this->db->where('products.id',$id);
		$this->db->join('product_cases','product_cases.product_id = products.id','left outer');
		$this->db->order_by('products.id ASC, product_cases.count ASC');
		$query = $this->db->get();
		$product = $this->parseQueryResult($query);
		if (count($product)) {
			return $product[1];	
		} else {
			return false;
		}
		
	}
	
	private function parseQueryResult($query)
	{
		$products = array();
		$productId = -1;
		$tmpProduct = null;
		foreach ($query->result() as $row) {
			if ($row->pid != $productId) {
				
				$products[]=$tmpProduct;
				
				$productId = $row->pid;
				$tmpProduct = $row;
				$tmpProduct->cases = array();
			} else {
				
			}
			
			if ($row->count!==NULL) {
				$tmpProduct->cases[]=array('count'=>$row->count,'cost'=>$row->cost);	
			}
			unset($tmpProduct->count, $tmpProduct->cost,$tmpProduct->id, $tmpProduct->product_id);
		}
		$products[]=$tmpProduct;
		unset($products[0]);
		return $products;
	}
	
	public function save($post)
	{
		$data['title'] = $post['title'];
		$data['base_cost'] = $post['base_cost'];
		$data['status'] = 1;
		
		$result = $this->db->insert('products',$data);
		
		if ($result) {
			$productId = $this->db->insert_id();
			
			$cases = array();
			for ($i=0;$i<count($post['count']);$i++) {
				$tmpCase = array(
					'count'=>$post['count'][$i],
					'cost'=>$post['cost'][$i],
					'product_id'=>$productId
				);
				$cases[]=$tmpCase;
			}
		
			$cases = $this->clearCases($cases);

			$this->db->insert_batch('product_cases',$cases);
		}
	}
	
	public function update($post,$id) {
		$data['title'] = $post['title'];
		$data['base_cost'] = $post['base_cost'];
		$this->db->where('id',$id);
		$result = $this->db->update('products',$data);
		
		if ($result) {
			$productId = $id;
			$this->deleteCases($id);
			$cases = array();
			for ($i=0;$i<count($post['count']);$i++) {
				$tmpCase = array(
					'count'=>$post['count'][$i],
					'cost'=>$post['cost'][$i],
					'product_id'=>$productId
				);
				$cases[]=$tmpCase;
			}
		
			$cases = $this->clearCases($cases);

			$this->db->insert_batch('product_cases',$cases);
		}
	}
	
	public function delete($id)
	{
		$this->deleteCases($id);
		
		$this->db->where('id',$id);
		$this->db->delete('products');
	}
	
	public function deleteCases($id)
	{ 	
		$this->db->where('product_id',$id);
		$this->db->delete('product_cases');
	}
	
	public function clearCases($cases)
	{
		$newCases = array();
		
		foreach ($cases as $key=>$case) {
			$tmpCase = $case;
			$tmpKey = $key;
			foreach ($cases as $key2=>$case2) {
				if ($case2['count']==$tmpCase['count'] && $key2!=$tmpKey) {
					unset($cases[$key]);
				}	
			}
			
		}
		
		foreach ($cases as $case) {
			if ($case['cost']!=0 || $case['count']!=0) {
				$newCases[]=$case;	
			}
		}
		return $newCases;
	}
	
	public function calcOrder($id,$count)
	{
		$sum = 0;
		if ($count <=0 ) {
			$data['cost']=0;
			$data['sum']=$data['cost']*$count;
			return $data;
		}
		$this->db->select('*, products.id as pid');
		$this->db->from('products');
		$this->db->where("product_cases.count <= {$count} AND products.id = {$id}");
		$this->db->join('product_cases','product_cases.product_id = products.id','left outer');
		$this->db->order_by('product_cases.count DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		$cost = 0;
		if ($query->num_rows() > 0) {
			$case = $query->result();
			$cost = $case[0]->cost;	
		} else {
			$this->db->select('*');
			$this->db->from('products');
			$this->db->where("id = {$id}");
			$query = $this->db->get();
			$case = $query->result();
			$cost = $case[0]->base_cost;
		}
		
		$data['cost']=$cost;
		$data['sum']=$cost*$count;
		return $data;
	}
}

?>