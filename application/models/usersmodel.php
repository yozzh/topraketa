<?php
class UsersModel extends CI_Model {

	private $user;

	function __construct() {
		parent::__construct();
		$this -> user = $this -> ion_auth -> user() -> row();
		$this -> load -> model('projectsmodel','projects');
	}

	public function get($active = 1, $page = 0, $limit = 20, $id = -1) {
		$limitStart = ($page-1)*$limit;
		$this->db->where('active',$active);
		$this->db->order_by('id','asc');
		if ($limit > 0) {
			$this->db->limit($limit,$limitStart);	
		}
		$query = $this->db->get('users');
		$users = $query->result();
		$this->additionalInfo($users);
		return $users;
	}
	
	public function search($field,$value) {
		$this->db->where('active',1);
		$this->db->like($field,$value);
		$this->db->order_by('id','asc');
		$query = $this->db->get('users');
		$users = $query->result();
		$this->additionalInfo($users);
		return $users;
	}
	
	private function additionalInfo(&$users) {
		foreach ($users as &$user) {
			$user->groups = $this->ion_auth->get_users_groups($user->id)->result();
			foreach ($user->groups as &$group) {
				$group->class = $this -> getGroupClass($group -> name);
			}
			$user -> balance = $this -> money -> calcUserMoney($user -> id);
			$user -> projectsCount = $this->projects -> getProjectsCount($user->id);
		}
		return $users;
	}
	
	public function count($active=1) {
		$this->db->where('active',1);
		$this->db->from('users');
		return $this->db->count_all_results();
	}

	function getUserExpense($userId) {

	}

	function activate($userId, $sum, $bonus) {
		$this -> load -> helper('string');
		$password = random_string('alnum', 8);
		$this->ion_auth->update($userId,array(
			'password'=>$password
		));
		
		$user = $this->ion_auth->user($userId)->row();
		
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$data['email']=$user->email;
		$data['password']=$password;
		$data['sum']=$sum;
		$data['bonus']=$bonus;
		$body = $this->template->build('modules/users/email/activate',$data, true);
		$this->subscribemodel->send($user->email,'Поздравляем Вас с регистрацией в системе!', $body);
		
		$this->projects->startFrozenByUser($userId);
	}
	
	private function getGroupClass($groupName) {
		$groupClass = '';
		switch ($groupName) {
			case 'admin' :
				$groupClass = 'label-important';
				break;
			case 'ticket_admin' :
				$groupClass = 'label-warning';
				break;
			case 'members' :
				$groupClass = 'label-info';
				break;
			case 'ticket_moderator' :
				$groupClass = 'label-success';
				break;
		}
		return $groupClass;
	}

	public function updateGroups($userId, $groups) {
		$this -> db -> where('user_id', $userId);
		$this -> db -> delete('users_groups');

		foreach ($groups as $group) {
			$data = array('user_id' => $userId, 'group_id' => $group);

			$this -> db -> insert('users_groups', $data);
		}
	}

	public function lock($userId, $active = 0) {
		$data = array('active' => $active);
		$this -> db -> where('id', $userId);
		$this -> db -> update('users', $data);
	}

}
?>