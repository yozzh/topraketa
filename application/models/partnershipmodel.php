<?php
class PartnershipModel extends CI_Model {
	private $user;
	public $salt = 'referal';

	function __construct() {
		parent::__construct();
		$this -> user = $this -> ion_auth -> user() -> row();
	}
	
	function getClients() {
		$this->db->where('partner_id',$this->user->id);
		$query = $this->db->get('users');
		return $query->result();
	}

	public function link($user = null) {
		if ($user == null) {
			$user = $this -> user;
		} else {
			$user = $this->ion_auth->user($user)->row();
		}
		
		$link = 'http://top-raketa.ru/partnership/code/'.$this->code($user);
		return $link;		
	}
	
	/**
	 * Функция генерирует реферальный код пользователя, основываясь на его ID и E-mail
	 *
	 * @return string: реферальный код
	 * @author  
	 */
	function code($user) {
		$code = md5($user->id.$user->email.$this->salt);
		return $code;
	}
	
	/**
	 * Функция возвращает id пользователя по реферальному коду
	 *
	 * @return int: id пользователя
	 * @author  
	 */
	function decode($code) {
		$this->db->where("MD5(CONCAT(users.id,users.email,'{$this->salt}')) = '{$code}'");
		$query = $this->db->get('users');
		$user = $query->row();
		if ($user != null) {
			return $user->id;
		} else {
			return false;
		}
	}
	
	/**
	 * Функция отправляет приглашение на электронный ящик нового пользователя и пополняет баланс на заданную сумму
	 *
	 * @return void
	 * @author  
	 */
	function sendInvite($email, $password, $sum) {
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$data['email']=$email;
		$data['password']=$password;
		$data['sum']=$sum;
		$body = $this->template->build('modules/partnership/email/invite',$data, true);
		$this->subscribemodel->send($email,$sum.' р. на раскрутку в подарок!', $body);
	}
	
	/**
	 * Функция пополняет баланс пользователя на бонусные 300 р.
	 *
	 * @return void
	 * @author  
	 */
	function addSocialBonus($email, $sum) {
		$this->load->model('moneymodel','money');
		$comment = "Бонусное пополнение для социальных сетей.";
		
		$this->db->where('email',$email);
		$query = $this->db->get('users');
		$userId = $query->row()->id;
		
		$data = array(
			'user_id'=>$userId,
			'value'=>$sum,
			'date' => date("Y-m-d H:i:s",time()),
			'status' => 2,
			'target_id' => '',
			'comment' => $comment,
			'type' => 'bonus'
		);
		
		$result = $this->money->addExpense($data);
	}

}
?>