<?php
class LinksModel extends CI_Model {
	
	private $user;
	
	function __construct()
    {
        parent::__construct();
		$this->user = $this->ion_auth->user()->row();
    }
	
	public function get($projectId) {
		
		$result = $this->hammermodel->getLinks($projectId);
		$linkCost = $this->benefit->getLinkCost($projectId);
		//var_dump($linkCost);
		$links = array();
		if (is_array($result)) {
		foreach ($result as $hmLink) {
			$hmLink['linkcost'] = (isset($linkCost[$hmLink['id']]))?round($linkCost[$hmLink['id']]/30,2):0;
			$links[] = $hmLink;
		}
		}
		return $links;
	}
	
	public function getCostByProject($projectId) {
		$sql = "SELECT SUM(budget) as sum FROM projects__links WHERE hm_project_id = {$projectId} AND status = 1";
		//echo $sql;
		$query = $this->db->query($sql);
		$sum = ($query->row()->sum!=NULL)?$query->row()->sum:0;
		//$sum = round ($sum,2);
		return $sum;
	}
	
	public function delete($linkId)
	{
		$this->hammermodel->deleteLink($linkId);
		$this->db->where('hm_link_id',$linkId);
		$this->db->update('projects__links',array('status'=>0));
	}
}

?>