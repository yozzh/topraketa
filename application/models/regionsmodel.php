<?php
class RegionsModel extends CI_Model {
	public $ya_regions;
	public $g_regions;
	
	function __construct()
    {
        parent::__construct();
		$this -> load -> helper('file');
		$this->loadRegions();
    }
	
	function loadRegions() {
		$string = read_file(APPPATH.'data/ya_regions.json');
		$this->ya_regions = json_decode($string);
		
		$string = read_file(APPPATH.'data/google_regions.json');
		$this->g_regions = json_decode($string);
	}
	
	function getYaRegions($format = '') {
		switch ($format) {
			case '':
				return $this->ya_regions;
				break;
			case 'json':
				return json_encode($this->ya_regions);		
				break;
		}
	}
	
	function getGoogleRegions($format = '') {
		switch ($format) {
			case '':
				return $this->g_regions;
				break;
			case 'json':
				return json_encode($this->g_regions);		
				break;
		}
	}
	
	function getYaRegionById($id) {
		foreach ($this->ya_regions as $region) {
			if ($region->id == $id) {
				return $region;
			}
		}
		return false;
	}
	
	function getGoogleRegionById($id) {
		foreach ($this->g_regions as $region) {
			if ($region->id == $id) {
				return $region;
			}
		}
		return false;
	}
}

?>