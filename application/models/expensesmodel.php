<?php
class ExpensesModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->model('projectsmodel','projects');
		$this->load->model('moneymodel','money');
	}
	
	function updateExpenses() {
		$data['items'] = $this -> projects -> items(false,0,true);
		$ids = array();
		$projectUser = array();
		foreach ($data['items'] as $item) {
			$ids[] = $item -> hm_project_id;
			$projectUser[$item -> hm_project_id]=$item -> user_id;
		}
		$projects = $this -> hammermodel -> getProjects($ids);
		foreach ($projects as $project) {
			$userId = $projectUser[$project['id']];
			$projectExpense = $this->projects->getProjectCost($project['id'])/30;
			
			$costBenefit = $this->settingsmodel->get('costBenefit');
			$benefitExpense = $projectExpense*(($costBenefit)/100);
			
			$userMoney = $this->money->calcUserMoney($userId);
			if ($projectExpense == 0) {
				continue;
			}
			echo $project['domain'].': '.$projectExpense.' - '.$userMoney;
			if ($userMoney >= ($projectExpense+$benefitExpense)) {
				
				$comment = "Списание денежных средств за продвижение проекта {$project['domain']}";
				
				$data = array(
					'user_id'=>$userId,
					'value'=>(-$projectExpense),
					'date' => date("Y-m-d H:i:s",time()),
					'status' => 2,
					'target_id' => $project['id'],
					'comment' => $comment,
					'type' => 'payment'
				);
				
				$result = $this->money->addExpense($data);
				//$result = true;
				
				if ($result) {
					$comment = "Вознаграждение Top-Raketa.ru за проект {$project['domain']}";
				
					$data = array(
						'user_id'=>$userId,
						'value'=>(-$benefitExpense),
						'date' => date("Y-m-d H:i:s",time()),
						'status' => 2,
						'target_id' => $project['id'],
						'comment' => $comment,
						'type' => 'system'
					);
					
					$result = $this->money->addExpense($data);
					
					$this->addBenefit($project, $userId,$benefitExpense);
				}
			} else {
				$comment = "Отмена списания денежных средств за продвижение проекта {$project['domain']}. Проект заблокирован";
				$targetUser = $this->ion_auth->user($userId)->row();
				$message['userBalance']=$userMoney;
				$this -> template -> set_theme('frontend_second');
				$this -> template -> set_layout('ajax');
				$body = $this->template->build('modules/projects/email/block',$message, true);
				$this->subscribemodel->send($targetUser->email,"{$project['domain']} - Проект заблокирован (Недостаточно средств)", $body);
				
				/*$data = array(
					'user_id'=>$userId,
					'value'=>(-$projectExpense-$benefitExpense),
					'date' => date("Y-m-d H:i:s",time()),
					'status' => 3,
					'target_id' => $project['id'],
					'comment' => $comment
				);*/
				
				//$result = $this->money->addExpense($data);
				
				//
				$this->projects->freeze($project['id'],10);
			}
			
			echo "<br/><br/>";
		}
	}
	
	function checkBalance() {
		$group = time();
		$data['group']=$group;
		$this->db->where('alias','checkBalance');
		$this->db->where('group',0);
		$this->db->order_by('value','asc');
		$this->db->limit(30,0);
		$this->db->update('threads',$data);
		
		$this->db->where('group',$group);
		$query = $this->db->get('threads');
		$users = $query->result();
		
		foreach ($users as $user) {
			$userInfo = $this->ion_auth->user($user->value)->row();
			$userBalance = $this -> money -> calcUserMoney($user->value);
			$totalCost = $this->projects->callProjectCostByUser($user->value);
			$daysLeft = ($totalCost==0)?'Не ограничено':(int)floor($userBalance/$totalCost);
			$sendMessage = (is_int($daysLeft) && $daysLeft<=5)?true:false;
			if ($sendMessage) {
				$this->sendLowBalanceMessage($userInfo->email, $daysLeft, $userBalance, $totalCost);
			}
		}
		
		$this->db->where('group',$group);
		$query = $this->db->delete('threads');
	}
	
	function sendLowBalanceMessage($email,$daysLeft,$userBalance,$totalCost) {
		$message = array (
			'daysLeft'=>$daysLeft,
			'userBalance'=>$userBalance,
			'totalCost'=>$totalCost
		);
		$this -> template -> set_theme('frontend_second');
		$this -> template -> set_layout('ajax');
		$body = $this->template->build('modules/money/email/low_balance',$message, true);
		$this->subscribemodel->send($email,"Внимание! Средств на счете хватит на ".$daysLeft." ".proceedTextual($daysLeft, "дней", "день", "дня"), $body);
	}
	
	function updateProjectExpenses(){
		
	}
	
	function addBenefitExpense($project,$userId,$benefitExpense) {
		$comment = "Отмена списания денежных средств за продвижение проекта {$project['domain']}. Проект заблокирован";
		
		$data = array(
			'user_id'=>$userId,
			'value'=>(-$benefitExpense),
			'date' => date("Y-m-d H:i:s",time()),
			'status' => 3,
			'target_id' => $project['id'],
			'comment' => $comment
		);
		
		$result = $this->money->addExpense($data);
		
	}
	
	function addBenefit($project,$userId,$benefitExpense) {
		$this->db->where('id',$userId);
		$user = $this->db->get('users');
		$user = $user->row();
		if ($user->partner_id != NULL) {
			$groups = $this->ion_auth->get_users_groups($user->partner_id);
			$benefit = 0;
			foreach ($groups->result() as $group) {
				if ($group->value!=NULL) {
					$benefit = $group->value;
				}
			}
			
			$benefitAddins = $benefitExpense*(($benefit)/100);
			$comment = "Партнерское отчисление за проект от пользователя {$user->email} ({$benefit}%)";
			
			$data = array(
				'user_id'=>$user->partner_id,
				'value'=>$benefitAddins,
				'date' => date("Y-m-d H:i:s",time()),
				'status' => 2,
				'type' => 'benefit',
				'target_id' => $project['id'],
				'comment' => $comment
			);
			
			$result = $this->money->addExpense($data);
		}
	}
}
?>