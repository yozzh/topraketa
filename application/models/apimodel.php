<?php
class ApiModel extends CI_Model {
	private $user;
	public $salt = 'referal';

	function __construct() {
		parent::__construct();
		$this -> user = $this -> ion_auth -> user() -> row();
	}
	
	function getJson() {
		$json = $this->input->post('json');
		$request = json_decode($json);
		if ($request == NULL) {
			$return['code'] = 'ERROR_JSON';
			$return['message'] = 'Полученные данные не корректны';
			header('Content-type: application/json');
			echo json_encode($return);
			exit;
		} else {
			return $request;
		}
	}
	
	function checkUserPartner($userId,$partnerId) {
		$this->db->where('id',$userId);
		$this->db->where('partner_id',$partnerId);
		$query = $this->db->get('users');
		$user = $query->row();
		if ($user) {
			return true;
		} else {
			$data['code']='ERROR_PARTNERSHIP';
			$data['message']='Пользователь не зарегистрирован у данного партнера.';
			header('Content-type: application/json');
			echo json_encode($data);
			exit;
		}
	}
	
	function checkUserProject($userId,$projectId) {
		$this->db->where('user_id',$userId);
		$this->db->where('id',$projectId);
		$query = $this->db->get('projects');
		$project = $query->row();
		if ($project) {
			return true;
		} else {
			$data['code']='ERROR_USER';
			$data['message']='Проект не существует у данного пользователя.';
			header('Content-type: application/json');
			echo json_encode($data);
			exit;
		}
	}
	
	function checkKeywordsArray($keywords, $withBudget = true) {
		if (count($keywords)) {
			$i=0;
			foreach ($keywords as $keyword) {
				
				$checkResult = isset($keyword->text) && isset($keyword->ya_region) && isset($keyword->g_region);
				
				if ($withBudget) {
					$checkResult = $checkResult && isset($keyword->budget) && isset($keyword->url);
				}
					
				if (!$checkResult) {
					$data['code']='ERROR';
					$data['message']="Список ключевых слов некорректен (Номер ключа с ошибкой: {$i}).";
					header('Content-type: application/json');
					echo json_encode($data);
					exit;
				}
				$i++;
			}
			return true;
		} else {
			$data['code']='ERROR';
			$data['message']='Список ключевых слов пуст.';
			header('Content-type: application/json');
			echo json_encode($data);
			exit;
		}
	}
}
?>