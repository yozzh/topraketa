<?php
class PagesModel extends CI_Model {
	public $menuItems;
	
	function __construct()
    {
        parent::__construct();
		
		$this->load->spark('template/1.9.0');
		$this->load->spark('ion_auth/2.4.0');
		
		//$this->loadInformer();
    }
	
	public function loadInformer()
	{
		if ($this->ion_auth->logged_in()) {
			$this->load->model('moneyModel','money');
			$currentSum = $this->money->calcUserMoney();
		
			$this->load->model('ordersModel','orders');
			$currentOrders = $this->orders->calcUserOrdersByStatus();
			
			$this->template->set(array(
				'currentSum'=>$currentSum,
				'currentOrders'=>$currentOrders
			));
			$this->template->set_partial('informer','partials/informer');	
		} else {
			$this->template->set_partial('informer','partials/auth');
		}
	}
	
	public function getInformer() {
		$this->template->set_theme('frontend');
		$this->template->set_layout('ajax');
		
		if ($this->ion_auth->logged_in()) {
			$this->load->model('moneyModel','money');
			$currentSum = $this->money->calcUserMoney();
		
			$this->load->model('ordersModel','orders');
			$currentOrders = $this->orders->calcUserOrdersByStatus();
			
			$this->template->set(array(
				'currentSum'=>$currentSum,
				'currentOrders'=>$currentOrders
			));
			return $this->template->build('partials/informer',array(),true);
		} else {
			
			//$this->template->set_partial('informer','partials/auth');
			return $this->template->build('partials/auth',array(),true);
		}
	}
	
	public function loginSuccess() {
		$this->template->set_theme('frontend');
		$this->template->set_layout('ajax');
		return $this->template->build('partials/login_success',array(),true);
	}
	
	/**
	 * Creates captcha image and return image tag
	 *
	 * @return Captcha array
	 * @author  YozZh
	 */
	function createCaptcha() {
		$this->load->helper('captcha', 'string');
		
		$vals = array(
			'word' => strtoupper(random_string('alnum', 5)),
		    'img_path'	 => './include/captcha/',
		    'img_url'	 => '/include/captcha/',
		    'font_path'	 => './include/frontend/fonts/captchafont.ttf',
		    'img_width'	 => '100',
		    'img_height' => 30,
		    'expiration' => 7200
		    );
		
		$cap = create_captcha($vals);	
		
		$data = array(
		    'captcha_time'	=> $cap['time'],
		    'ip_address'	=> $this->input->ip_address(),
		    'word'	 => $cap['word']
		    );
		
		$query = $this->db->insert_string('captcha', $data);
		$this->db->query($query);
		return $cap['image'];
	}
  
  /**
   * Returns page from pages table
   *
   * @return Page object
   * @author  
   */
  function page($id,$field='id') {
    $this->db->where($field, $id);
    $query = $this->db->get('pages');
    return $query->row();
  }
  
  /**
   * Returns pages from pages table
   *
   * @return Pages array
   * @author  
   */
  function pages() {
    $this->db->where('status < 3');
    $this->db->order_by('order','ASC');
    $query = $this->db->get('pages');
    return $query->result();
  }
  
  /**
   * Save page to database
   *
   * @return bool
   * @author  
   */
  function save($data, $id = false) {
    if ($id===false) {
      $this->db->insert('pages',$data);
    } else {
      $this->db->where('id',$id);
      $this->db->update('pages',$data);
    }
    return true;
  }
	
  /**
   * Send page to archve
   *
   * @return bool
   * @author  
   */
  function archive($pageId) {
    $this->db->where('id',$pageId);
    $this->db->update('pages',array('status'=>3));
  }

  /**
   * Return max order value
   *
   * @return int
   * @author  
   */
  function getMaxOrder() {
    $sql = "SELECT MAX(pages.order) as maxOrder FROM pages";
    $query = $this->db->query($sql);
    $res = $query->row();
    return ($res->maxOrder+1);
  }
}

?>