<?php
class FinanceModel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this -> load -> model('projectsmodel', 'projects');
		$this -> load -> model('moneymodel', 'money');
	}

	public function get($active = 1, $page = 0, $limit = 20, $id = -1) {
		$limitStart = ($page - 1) * $limit;
		$this -> db -> where('active', $active);
		$this -> db -> order_by('id', 'asc');
		if ($limit > 0) {
			$this -> db -> limit($limit, $limitStart);
		}
		$query = $this -> db -> get('users');
		$users = $query -> result();
		$this -> additionalInfo($users);
		return $users;
	}

	public function search($field,$value) {
		$this->db->where('active',1);
		$this->db->like($field,$value);
		$this->db->order_by('id','asc');
		$query = $this->db->get('users');
		$users = $query->result();
		$this->additionalInfo($users);
		return $users;
	}
	
	private function additionalInfo(&$users) {
		foreach ($users as &$user) {
			$user -> balance = $this -> getBalance($user -> id);
		}
		return $users;
	}

	public function getBalance($userId=false) {
		
		$userId = ($userId)?$userId:$this->user->id;
		$this->db->select('SUM(IF(`value`>0),`value`,0) as plus, SUM(IF(value<0),value,0) as minus');
		$this->db->where('user_id',$userId);
        $this->db->where('status',2);
		$sql = "SELECT
					ROUND(SUM(IF(`value` > 0, `value`, 0)),2)AS plus,
					ROUND(SUM(IF(`value` < 0, `value`, 0)),2)AS minus
				FROM
					payments
				WHERE
					user_id = {$userId}
				AND STATUS = 2";
		$query = $this->db->query($sql);
		$return = $query->row();
		if ($return!=NULL) {
			return $return;	
		} else {
			return 0;
		}
	}
	
	public function count() {
		$sql = "SELECT count(*) as count FROM users WHERE active = 1";
		$query = $this->db->query($sql);
		$return = $query->row();
		return $return->count;
	}
	
	/**
	 * Return user's finance information
	 *
	 * @return array
	 * @author  
	 */
	function statistic($userId) {
		
		$sql = "SELECT
					ROUND(SUM(IF(`value` > 0, `value`, 0)), 2)AS plus,
					ROUND(SUM(IF(`value` < 0, `value`, 0)), 2)AS minus,
					date_format(date, \"%m.%Y\") AS `month`,
					date_format(date, \"%m\") AS `m`,
					date_format(date, \"%Y\") AS `y`,
					date
				FROM
					payments
				WHERE
					user_id = {$userId}
				AND STATUS = 2
				GROUP BY
					`month`
				ORDER BY
					date ASC";
		$query = $this->db->query($sql);
		$result = $query->result();
		$startBalance = 0;
		foreach ($result as &$row) {
			$startBalance = $startBalance + $row->plus + $row->minus;
			$row->balance = $startBalance;
		}
		return $result;
	}
	
	/**
	 * Return total finance statistic
	 *
	 * @return void
	 * @author  
	 */
	function stat() {
		$sql = "SELECT
					ROUND(SUM(IF(`value` > 0, `value`, 0)), 2)AS plus,
					ROUND(SUM(IF(`value` < 0, `value`, 0)), 2)AS minus,
					ROUND(SUM(IF(`type` = 'online', `value`, 0)), 2)AS `online`
				FROM
					payments
				WHERE
					date_format(date, '%Y%m') = date_format(now(), '%Y%m') AND
					`status` = 2";
		$query = $this->db->query($sql);
		return $query->row();
	}
}
