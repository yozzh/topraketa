<?php
class BenefitModel extends CI_Model {
	private $settings = array();
	private $ranges = array();
	
	function __construct() {
		parent::__construct();
		$this->getKeywordRanges();
	}
	
	function syncProjects() {
		$data['items'] = $this -> projects -> items(false);
		$ids = array();
		$projectUser = array();
		foreach ($data['items'] as $item) {
			$ids[] = $item -> hm_project_id;
			$projectUser[$item -> hm_project_id]=$item -> user_id;
		}
		$projects = $this -> hammermodel -> getProjects($ids);
		foreach ($projects as $project) {
			$keyCost = $this->syncLinks($project['id']);
			$this->syncKeywords($project['id'], $keyCost);
		}
	}
	
	 function syncKeywords($projectId, $keyCost) {
    $hmKeywords = $this->hammermodel->getKeys($projectId);
    $costMultiplier = $this->settingsmodel->get('costMultiplier');
    if ($hmKeywords) {
      foreach ($hmKeywords as $hmKeyword) {
        //var_dump($hmKeyword);
        //$budget = $this->hammermodel->calcBudget($hmKeyword['linkcost'],1);
        if (isset($keyCost[$hmKeyword['id']])) {
          $budget = $keyCost[$hmKeyword['id']];
        } else {
          $budget = $this->hammermodel->calcBudget($hmKeyword['linkcost'],1);
        }
        $sql = "INSERT INTO projects__keywords (`project_id`,`budget`, `budget_m`, `hm_keyword_id`)
              VALUES ({$projectId},{$budget},{$costMultiplier},{$hmKeyword['id']})
            ON DUPLICATE KEY
              UPDATE budget = {$budget};";
        $query = $this->db->query($sql);
      }
    }
  }
  
  function syncLinks($projectId) {
    $hmLinks = $this->hammermodel->getLinks($projectId);
    $costMultiplier = $this->settingsmodel->get('costMultiplier');
    $keyCost = array();
    
    $this->db->where('hm_project_id',$projectId);
    $this->db->update('projects__links',array('status'=>0));
    $i=1;
    if ($hmLinks) {
      //var_dump($hmLinks);
      foreach ($hmLinks as $hmLink) {
        //var_dump($hmKeyword);
        $budget = $this->hammermodel->calcLinkBudget($hmLink['cost'],1);
        if (!isset($keyCost[$hmLink['keyid']])) {
          $keyCost[$hmLink['keyid']] = $budget;
        } else {
          $keyCost[$hmLink['keyid']] += $budget;
        }
        $sql = "INSERT INTO projects__links (`hm_project_id`,`hm_key_id`,`budget`, `budget_m`, `hm_link_id`, `status`)
              VALUES ({$projectId},{$hmLink['keyid']},{$budget},{$costMultiplier},{$hmLink['id']}, 1)
            ON DUPLICATE KEY
              UPDATE `status` = 1;";
        //echo $i.': '.$sql."\n\r\n\r";
        $i++;
        $query = $this->db->query($sql);
      }
      
      
      
      return $keyCost;
    }
  }
  
	
	function calcKeywordBudget($budget, $from = false, $ranged = false) {
		if ($ranged) {
			$costPercent = $this->getKeywordRangePercent($budget,true);
		} else {
			$costPercent = $this->settingsmodel->get('costMultiplier');	
		}

		
		
		$totalPercent = 100+$costPercent;
		$budget = (!$from)?$budget*(100/$totalPercent):$budget*($totalPercent/100);
		$budget = round($budget,2);

		//var_dump($budget);
		return $budget;
	}
	
	function getKeywordRangePercent($budget) {
		$returnRange = null;
		foreach ($this->ranges as $range) {
			if ($budget >= $range->range) {
				$returnRange = $range->percent;		
			}
		}
		return $returnRange;
	}
	
	function getKeywordRanges() {
		$this->db->order_by('range','asc');
		$query = $this->db->get('cost_ranges');
		$this->ranges = $query->result();
	}
	
	function getProjectBudget($projectId) {
		$sql = "SELECT SUM(budget) as sum FROM projects__links WHERE hm_project_id = {$projectId}";
		$query = $this->db->query($sql);
		$budget = round($query->row()->sum/30,2);
		return $budget;
	}
	
	function getKeyCost($id) {
		$this->db->where('project_id',$id);
		$query = $this->db->get('projects__keywords');
		$keyCost = array();
		foreach ($query->result() as $keyCostItem) {
			$keyCost[$keyCostItem->hm_keyword_id]=$keyCostItem->budget;
		}
		return $keyCost;
	}
	
	function getLinkCost($id) {
		$this->db->where('hm_project_id',$id);
		$query = $this->db->get('projects__links');
		$linkCost = array();
		foreach ($query->result() as $linkCostItem) {
			$linkCost[$linkCostItem->hm_link_id]=$linkCostItem->budget;
		}
		return $linkCost;
	}
	
	
	
	
}