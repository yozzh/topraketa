id = 0;

/*!
 *
 *  Copyright (c) David Bushell | http://dbushell.com/
 *
 */
(function(window, document, undefined)
{

    var transformProp = window.Modernizr.prefixed('transform'),
        transitionProp = window.Modernizr.prefixed('transition'),
        transitionEnd = (function() {
            var props = {
                'WebkitTransition' : 'webkitTransitionEnd',
                'MozTransition'    : 'transitionend',
                'OTransition'      : 'oTransitionEnd otransitionend',
                'msTransition'     : 'MSTransitionEnd',
                'transition'       : 'transitionend'
            };
            return props.hasOwnProperty(transitionProp) ? props[transitionProp] : false;
        })(),

        hasTT = transitionEnd && transitionProp && transitionProp;

    var log = function(obj)
    {
        if (typeof window.console === 'object' && typeof window.console.log === 'function') {
            window.console.log(obj);
        }
    };

    window.App = (function()
    {

        var _init = false, app = { };

        app.init = function()
        {
            if (_init) {
                return;
            }
            _init = true;

            app.win    = $(window);
            app.docEl  = $(document.documentElement);
            app.bodyEl = $(document.body);

            app.docEl.addClass('js-ready js-' + (hasTT ? 'advanced' : 'basic'));

            var menuLinkEl = $('#menu-link'),
                menuEl = $('#menu'),
                wrapEl = $('#wrap');

            var closeMenu =function()
            {
                if (hasTT) {
                    menuEl.one(transitionEnd, function(e) {
                        app.docEl.removeClass('js-offcanvas');
                    });
                } else {
                    app.docEl.removeClass('js-offcanvas');
                }
                app.docEl.removeClass('js-menu');
            };

            var openMenu = function()
            {
                app.docEl.addClass('js-offcanvas js-menu');
            };

            menuLinkEl.on('click', function(e)
            {
                if (app.docEl.hasClass('js-menu')) {
                    closeMenu();
                } else {
                    openMenu();
                }
                e.preventDefault();
            });

         

        };

        return app;

    })();

})(window, window.document);

$(document).ready(function() {
	
	/*
	 * Product Scripts
	 */
	
	$('.productCase a.edit').live('click',function(){
		var productCase = $(this).closest('.productCase');
		var countInput = $('<input type="number" name="f_count" class="span1" />');
		$(countInput).val($('.count',productCase).val());
		$('.caseCount', productCase).empty().append(countInput);
		
		var costInput = $('<input type="text" name="f_cost" class="span1"/>');
		$(costInput).val($('.cost',productCase).val());
		$('.caseCost', productCase).empty().append(costInput);
		
		$('.caseActions', productCase).addClass('hidden');
		$('.caseSave', productCase).removeClass('hidden');
		
	});
	
	$('.productCase a.delete').live('click',function(){
		var productCase = $(this).closest('.productCase');
		if (window.confirm("Удалить порог стоимости?")) { 
		    $(productCase).remove();
		}
	});
	
	$('.addCase').live('click',function(){
		var newCase = $('.newCase .productCase').clone();
		$('.productCases').append(newCase);
	});
	
	$('.productCase a.save').live('click',function(){
		var productCase = $(this).closest('.productCase');
		$('.caseActions', productCase).removeClass('hidden');
		$('.caseSave', productCase).addClass('hidden');
		
		var countSpan = $('<span />');
		var newCount = $('input[name=f_count]',productCase).val();
		$(countSpan).text(newCount+' шт.');
		$('.caseCount', productCase).empty().append(countSpan);
		$('.count',productCase).val(newCount);
		
		var costSpan = $('<span />');
		var newCost = $('input[name=f_cost]',productCase).val();
		$(costSpan).text(newCost+' р.');
		$('.caseCost', productCase).empty().append(costSpan);
		$('.cost',productCase).val(newCost);
	});
	
	$('.productDelete').click(function(){
		if (window.confirm("Удалить этот продукт?")) { 
		    var url = $(this).attr('href');
		    window.location = url;
		}
		return false;
	});
	
	/*
	 * Order Scripts
	 * 
	 */
	
	$('.orderCount').keydown(function(){
		clearTimeout(id);
		id = setTimeout(updateCost, 1000);
	});
	
	$('.orderCancel').click(function(){
		if (window.confirm("Отменить этот заказ?")) { 
		    var url = $(this).attr('href');
		    window.location = url;
		}
		return false;
	});
});

function updateCost() {
	var productId = $('select[name="product"]').val();
	var count = $('input[name="count"]').val();
	$.post("/orders/getOrderSum", { productId: productId, count: count })
	.done(function(data) {
	  var response = $.parseJSON(data);
	  var sum = response.data.sum+' руб.';
	  var cost = 'по '+response.data.cost+' р. за посетителя';
	  $('.orderSum .cost').empty().text(sum);
	  $('.orderSum .cost_comment').empty().text(cost);
	  
	});
}
