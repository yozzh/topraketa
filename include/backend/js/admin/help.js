jQuery(document).ready(function() {
  tinymce.init({
    selector: "textarea#content",
    plugins : 'advlist autolink link image lists charmap print preview code',
    image_advtab: true
 });
});