$(document).ready(function() {
	$('.openStat').click(function(){
		var editRow = $(this).closest('tr');
		var userId = editRow.attr('data-key');
		$.post('/admin/finance/users/statistic/'+userId,function(response){	
			$('#user-modal .modal-body').empty().append(response.body)
			$('#user-modal .modal-header').empty().append(response.title)
			$('#user-modal').modal('toggle');
		});	
	});
});
