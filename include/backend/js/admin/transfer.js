var projects, users;
$(document).ready(function() {
	$.get('/admin/projects/projectsList',function(data){
		projects = data.projects
		$('input#project').typeahead({
			minLength: 1,
			source: function (query, process) {
			    map = {};
			    states = [];
			    		 
			    $.each(projects, function (i, project) {
			        map[project.id+' - '+project.domain] = project;
			        states.push(project.id+' - '+project.domain);
			    });
			    process(states);
			},
			matcher: function(item) {
				if (item.indexOf(this.query)!=-1) {
					return true;
				}
			},
			sorter: function(items) {
				return items;
			},
			updater: function(item) {
				$('#projectId').val(map[item].id);
				return item;
			},
			highlighter:function(item) {
				var str = item;
				var regex = new RegExp( '(' + this.query + ')', 'gi' );
    			return str.replace( regex, "<strong>$1</strong>" );
			}
		});
	});
	
	$.get('/admin/projects/usersList',function(data){
		users = data.users
		$('input#user').typeahead({
			minLength: 1,
			source: function (query, process) {
			    usersMap = {};
			    states = [];
			    		 
			    $.each(users, function (i, user) {
			        usersMap[user.email] = user;
			        states.push(user.email);
			    });
			    process(states);
			},
			matcher: function(item) {
				if (item.indexOf(this.query)!=-1) {
					return true;
				}
			},
			sorter: function(items) {
				return items;
			},
			updater: function(item) {
				$('#userId').val(usersMap[item].id);
				return item;
			},
			highlighter:function(item) {
				var str = item;
				var regex = new RegExp( '(' + this.query + ')', 'gi' );
    			return str.replace( regex, "<strong>$1</strong>" );
			}
		});
	});
	
});

