
$(document).ready(function() {
	
	$('.userLock').on('click',function() {
		var userId = $(this).closest('tr').attr('data-key');
		$.post('/users/lock', {id: userId}, function(data) {
			var response = $.parseJSON(data);
			openModal('usersModal','Блокировка пользователя',response.data);
		});
		return false;
	});
	
	$('.userUnlock').on('click',function() {
		var userId = $(this).closest('tr').attr('data-key');
		$.post('/users/unlock', {id: userId}, function(data) {
			var response = $.parseJSON(data);
			openModal('usersModal','Блокировка пользователя',response.data);
		});
		return false;
	});
});

function groupsSave(data) {
	var response = $.parseJSON(data);
	
	if (response.status != 'ok') {
			
	} else {

	}
	
	$('#supportModal .modal-body').html(response.data);
	
}

function ticketAdd(data) {
	var response = $.parseJSON(data);
	
	if (response.status != 'ok') {
			
	} else {

	}
	
	$('#supportModal .modal-body').html(response.data);
	
}
