
$(document).ready(function() {
	$('.userEdit').on('click',function() {
		var userId = $(this).closest('tr').attr('data-key');
		$.post('/users/edit', {id: userId}, function(data) {
			var response = $.parseJSON(data);
			openModal('usersModal','Редактирование пользователя',response.data, 970);
		});
		return false;
	});
	
	$('.addMoney').on('click',function() {
		var userId = $(this).closest('tr').attr('data-key');
		$.post('/users/addMoney', {id: userId}, function(data) {
			var response = $.parseJSON(data);
			openModal('usersModal','Пополнение баланса',response.data, 450);
		});
		return false;
	});
	
	$('.userLock').on('click',function() {
		var userId = $(this).closest('tr').attr('data-key');
		$.post('/users/lock', {id: userId}, function(data) {
			var response = $.parseJSON(data);
			openModal('usersModal','Блокировка пользователя',response.data);
		});
		return false;
	});
	
	$('.userUnlock').on('click',function() {
		var userId = $(this).closest('tr').attr('data-key');
		$.post('/users/unlock', {id: userId}, function(data) {
			var response = $.parseJSON(data);
			openModal('usersModal','Блокировка пользователя',response.data);
		});
		return false;
	});
});

function usersSave(data) {
	var response = $.parseJSON(data);
	
	if (response.status != 'ok') {
			
	} else {

	}
	
	$('#usersModal .modal-body').html(response.data);
	
}
