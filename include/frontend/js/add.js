var mapYandex = {};
var mapGoogle = {};
var keywordsStore = new Array();

$(document).ready(function() {
	$('.addKeywords').click(function() {
		addKeywords(this);
	});

	addTypeAhead();
	
	$('body').on('focus','#ya_region', function() {
		loadMaps();
	});
	
	
	$('body').on('focus','#g_region', function() {
		loadMaps();
	});

	$('#ya_is_region').change(function() {
		var val = $(this).is(':checked');
		if (val) {
			$('#ya_region').prop('disabled', false);
			loadMaps();
		} else {
			$('#ya_region').prop('disabled', true);
		}
	});
	
	$('#g_is_region').change(function() {
		var val = $(this).is(':checked');
		if (val) {
			$('#g_region').prop('disabled', false);
			loadMaps();
		} else {
			$('#g_region').prop('disabled', true);
		}
	});
});

function regionSearch(query, map, type) {
	var result = map[query];
	if (result !== undefined) {
		applyRegion(query, type);
	}
}

function applyRegion(item,type) {
	if (type == 'yandex') {
		$('#ya_region_hidden').val(mapYandex[item].id);
		$('#ya_region_hidden2').val(mapYandex[item].yandexid);
		
	} else {
		$('#g_region_hidden').val(mapGoogle[item].id);
	}
	console.debug(item);
}

function addTypeAhead() {
	$('#ya_region').typeahead({
		source : function(query, process) {
			objects = [];
			$.each(mapYandex, function(i, object) {
				objects.push(object.name);
			});
			process(objects);
		},
		updater : function(item) {
			applyRegion(item,'yandex');
			return item;
		}
	});
	
	$('#g_region').typeahead({
		source : function(query, process) {
			objects = [];
			$.each(mapGoogle, function(i, object) {
				objects.push(object.name);
			});
			process(objects);
		},
		updater : function(item) {
			applyRegion(item,'google');
			return item;
		}
	});
}

function loadMaps() {
	var mapLength = Object.keys(mapGoogle).length;
	if (mapLength==0) {
		$("#selectGoogleRegion").mask('Загружается список регионов');
		$.get('/services/getGoogleRegions', function(response) {
			$("#selectGoogleRegion").unmask();
			var data = $.parseJSON(response);
			$.each(data, function(i, object) {
				mapGoogle[object.name] = object;
			});
		});
	}
	
	var mapLength = Object.keys(mapYandex).length;
	if (mapLength==0) {
		$("#selectYandexRegion").mask('Загружается список регионов');
		$.get('/services/getYaRegions', function(response) {
			$("#selectYandexRegion").unmask();
			var data = $.parseJSON(response);
			$.each(data, function(i, object) {
				mapYandex[object.name] = object;
			});
		});
	}
}

$(document).ready(function() {
	$('body').on('click','.removeKeyword',function(){
		var parentRow = $(this).closest('tr');
		var id = parentRow.attr('data-key');
		
		removeFromStore(id,keywordsStore);
		$(parentRow).remove();
		updateKeywordsSum();
		if (keywordsStore.length==0) {
			$('#keywords_02').fadeOut();
			$('#keywords_02_empty').fadeIn();
		}
		setTimeout(setWhiteWidth,500);
	});
	
	$('#addProject').on('click',function(){
		var form = $(this).closest('form');

		var keysJSON = (keywordsStore.length)?JSON.stringify(keywordsStore):'';
		$('#keywordsInput_02',form).val(keysJSON);	
		
		form.submit();
		return false;
	});
	
	if ($('#keywordsInput_02').length) {
		if ($('#keywordsInput_02').val()!="") {
			var items = $.parseJSON($('#keywordsInput_02').val());
			fillKeywordsTable(items, '#keywordsTable');
		}
	}
});


function addKeywords(button) {
	regionSearch($('#ya_region').val(),mapYandex,'yandex');
	regionSearch($('#g_region').val(),mapGoogle,'google');

	var form = $(button).closest('form');
	var data = $(form).serialize();
	$(form).mask("Обновление итогового списка ключей..");
	$.ajax({
		type : "POST",
		url : "/calc/addKeywords",
		data : data,
		success : function(data) {
			console.debug(data);
			var response = $.parseJSON(data);
			if (response.result == 'success') {
				fillKeywordsTable(response.data, '#keywordsTable');
				updateKeywordsSum();
				$('#keywords_01').val('');
				
			}
			$(form).unmask();
		}
	});
	console.debug(keywordsStore);
	return false;
}

function fillKeywordsTable(data, target) {
	var tBody = $('tbody', target);
	var tableWrapper = tBody.closest('.datagrid');
	if (!tBody.length) {
		return false;
	}
	//tBody.empty();
	
	for (var i = 0; i < data.length; i++) {
		var keyId = addToStore(data[i],keywordsStore);
		if (keyId!==false) {
			addDataRow(data[i],target,keyId);
		}
	}
	$(tableWrapper).fadeIn();
	$('#keywords_02_empty').fadeOut();
	setTimeout(setWhiteWidth,500);
}

function updateKeywordsSum() {
	var sum = 0
	$.each(keywordsStore,function(i,keyword){
		sum+=keyword.budget;
	});
	sum = Math.round (sum * 100) / 100;
	sum = formatNumber(sum);
	$('.summaryBudget').empty().text(sum+' р./мес.');
}

function addDataRow(object, dataTable, id) {
	var tmpRow = $('<tr />').attr('data-key',id);
	$('<td />').append(renderRegions(object)).appendTo(tmpRow);
	$('<td />').text(object.title).appendTo(tmpRow);
	$('<td />').text(object.visits).appendTo(tmpRow);
	$('<td />').text(object.views).appendTo(tmpRow);
	$('<td />').append(renderBudget(object.budget)).appendTo(tmpRow);
	$('<td />').append(renderPageLink(object.page)).appendTo(tmpRow);
	$('<td />').append(renderActions()).appendTo(tmpRow);
	$('tbody',dataTable).append(tmpRow);
	
	$('a[data-toggle="tooltip"]').tooltip();
}

function renderRegions(object) {
	var regionsDiv = $('<div class="regions" />');
	regionsDiv.append($('<a class="yaIcon" data-toggle="tooltip" />').attr('title',object.ya_region_title));
	regionsDiv.append($('<a class="gIcon" data-toggle="tooltip" />').attr('title',object.g_region_title));
	return regionsDiv;
}

function renderBudget(cost) {
	cost = formatNumber(cost);
	var budgetDiv = $('<div class="budgetDiv" />').html('<strong>'+cost+' р./мес.</strong>');
	budgetDiv.prepend($('<i class="icon icon-shopping-cart" />'));
	return budgetDiv;
}

function renderPageLink(url) {
	var link = $('<a target="_blank" />').attr('href',url).text(url);
	return link;
}

function renderActions() {
	var actionsDiv = $('<div class="regions" />');
	var tmpLink = $('<a class="btn btn-mini btn-danger removeKeyword" data-toggle="tooltip" />')
						.attr('title','Удалить фразу')
						.append($('<i class="icon icon-white icon-remove" />'));
	actionsDiv.append(tmpLink);
	return actionsDiv;
}

function addToStore(object, store) {
	var flag = true;
	
	$.each(store,function(i, item){
		if (isEqualItems(object,item)) {
			flag = false;
			return false;
		}
	});
	
	if (flag) {
		var lastId = store.length;
		store[lastId] = object;
		return lastId;	
	} else {
		return false;
	}
}

function isEqualItems(item1,item2) {
	var equal = false;
	if (item1.title == item2.title) {
		return true;
	}
	
	return equal;
}

function removeFromStore(id, store) {
	store.splice(id,1);
}