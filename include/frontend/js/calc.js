var mapYandex = {};
var keywordsStore = new Array();

$(document).ready(function() {
	$('.addKeywords').click(function() {
		addKeywords(this);
	});

	addTypeAhead();
	
	$('body').on('focus','#ya_region', function() {
		loadMaps();
	});

	$('#ya_is_region').change(function() {
		var val = $(this).is(':checked');
		if (val) {
			$('#ya_region').prop('disabled', false);
			loadMaps();
		} else {
			$('#ya_region').prop('disabled', true);
		}
	});
});


function regionSearch(query, map, type) {
	var result = map[query];
	if (result !== undefined) {
		applyRegion(query, type);
	}
}

function applyRegion(item,type) {
	if (type == 'yandex') {
		$('#ya_region_hidden').val(mapYandex[item].id);
		$('#ya_region_hidden2').val(mapYandex[item].yandexid);
		
	} else {
		$('#g_region_hidden').val(mapGoogle[item].id);
	}
}

function addTypeAhead() {
	$('#ya_region').typeahead({
		minLength:3,
		source : function(query, process) {
			objects = [];
			$.each(mapYandex, function(i, object) {
				objects.push(object.name);
			});
			process(objects);
		},
		updater : function(item) {
			applyRegion(item,'yandex');
			return item;
		}
	});
}

function loadMaps() {
	
	var mapLength = Object.keys(mapYandex).length;
	if (mapLength==0) {
		$("#selectYandexRegion").mask('Загружается список регионов');
		$.get('/services/getYaRegions', function(response) {
			$("#selectYandexRegion").unmask();
			var data = $.parseJSON(response);
			$.each(data, function(i, object) {
				mapYandex[object.name] = object;
			});
		});
	}
}

$(document).ready(function() {
	$('body').on('click','.removeKeyword',function(){
		var parentRow = $(this).closest('tr');
		var id = parentRow.attr('data-key');
		
		removeFromStore(id,keywordsStore);
		$(parentRow).remove();
		updateKeywordsSum();
		if (keywordsStore.length==0) {
			$('.keywordsFull').fadeOut();
			$('.keywordsEmpty').fadeIn();
		}
	});
	
	$('#addProject').on('click',function(){
		var form = $(this).closest('form');

		var keysJSON = (keywordsStore.length)?JSON.stringify(keywordsStore):'';
		$('#keywordsInput_02',form).val(keysJSON);	
		
		form.submit();
		return false;
	});
	
	if ($('#keywordsInput_02').length) {
		if ($('#keywordsInput_02').val()!="") {
			var items = $.parseJSON($('#keywordsInput_02').val());
			fillKeywordsTable(items, '#keywordsTable');
		}
	}
});


function addKeywords(button) {
	regionSearch($('#ya_region').val(),mapYandex,'yandex');
	
	var form = $(button).closest('form');
	var data = $(form).serialize();
	$(form).mask("Обновление итогового списка ключей..");
	$.ajax({
		type : "POST",
		url : "/calc/addKeywords",
		data : data,
		success : function(data) {
			console.debug(data);
			var response = $.parseJSON(data);
			if (response.result == 'success') {
				fillKeywordsTable(response.data, '#keywordsTable');
				updateKeywordsSum();
				$('#keywords_01').val('');
				
			}
			$(form).unmask();
		}
	});
	
	return false;
}

function fillKeywordsTable(data, target) {
	var tBody = $('tbody', target);
	var tableWrapper = tBody.closest('.datagrid');
	if (!tBody.length) {
		return false;
	}
	//tBody.empty();
	$(tableWrapper).fadeIn();
	$('.keywordsFull').fadeIn();
	$('.keywordsEmpty').fadeOut();
	for (var i = 0; i < data.length; i++) {
		var keyId = addToStore(data[i],keywordsStore);
		if (keyId!==false) {
			addDataRow(data[i],target,keyId);
		}
	}
	setWhiteWidth();
}

function updateKeywordsSum() {
	var sum = 0
	$.each(keywordsStore,function(i,keyword){
		sum+=keyword.budget;
	});
	sum = Math.round (sum * 100) / 100;
	$('.summaryBudgetMain').empty().text(formatNumber(sum)+' р.');
}

function addDataRow(object, dataTable, id) {
	var tmpRow = $('<tr />').attr('data-key',id);
	//$('<td />').append(renderRegions(object)).appendTo(tmpRow);
	$('<td />').append(renderTitle(object)).appendTo(tmpRow);
	//$('<td />').text(object.title).appendTo(tmpRow);
	//$('<td />').text(object.visits).appendTo(tmpRow);
	//$('<td />').text(object.views).appendTo(tmpRow);
	$('<td width="200">').append(renderBudget(object.budget)).appendTo(tmpRow);
	//$('<td />').append(renderPageLink(object.page)).appendTo(tmpRow);
	$('<td width="30"/>').append(renderActions()).appendTo(tmpRow);
	$('tbody',dataTable).append(tmpRow);
	
	$('a[data-toggle="tooltip"]').tooltip();
}

function renderTitle(object) {
	var titleDiv = $('<div class="titleDiv text-left" />');
	$(titleDiv).append($('<span class="preTitle" />').text('Ключевой запрос'));
	var titleRow = $('<div class="titleRow" />');
	$(titleRow).append($('<div class="keyTitle" />').text(object.title));
	$(titleRow).append($('<hr />'));
	$(titleDiv).append(titleRow);
	return titleDiv;
}

function renderRegions(object) {
	var regionsDiv = $('<div class="regions" />');
	regionsDiv.append($('<a class="yaIcon" data-toggle="tooltip" />').attr('title',object.ya_region_title));
	regionsDiv.append($('<a class="gIcon" data-toggle="tooltip" />').attr('title',object.g_region_title));
	return regionsDiv;
}

function renderBudget(cost) {
	//var budgetDiv = $('<div class="budgetDiv text-left" />').html('<strong>'+cost+' р.</strong>');
	cost = formatNumber(cost);
	
	var budgetDiv = $('<div class="budgetDiv text-left" />').html(cost+' р./мес.');
	return budgetDiv;
}

function renderPageLink(url) {
	var link = $('<a target="_blank" />').attr('href',url).text(url);
	return link;
}

function renderActions() {
	var actionsDiv = $('<div class="regions" />');
	var tmpLink = $('<a class="removeKeyword" data-toggle="tooltip" />')
						.attr('title','Удалить фразу')
						.append($('<i class="icon icon-remove" />'));
	actionsDiv.append(tmpLink);
	return actionsDiv;
}

function addToStore(object, store) {
	var flag = true;
	var maximum = 10;
	$.each(store,function(i, item){
		if (isEqualItems(object,item)) {
			flag = false;
			return false;
		}
	});
	
	if (flag && store.length<maximum) {
		var lastId = store.length;
		store[lastId] = object;
		return lastId;	
	} else {
		return false;
	}
}

function isEqualItems(item1,item2) {
	var equal = false;
	if (item1.title == item2.title) {
		return true;
	}
	
	return equal;
}

function removeFromStore(id, store) {
	store.splice(id,1);
}

