var mainpageKeywords;
var tmpMainpageKeywords;
var keywordsRotatorId = 0;

$(document).load(function() {
	setWhiteWidth();
});
$(document).ready(function() {
	setWhiteWidth();
	$('header nav a, .staticBar a').each(function() {
		var href = $('a').attr('href');
		if (href.charAt(0) == '#' && href.length>1) {
			$(this).smoothScroll();
		}
	});
	$('.carousel').carousel();
	$('body').on('click','.ajaxSubmit',ajaxForm);
	$('body').on('keyup','.ajaxSubmit',function(event) {
		if (event.keyCode == 13) {
			$(this).click();
		}
	});
	
	$('.tabWidget a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	})
	
	loadKeywords();
});

function loadKeywords() {
	$.get('/services/getMainpageKeywords',function(data){
		mainpageKeywords = $.parseJSON(data);
		tmpMainpageKeywords = mainpageKeywords.slice();
		updateKeywords();
		$('.keywordRotator .keywords').fadeIn('slow');
	});
}

function updateKeywords() {
	$('.keyword').each(function(index) {
	    var delay = index*200;
	    var object = $('.plane',this);
	    var id = setTimeout(function(){
	      rotateKeyword(object);
	    },delay);
  	});
  	keywordsRotatorId = setTimeout(updateKeywords,5000);
}

function getRandomKeyword() {
	if (tmpMainpageKeywords.length<3) {
		tmpMainpageKeywords = mainpageKeywords.slice();
	}
	var arrayKey = Math.floor(Math.random() * tmpMainpageKeywords.length);
	var value = tmpMainpageKeywords[arrayKey];
	tmpMainpageKeywords.splice(arrayKey, 1);
	return value;
}

function setWhiteWidth() {
	var parent = $('.whiteBg').parent(); 
	var mT = parseInt($('.whiteBg').css('marginTop'));
	var pH = parent.height();
	newHeight = document.getElementById('mainParts').offsetHeight;
	var newHeight = $('.whiteBg').parent().innerHeight()-mT;
	$('.whiteBg').css('height', newHeight + 'px');
}

function ajaxForm(button) {
	var form = $(this).closest('form');
	var url = $(form).attr('action');
	var fn = $(form).attr('data-callback');
	var data = $(form).serialize();
	data += '&ajax=true';
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			window[fn](data);
		}
	});
	return false;
}

function ajaxLogin(data) {
	var response = $.parseJSON(data);
	if (response.result == 'success') {
		var t = setTimeout(function() {
			window.location.href = '/projects';
		}, 100);
	} else {
		alert('Неверная пара логин/пароль!');
	}
	$('#informer').empty().append(response.html);
}

function ajaxCallback(data) {
	var response = $.parseJSON(data);
	if (response.result == 'success') {
		var t = setTimeout(function() {
			window.location.href = '/';
		}, 10000);
	} else {
		alert ('Форма заполнена не корректно!');
	}
	$('#callback').empty().append(response.html);
}

function formatNumber(str) {
	str = str.toString();
	return str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
}

function rotateKeyword(obj) {
  var text = getRandomKeyword();
  $(obj).transition({
    perspective: '200px',
    rotateX: '-=180deg',
    color:'#eeeeee'
  },250,'ease').text(text).transition({
    perspective: '200px',
    rotateX: '-=180deg',
    color:'#999999'
  },250,'ease');
}
