var map = {};
var keywordsStore = new Array();
var id = 0;
$(document).ready(function() {
	$('a[data-toggle="tooltip"]').tooltip();
	
	var clip = new ZeroClipboard( document.getElementById("copy-button"), {
	  moviePath: "/include/frontend/js/vendor/zero-clipboard/zero-clipboard.swf"
	} );
	
	clip.on( 'complete', function(client, args) {
	  	$(this).tooltip('show');
	} );
	
	$('.copyContainer').tooltip(
		{
			trigger:'click',
			delay: { show: 100, hide: 1000 }
		}
	);
	
	scrollToHash();
});

function scrollToHash() {
	var str= location.hash;
	if (str!==undefined) {
		var targetLink = $('a[data-toggle="tab"]');
		console.debug(targetLink);
		$('a[data-toggle="tab"][href="'+str+'"]').tab('show');
	}
}

$(document).ready(function() {
	$('.modalVoid').on('click',function() {
		var id = $(this).closest('tr').attr('data-key');
		var url = $(this).attr('href');
		url = url.replace("#","");
		var title = $(this).text();
		$.post('/'+url, {id: id}, function(data) {
			var response = $.parseJSON(data);
			openModal('supportModal', title, response.data, 490);
		});
		return false;
	});
	
	$('.keywordDelete').on('click',function() {
		if (confirm('Удалить ключевое слово')) {
			var keywordId = $(this).closest('tr').attr('data-key');
			$.post('/projects/removeKeyword', {id: keywordId}, function(response) {
				location.reload();
			});
		}
		return false;
	});
	
	$('.keywordEdit').on('click',function() {
		
		var editRow = $(this).closest('tr');
		if (editRow.parent().children('tr.editRow').length) {
			$('.keywordEditDiv').slideUp('slow',function() {
				$(this).closest('tr.editRow').remove();
			});
		} else {
			var keywordId = editRow.attr('data-key');
			$.post('/projects/editKeyword',{id:keywordId},function(response){
				addEditRow(response,editRow);	
				$('.keywordEditDiv').slideDown('slow');
				addTypeAhead();
			});	
		}
		return false;
	});
	
	$(".longString").each(function () {
        var maxwidth = 20;
        if ($(this).text().length > maxwidth) {
        	var newString = $(this).text().substring(0, maxwidth)+'...';
        	var span = $('<span data-toggle="tooltip" />').text(newString).attr('title',$(this).text());
            $(this).html(span);
            $('a[data-toggle="tooltip"]').tooltip();
        }
    });
    
    $('.linkDelete').on('click',function() {
		if (confirm('Удалить ссылку?')) {
			var linkId = $(this).closest('tr').attr('data-key');
			$.post('/projects/deleteLink', {id: linkId}, function(response) {
				//location.reload();
			});
		}
		return false;
	});
	
	$('.projectDelete').on('click',function() {
		if (confirm('Вы действительно хотите удалить проект?')) {
			var projectId = $(this).closest('tr').attr('data-key');
			$.post('/projects/delete', {id: projectId}, function(data) {
				var response = $.parseJSON(data);
				if (response.result == 'success') {
					location.reload();
				} else {
					alert('Невозможно удалить проект');
				}
			});
		}
		return false;
	});
	
	$('input.totalCheckbox').on('click',function(){
		var currentCell = $(this).closest('td, th');
		var currentValue = $(this).is(':checked')?'checked':false;
		var currentTable = $(this).closest('table');
		var columnNumber = $(currentCell).parent().children().index(currentCell);
		var targetCells = $('tr',currentTable).find('td:eq('+columnNumber+')');
		var subInputs = $('input[type="checkbox"]',targetCells);
		$(subInputs).prop('checked',currentValue);
	});
});

function keywordSaved(data) {
	var response = $.parseJSON(data);
	if (response.result == 'success') {
		location.reload();
	} else {
		alert(response.body);
	}
}

function addEditRow(data, currentRow) {
	var colspan = $('td',currentRow).length;
	var editRow = $('<tr />').addClass('editRow');
	var editCell = $('<td />').addClass('editCell').attr('colspan',colspan).append(data);
	editRow.append(editCell);
	editRow.insertAfter(currentRow);
}

function openModal(id, title, data, width) {
	width = typeof width !== 'undefined' ? width : 560;
	if (!$('#'+id).length) {
		var html = '<div id="'+id+'" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width:'+width+'px; margin-left:-'+(width/2)+'px"> \
		  <div class="modal-header">\
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>\
		    <h3 id="myModalLabel">'+title+'</h3>\
		  </div>\
		  <div class="modal-body">\
		  </div>\
		</div>';
		$('body').append($(html));
	} else {
		
	}
	$('.modal-body','#'+id).html(data);
	$('#'+id).modal('toggle');
}
